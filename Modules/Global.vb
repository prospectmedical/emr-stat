#Region "Init"
Option Strict On
Option Explicit On
#End Region

Module [Global]
#Region "Variables"
	Public gsEventLogName, gsLogPath, gsLogFileName, gsWindowsUserID, gsVersion, gsAppPath, gsEMailSender, gsHTMLPath, gsGraphicsPath, gsINIFileName, gsScriptPath As String
	Public gsUnsignedDocumentsTime, gsReportRecipientUnsignedDocuments, gsDischargeSumTime, gsEMailCheckBoxScriptTime, gsReportRecipientEMailCheckBoxScript As String
	Public gsErrorMessage, gsCOVID19WaterburyInputfolder, gsCOVID19ECHNInputFolder, gsCOVID19EOGHInputFolder, gsCOVID19AltaInputFolder, gsCOVID19WaterburyOutputFolder As String
	Public gsCOVID19PMHOutputFolder As String
	Public gbTrace, gbSendEMail, gbSendErrorEMail, gbByPassDateCheck As Boolean
	'Public gbByPassDateCheck As Boolean
	Public giServicePauseTime, giUnsignedDocumentsInterval As Int32
	Public galEMailRecipient, galCOVID19WarehouseUpdateTime, galCOVID19ReportTime, galCOVID19CharterCareUpdateTime, galCOVID19HANUpdateTime, galCOVID19WaterburyOutputTime As ArrayList
	Public galCOVID19LabReportEMail, galUnsignedDocumentsDay, galCOVID19VisiquateUpdateTime As ArrayList
#End Region

#Region "Constants"
	Public gsApplicationName As String = "EMR Stat"
	Public gsSMTPAddress As String = "smtp.crozer.org"
	Public giEMailPauseTime As Int32 = 2000
	Public gsBCC As String = "EMRStat@Crozer.org"
	Public Const gsDoubleQuote As String = Chr(34)
	Public Const gsBuildPath As String = "C:\EMRStat"
	Public Const giTabSpaces As Int32 = 5
	Public Const giHistoryDays As Int32 = 14
	Public Const giNormalSQLCommandTimeout As Int32 = 120
	Public Const giLongSQLCommandTimeout As Int32 = 720
	Public Const gsCOVID19ReportFileName = "COVID-19 Lab Report"
#End Region
End Module