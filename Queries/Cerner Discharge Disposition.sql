SELECT	dsch_disp_key, 
		src_sys_id, 
		orgz_cd, 
		dsch_disp, 
		dsch_disp_desc, 
		dsch_disp_short_desc, 
		lkup_dsch_disp, 
		lkup_dsch_disp_ind, 
		dsch_ub_cd, 
		dsch_ub_desc
FROM	smsdss.dsch_disp_dim