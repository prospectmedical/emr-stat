#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports System.Data.OleDb
#End Region

Public Class InitializeApplication
#Region "Class Properties"
	Public Property LogPath As String
	Public Property Trace As Boolean
	Public Property ServicePauseTime As Int32
	Public Property EMailRecipient As ArrayList
	Public Property EMailSender As String
	Public Property SendEMail As Boolean
	Public Property SendErrorEMail As Boolean
	Public Property UnsignedDocumentsTime As String
	Public Property ReportRecipientUnsignedDocuments As String
	Public Property UnsignedDocumentsDay As ArrayList
	Public Property UnsignedDocumentsInterval As Int32
	Public Property DischargeSumTime As String
	Public Property EMailCheckBoxScriptTime As String
	Public Property ReportRecipientEMailCheckBoxScript As String
	Public Property COVID19WarehouseUpdateTime As ArrayList
	Public Property COVID19CharterCareUpdateTime As ArrayList
	Public Property COVID19WaterburyInputFolder As String
	Public Property COVID19WaterburyOutputFolder As String
	Public Property COVID19WaterburyOutputTime As ArrayList
	Public Property COVID19ECHNInputFolder As String
	Public Property COVID19EOGHInputFolder As String
	Public Property COVID19AltaInputFolder As String
	Public Property COVID19HANUpdateTime As ArrayList
	Public Property COVID19ReportTime As ArrayList
	Public Property ByPassDateCheck As Boolean
	Public Property COVID19LabReportEMail As ArrayList
	Public Property COVID19VisiquateUpdateTime As ArrayList
	Public Property COVID19PMHOutputFolder As String


#End Region

#Region "Initialize"
	Private Sub Initialize()
		LogPath = "C:\Service Logs\EMR Stat\"
		Trace = True
		ServicePauseTime = 60000
		EMailRecipient = New ArrayList
		EMailSender = String.Empty
		SendEMail = False
		SendErrorEMail = False
		EMailCheckBoxScriptTime = "08:00"
		ReportRecipientEMailCheckBoxScript = String.Empty
		COVID19WarehouseUpdateTime = New ArrayList
		COVID19CharterCareUpdateTime = New ArrayList
		COVID19WaterburyInputFolder = String.Empty
		COVID19WaterburyOutputFolder = String.Empty
		COVID19WaterburyOutputTime = New ArrayList
		COVID19ECHNInputFolder = String.Empty
		COVID19EOGHInputFolder = String.Empty
		COVID19HANUpdateTime = New ArrayList
		COVID19ReportTime = New ArrayList
		COVID19LabReportEMail = New ArrayList
		UnsignedDocumentsDay = New ArrayList
		COVID19VisiquateUpdateTime = New ArrayList
		COVID19PMHOutputFolder = String.Empty
	End Sub
#End Region

#Region "New"
	Public Sub New()
    Initialize()
  End Sub
#End Region

#Region "GetSettings"
  Public Function GetSettings(INIPathFileName As String) As Boolean
    Dim oFileStream As New FileStream(INIPathFileName, FileMode.Open, FileAccess.Read)
    Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord As String, sItemType As String, sItemSetting, sWork As String
		Dim iINICounter As Int32 = 0
		Const iINICount As Int32 = 28
		Dim iWork As Integer
		Dim bEMailRecipientFound As Boolean = False
		Dim bStatusEMailTimeFound As Boolean = False
		Dim bStatusEMailRecipientFound As Boolean = False
		Dim bTestModeEMailAccountFound As Boolean = False
		Dim bReportRecipientUnsignedDocumentsFound As Boolean = False
		Dim bReportRecipientEMailCheckBoxScriptFound As Boolean = False
		Dim bCOVID19WarehouseUpdateTimeFound As Boolean = False
		Dim bCOVID19CharterCareUpdateTimeFound As Boolean = False
		Dim bCOVID19HANUpdateTimeFound As Boolean = False
		Dim bCOVID19ReportTimeFound As Boolean = False
		Dim bCOVID19WaterburyOutputTimeFound As Boolean = False
		Dim bCOVID19LabReportEMailFound As Boolean = False
		Dim bUnsignedDocumentsDayFound As Boolean = False
		Dim bCOVID19VisiquateUpdateTimeFound As Boolean = False

		Try
      GetSettings = False
      oStreamReader.BaseStream.Seek(0, SeekOrigin.Begin)
      While oStreamReader.Peek() > -1
        sInRecord = Trim$(oStreamReader.ReadLine())
        If Len(sInRecord) > 0 Then
          If Mid$(sInRecord, 1, 1) <> ";" Then
            iWork = InStr(1, sInRecord, ";")
            If iWork > 0 Then sInRecord = Trim$(Mid$(sInRecord, 1, iWork - 1))
            iWork = InStr(1, sInRecord, " ")
            If iWork > 3 Then
							If iWork < Len(sInRecord) Then
								sItemType = Trim$(LCase$(Mid$(sInRecord, 1, iWork - 1)))
								sItemSetting = Trim$(Mid$(sInRecord, iWork + 1, Len(sInRecord)))
								Select Case sItemType
									Case "[logpath]"
										LogPath = sItemSetting
										iINICounter += 1
									Case "[trace]"
										Trace = LCase$(SetString(sItemSetting)) = "true"
										iINICounter += 1
									Case "[servicepausetime]"
										ServicePauseTime = SetInt32(SetDouble(sItemSetting) * 60000)
										iINICounter += 1
									Case "[emailrecipient]"
										EMailRecipient.Add(SetString(sItemSetting))
										If Not bEMailRecipientFound Then
											iINICounter += 1
											bEMailRecipientFound = True
										End If
									Case "[emailsender]"
										EMailSender = SetString(sItemSetting)
										iINICounter += 1
									Case "[sendemail]"
										SendEMail = LCase$(SetString(sItemSetting)) = "true"
										iINICounter += 1
									Case "[senderroremail]"
										SendErrorEMail = LCase$(SetString(sItemSetting)) = "true"
										iINICounter += 1
									Case "[unsigneddocumentstime]"
										UnsignedDocumentsTime = SetString(sItemSetting)
										iINICounter += 1
									Case "[reportrecipientunsigneddocuments]"
										sWork = SetString(sItemSetting)
										If Len(ReportRecipientUnsignedDocuments) > 0 Then
											If Mid(ReportRecipientUnsignedDocuments, Len(ReportRecipientUnsignedDocuments), 1) <> "," AndAlso Mid(ReportRecipientUnsignedDocuments, Len(ReportRecipientUnsignedDocuments), 1) <> ";" Then ReportRecipientUnsignedDocuments = ReportRecipientUnsignedDocuments & ";"
										End If
										ReportRecipientUnsignedDocuments = ReportRecipientUnsignedDocuments & Replace(sWork, ",", ";")
										If Not bReportRecipientUnsignedDocumentsFound Then
											iINICounter += 1
											bReportRecipientUnsignedDocumentsFound = True
										End If
									Case "[unsigneddocumentsday]"
										UnsignedDocumentsDay.Add(SetString(sItemSetting))
										If Not bUnsignedDocumentsDayFound Then
											iINICounter += 1
											bUnsignedDocumentsDayFound = True
										End If
									Case "[unsigneddocumentsinterval]"
										UnsignedDocumentsInterval = SetInt32(sItemSetting)
										iINICounter += 1
									Case "[dischargesumtime]"
										DischargeSumTime = SetString(sItemSetting)
										iINICounter += 1
									Case "[emailcheckboxscripttime]"
										EMailCheckBoxScriptTime = SetString(sItemSetting)
										iINICounter += 1
									Case "[reportrecipientemailcheckboxscript]"
										sWork = SetString(sItemSetting)
										If Len(ReportRecipientEMailCheckBoxScript) > 0 Then
											If Mid(ReportRecipientEMailCheckBoxScript, Len(ReportRecipientEMailCheckBoxScript), 1) <> "," AndAlso Mid(ReportRecipientEMailCheckBoxScript, Len(ReportRecipientEMailCheckBoxScript), 1) <> ";" Then ReportRecipientEMailCheckBoxScript = ReportRecipientEMailCheckBoxScript & ";"
										End If
										ReportRecipientEMailCheckBoxScript = ReportRecipientEMailCheckBoxScript & Replace(sWork, ",", ";")
										If Not bReportRecipientEMailCheckBoxScriptFound Then
											iINICounter += 1
											bReportRecipientEMailCheckBoxScriptFound = True
										End If
									Case "[covid19warehouseupdatetime]"
										COVID19WarehouseUpdateTime.Add(SetString(sItemSetting))
										If Not bCOVID19WarehouseUpdateTimeFound Then
											iINICounter += 1
											bCOVID19WarehouseUpdateTimeFound = True
										End If
									Case "[covid19chartercareupdatetime]"
										COVID19CharterCareUpdateTime.Add(SetString(sItemSetting))
										If Not bCOVID19CharterCareUpdateTimeFound Then
											iINICounter += 1
											bCOVID19CharterCareUpdateTimeFound = True
										End If
									Case "[covid19waterburyinputfolder]"
										COVID19WaterburyInputFolder = SetString(sItemSetting)
										iINICounter += 1
									Case "[covid19waterburyouputfolder]"
										COVID19WaterburyOutputFolder = SetString(sItemSetting)
										iINICounter += 1
									Case "[covid19waterburyoutputtime]"
										COVID19WaterburyOutputTime.Add(SetString(sItemSetting))
										If Not bCOVID19WaterburyOutputTimefound Then
											iINICounter += 1
											bCOVID19WaterburyOutputTimefound = True
										End If
									Case "[covid19echninputfolder]"
										COVID19ECHNInputFolder = SetString(sItemSetting)
										iINICounter += 1
									Case "[covid19eoghinputfolder]"
										COVID19EOGHInputFolder = SetString(sItemSetting)
										iINICounter += 1
									Case "[covid19altainputfolder]"
										COVID19AltaInputFolder = SetString(sItemSetting)
										iINICounter += 1
									Case "[covid19hanupdatetime]"
										COVID19HANUpdateTime.Add(SetString(sItemSetting))
										If Not bCOVID19HANUpdateTimeFound Then
											iINICounter += 1
											bCOVID19HANUpdateTimeFound = True
										End If
									Case "[covid19reporttime]"
										COVID19ReportTime.Add(SetString(sItemSetting))
										If Not bCOVID19ReportTimeFound Then
											iINICounter += 1
											bCOVID19ReportTimeFound = True
										End If
									Case "[bypassdatecheck]"
										ByPassDateCheck = LCase$(SetString(sItemSetting)) = "true"
										iINICounter += 1
									Case "[covid19labreportemail]"
										COVID19LabReportEMail.Add(SetString(sItemSetting))
										If Not bCOVID19LabReportEMailFound Then
											iINICounter += 1
											bCOVID19LabReportEMailFound = True
										End If
									Case "[covid19visiquateupdatetime]"
										COVID19VisiquateUpdateTime.Add(SetString(sItemSetting))
										If Not bCOVID19VisiquateUpdateTimeFound Then
											iINICounter += 1
											bCOVID19VisiquateUpdateTimeFound = True
										End If
									Case "[covid19pmhouputfolder]"
										COVID19PMHOutputFolder = SetString(sItemSetting)
										iINICounter += 1
								End Select
							End If
            End If
          End If
        End If
      End While
      oStreamReader.Close()
      oFileStream.Close()
      If iINICount <> iINICounter Then
        WriteEventLog("InitializeApplication", "Error: Incorrect ini count, entries are missing.", EventLogEntryType.Error)
      Else
        GetSettings = True
      End If
    Catch oException As Exception
      WriteEventLog("InitializeApplication", "Error: " & oException.Message, EventLogEntryType.Error)
      GetSettings = False
    Finally
    End Try
  End Function
#End Region

#Region "Version Check"
  Public ReadOnly Property VersionCheck(Version As String) As Boolean
    Get
      Dim oDBC As OleDbConnection = New OleDbConnection
      Dim oCmd As OleDbCommand = New OleDbCommand
      Dim oRdr As OleDbDataReader = Nothing

      If Not OpenDB(oDBC) Then Return False
      oCmd.CommandText = "sVersionCheck"
      oCmd.CommandType = CommandType.StoredProcedure
      oCmd.Parameters.Add("@Version", OleDbType.VarChar, 25).Value = Version
      Try
        oCmd.Connection = oDBC
        oRdr = oCmd.ExecuteReader
        VersionCheck = oRdr.HasRows
      Catch oException As Exception
        VersionCheck = False
      Finally
        CloseRDR(oRdr)
        CloseCMD(oCmd)
        CloseDB(oDBC)
      End Try
    End Get
  End Property
#End Region
End Class