﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class COVID19Patient
#Region "Declartions"
	Private moDBC As OleDbConnection = New OleDbConnection
#End Region

#Region "Class Properties"
	Public Property PatientSourceID As Int32
	Public Property LastName As String
	Public Property FirstName As String
	Public Property MiddleName As String
	Public Property Gender As String
	Public Property DateOfBirth As Date
	Public Property ZipCode As String
	Public Property DistrictCode As String
	Public Property PatientID As String
	Public Property PatientIDStartDateTime As Date
	Public Property MedicalRecordNumber As String
	Public Property NHSIDNumber As String
	Public Property AdmitDateTime As Date
	Public Property DischargeDateTime As Date
	Public Property OrderCode As String
	Public Property OrderDescription As String
	Public Property OrderLocation As String
	Public Property OrderingPhysicanNumber As String
	Public Property OrderingPhysicianName As String
	Public Property ResultCode As String
	Public Property ResultCodeName As String
	Public Property ResultCreateDateTime As Date
	Public Property ResultDisplay As String
	Public Property ResultText As String
	Public Property FinalResultID As Int32
	Public Property FinalResultDateTime As String
	Public Property OrderStatusCode As String
	Public Property OrderStatus As String
	Public Property DischargeDisposition As String
	Public Property VisitType As String
	Public Property HospitalService As String
	Public Property PatientType As String
	Public Property OrderProcessDateTime As Date
	Public Property OrderNumber As Int32
	Public Property GroupKey As String
	Public Property ErrorMessage As String
	Public Property UpdateStatusMessage As String
#End Region

#Region "Routines"
	Public Sub New()
		Initialize()
	End Sub

	Public Sub Initialize()
		moDBC = New OleDbConnection
		NewPatient()
		ErrorMessage = vbNullString
	End Sub

	Private Function ValidDate(InDate As Date) As Boolean
		If IsDate(InDate.ToString) Then
			If Year(InDate) = 1 Then Return False
			Return True
		Else
			Return False
		End If

	End Function
#End Region

#Region "Open"
	Public Function Open() As Boolean
		Try
			If Not OpenDB(moDBC, 1) Then Throw New Exception("Error opening database.")
			Open = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.Open: " & oException.Message
			Open = False
		Finally
		End Try
	End Function
#End Region

#Region "New"
	Public Sub NewPatient()
		PatientSourceID = 0
		LastName = String.Empty
		FirstName = String.Empty
		MiddleName = String.Empty
		Gender = String.Empty
		DateOfBirth = Nothing
		ZipCode = String.Empty
		DistrictCode = String.Empty
		PatientID = String.Empty
		PatientIDStartDateTime = Nothing
		MedicalRecordNumber = String.Empty
		NHSIDNumber = String.Empty
		AdmitDateTime = Nothing
		DischargeDisposition = Nothing
		OrderCode = String.Empty
		OrderDescription = String.Empty
		OrderLocation = String.Empty
		OrderingPhysicanNumber = String.Empty
		OrderingPhysicianName = String.Empty
		ResultCode = String.Empty
		ResultCodeName = String.Empty
		ResultCreateDateTime = Nothing
		ResultDisplay = String.Empty
		ResultText = String.Empty
		FinalResultID = 0
		FinalResultDateTime = String.Empty
		OrderStatusCode = String.Empty
		OrderStatus = String.Empty
		DischargeDisposition = String.Empty
		VisitType = String.Empty
		HospitalService = String.Empty
		PatientType = String.Empty
		OrderProcessDateTime = Nothing
		OrderNumber = 0
		GroupKey = String.Empty
	End Sub
#End Region

#Region "Insert"
	Public Function Insert01() As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "iCOVID19Patient01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@PatientSourceID", OleDbType.BigInt).Value = PatientSourceID
			oCmd.Parameters.Add("@LastName", OleDbType.VarChar, 100).Value = LastName
			oCmd.Parameters.Add("@FirstName", OleDbType.VarChar, 100).Value = FirstName
			oCmd.Parameters.Add("@MiddleName", OleDbType.VarChar, 100).Value = MiddleName
			oCmd.Parameters.Add("@Gender", OleDbType.VarChar, 10).Value = Gender
			If ValidDate(DateOfBirth) Then
				oCmd.Parameters.Add("@DateOfBirth", OleDbType.Date).Value = DateOfBirth
			Else
				oCmd.Parameters.Add("@DateOfBirth", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@ZipCode", OleDbType.VarChar, 15).Value = ZipCode
			oCmd.Parameters.Add("@DistrictCode", OleDbType.Char, 15).Value = DistrictCode
			oCmd.Parameters.Add("@PatientID", OleDbType.VarChar, 100).Value = PatientID
			If ValidDate(PatientIDStartDateTime) Then
				oCmd.Parameters.Add("@PatientIDStartDateTime", OleDbType.Date).Value = PatientIDStartDateTime
			Else
				oCmd.Parameters.Add("@PatientIDStartDateTime", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@MedicalRecordNumber", OleDbType.VarChar, 40).Value = MedicalRecordNumber
			oCmd.Parameters.Add("@NHSIDNumber", OleDbType.VarChar, 17).Value = NHSIDNumber

			If ValidDate(AdmitDateTime) Then
				oCmd.Parameters.Add("@AdmitDateTime", OleDbType.Date).Value = AdmitDateTime
			Else
				oCmd.Parameters.Add("@AdmitDateTime", OleDbType.Date).Value = DBNull.Value
			End If
			If ValidDate(DischargeDateTime) Then
				oCmd.Parameters.Add("@DischargeDateTime", OleDbType.Date).Value = DischargeDateTime
			Else
				oCmd.Parameters.Add("@DischargeDateTime", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@OrderCode", OleDbType.VarChar, 100).Value = OrderCode
			oCmd.Parameters.Add("@OrderDescription", OleDbType.VarChar, 100).Value = OrderDescription
			oCmd.Parameters.Add("@OrderLocation", OleDbType.VarChar, 45).Value = OrderLocation
			oCmd.Parameters.Add("@OrderingPhysicanNumber", OleDbType.VarChar, 100).Value = OrderingPhysicanNumber
			oCmd.Parameters.Add("@OrderingPhysicianName", OleDbType.VarChar, 200).Value = OrderingPhysicianName
			oCmd.Parameters.Add("@ResultCode", OleDbType.VarChar, 100).Value = ResultCode
			oCmd.Parameters.Add("@ResultCodeName", OleDbType.VarChar, 64).Value = ResultCodeName
			If ValidDate(ResultCreateDateTime) Then
				oCmd.Parameters.Add("@ResultCreateDateTime", OleDbType.Date).Value = ResultCreateDateTime
			Else
				oCmd.Parameters.Add("@ResultCreateDateTime", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@ResultDisplay", OleDbType.VarChar, 2000).Value = ResultDisplay
			oCmd.Parameters.Add("@ResultText", OleDbType.VarChar, 80).Value = ResultText
			oCmd.Parameters.Add("@FinalResult", OleDbType.BigInt).Value = FinalResultID
			oCmd.Parameters.Add("@FinalResultDateTime", OleDbType.VarChar, 30).Value = FinalResultDateTime
			oCmd.Parameters.Add("@OrderStatusCode", OleDbType.VarChar, 25).Value = OrderStatusCode
			oCmd.Parameters.Add("@OrderStatus", OleDbType.VarChar, 45).Value = OrderStatus
			oCmd.Parameters.Add("@DischargeDisposition", OleDbType.VarChar, 254).Value = DischargeDisposition
			oCmd.Parameters.Add("@VisitType", OleDbType.VarChar, 25).Value = VisitType
			oCmd.Parameters.Add("@HospitalService", OleDbType.VarChar, 25).Value = HospitalService
			oCmd.Parameters.Add("@PatientType", OleDbType.VarChar, 25).Value = PatientType
			If ValidDate(OrderProcessDateTime) Then
				oCmd.Parameters.Add("@OrderProcessDateTime", OleDbType.Date).Value = OrderProcessDateTime
			Else
				oCmd.Parameters.Add("@OrderProcessDateTime", OleDbType.Date).Value = DBNull.Value
			End If
			oCmd.Parameters.Add("@OrderNumber", OleDbType.BigInt).Value = OrderNumber
			oCmd.Parameters.Add("@GroupKey", OleDbType.VarChar, 100).Value = GroupKey
			oCmd.Connection = moDBC
			oCmd.ExecuteNonQuery()
			Insert01 = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.Insert01: " & oException.Message
			Insert01 = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function
#End Region

#Region "Delete"
	Public Function DeleteByCurrentKey(pPatientSourceID As Int32, pGroupKey As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "dCOVID19PatientByCurrentGroupKey"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@PatientSourceID", OleDbType.BigInt).Value = pPatientSourceID
			oCmd.Parameters.Add("@GroupKey", OleDbType.VarChar, 100).Value = pGroupKey
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			DeleteByCurrentKey = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.DeleteByCurrentKey: " & oException.Message
			DeleteByCurrentKey = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function DeleteByPastKey(pPatientSourceID As Int32, pGroupKey As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "dCOVID19PatientPastGroupKey"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@PatientSourceID", OleDbType.BigInt).Value = pPatientSourceID
			oCmd.Parameters.Add("@GroupKey", OleDbType.VarChar, 100).Value = pGroupKey
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			DeleteByPastKey = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.DeleteByPastKey: " & oException.Message
			DeleteByPastKey = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "UpdateFinalResult"
	Public Function FinalResultHAN(GroupKey As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "uCOVID19FinalResultHAN"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@GroupKey", OleDbType.VarChar, 100).Value = GroupKey
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			FinalResultHAN = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.FinalResultHAN: " & oException.Message
			FinalResultHAN = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function FinalResultByPatientSource(GroupKey As String, PatientSourceID As Int32) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			Select Case PatientSourceID
				Case 1
					oCmd.CommandText = "uCOVID19FinalResulteCareAntibody"
				Case 3
					oCmd.CommandText = "uCOVID19FinalResultCharterCare"
				Case 4
					oCmd.CommandText = "uCOVID19FinalResultWaterbury"
				Case 5
					oCmd.CommandText = "uCOVID19FinalResultECHN"
				Case 6
					oCmd.CommandText = "uCOVID19FinalResultEOGH"
				Case 7
					oCmd.CommandText = "uCOVID19FinalResultAlta"
			End Select

			oCmd.CommandType = CommandType.StoredProcedure
			If PatientSourceID <> 1 Then oCmd.Parameters.Add("@GroupKey", OleDbType.VarChar, 100).Value = GroupKey
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			FinalResultByPatientSource = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.FinalResultByPatientSource: " & oException.Message
			FinalResultByPatientSource = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function FinalResultWarehouseInit() As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "uCOVID19WarehouseFinalResult01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Connection = moDBC
			oCmd.ExecuteNonQuery()
			FinalResultWarehouseInit = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.FinalResultWarehouse: " & oException.Message
			FinalResultWarehouseInit = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function

	Public Function FinalResultWarehouse(pCOVID19PatientID As Int32, pFinalResultID As Int32, pFinalResultDateTime As String) As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "uCOVID19WarehouseFinalResult02"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@COVID19PatientID", OleDbType.BigInt).Value = pCOVID19PatientID
			oCmd.Parameters.Add("@FinalResultID", OleDbType.BigInt).Value = pFinalResultID
			oCmd.Parameters.Add("@FinalResultDateTime", OleDbType.VarChar, 30).Value = pFinalResultDateTime
			oCmd.Connection = moDBC
			oCmd.ExecuteNonQuery()
			FinalResultWarehouse = True
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.FinalResultWarehouse: " & oException.Message
			FinalResultWarehouse = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function
#End Region

#Region "Close"
	Public Sub Close()
		Try
			CloseDB(moDBC)
		Catch oException As Exception
			ErrorMessage = "COVID19Count.Close: " & oException.Message
		Finally
		End Try
	End Sub
#End Region

#Region "UpdateCompleteCheck"
	''' <summary>
	''' Open database
	''' </summary>
	''' <param name="CheckType">1=CKHS/HAN, 2=Waterbury</param>
	''' <remarks></remarks>
	Public Function UpdateCompleteCheck(CheckType As Int32) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			UpdateStatusMessage = String.Empty
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			Select Case CheckType
				Case 1
					oCmd.CommandText = "sCOVID19PatientUpdateStatusCKHSHAN"
				Case 2
					oCmd.CommandText = "sCOVID19PatientUpdateStatusWaterbury"
				Case 3
					oCmd.CommandText = "sCOVID19PatientUpdateStatusPMH"
			End Select
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oRdr.Read()
				UpdateStatusMessage = SetString(oRdr!ReturnMessage)
				If Len(UpdateStatusMessage) <= 0 Then
					UpdateCompleteCheck = True
				Else
					UpdateCompleteCheck = False
				End If
			Else
				UpdateStatusMessage = "Unable to determine update state."
				UpdateCompleteCheck = False
			End If
		Catch oException As Exception
			ErrorMessage = "COVID19Patient.UpdateComplete: " & oException.Message
			UpdateStatusMessage = ErrorMessage
			UpdateCompleteCheck = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Class


