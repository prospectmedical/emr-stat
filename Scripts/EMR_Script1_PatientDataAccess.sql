spool EMR_Script1_PatientDataAccess.log
INSERT INTO MEDLISTS (MEDLISTSID, TABLENAME, DESCRIPTION, ListOrder, FUNCTIONNAME, OtherLong ,CREATED, CREATEDBY, LASTMODIFIED, LASTMODIFIEDBY)
Select  MEDLISTS_SEQ.NEXTVAL, 'OneTimeActivity', 'One time execution of Patient Api Access Script 1', 1, 'PatientAPIAccessScript1' ,0, SYSDATE, 'System', SYSDATE, 'System' from dual
where not exists (Select * from MEDLISTS where TABLENAME = 'OneTimeActivity' and FUNCTIONNAME = 'PatientAPIAccessScript1');
COMMIT;
ALTER TABLE MUActivityLog DISABLE ALL TRIGGERS;
DECLARE 
  v_currentdate DATE := SYSDATE; 
  v_APIB2CDate DATE := to_date('01/01/2018','MM/DD/YYYY');
  v_PreviousRun Numeric(19,0):= 0;
BEGIN
Select NVL(OtherLong,0) into v_PreviousRun FROM MEDLISTS where TABLENAME = 'OneTimeActivity' and FUNCTIONNAME = 'PatientAPIAccessScript1';
	if NVL(v_PreviousRun,0) = 0 then
		INSERT INTO MUActivityLog (MUActivityLogId, MUActivityLogTypeId, PID,  PVID, exported, DeliveryMethod, Event_Timestamp, DB_Create_Timestamp)
		SELECT GEN_EMR_ID, (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 526 else 525 end), PID, -2, 'N', 
		'Patient Access'|| (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then ' Granted' else ' Denied' end), v_APIB2CDate, v_currentdate
		from PERSON 
		where NVL(ISPATIENT,'Y') = 'Y' and PSTATUS = 'A' 
		and HasPatientAccess <> (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end);  
		update PERSON set HasPatientAccess = (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end)
		where NVL(ISPATIENT,'Y') = 'Y' and PSTATUS = 'A' 
		and HasPatientAccess <> (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end);
		UPDATE MEDLISTS SET OtherLong = 1 WHERE FUNCTIONNAME='PatientAPIAccessScript1' AND TABLENAME='OneTimeActivity' and OtherLong = 0;
		commit;
	  end if; 
END;
/
DECLARE 
  v_currentdate DATE := SYSDATE; 
  v_APIB2CDate DATE := to_date('01/01/2018','MM/DD/YYYY');
BEGIN
		INSERT INTO MUActivityLog (MUActivityLogId, MUActivityLogTypeId, PID,  PVID, exported, DeliveryMethod, Event_Timestamp, DB_Create_Timestamp)
		SELECT GEN_EMR_ID, (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 526 else 525 end), PID, -2, 'N', 
		'Patient Access'|| (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then ' Granted' else ' Denied' end), v_APIB2CDate, v_currentdate
		from PERSON 
		where NVL(ISPATIENT,'Y') = 'Y' and PSTATUS = 'A' 
		and HasPatientAccess <> (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end);    
		update PERSON set HasPatientAccess = (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end)
		where NVL(ISPATIENT,'Y') = 'Y' and PSTATUS = 'A' 
		and HasPatientAccess <> (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end);
		commit;
END;
/
DECLARE 
  v_currentdate DATE := SYSDATE; 
  v_APIB2CDate DATE := to_date('01/01/2018','MM/DD/YYYY');
BEGIN
		INSERT INTO MUActivityLog (MUActivityLogId, MUActivityLogTypeId, PID,  PVID, exported, DeliveryMethod, Event_Timestamp, DB_Create_Timestamp)
		SELECT GEN_EMR_ID, (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 526 else 525 end), PID, -2, 'N', 
		'Patient Access'|| (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then ' Granted' else ' Denied' end), v_APIB2CDate, v_currentdate
		from PERSON 
		where NVL(ISPATIENT,'Y') = 'Y' and PSTATUS = 'A' 
		and HasPatientAccess = 1 AND (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end) = 1 
		AND not exists(select 1 from MUActivityLog where PID = PERSON.PID and MUActivityLogTypeId = 526);
		commit;
END;
/
ALTER TABLE MUActivityLog ENABLE ALL TRIGGERS;
Exec ML.Patch_Update('EMR Database Script1 for Patient Data Access Checkbox', 'EMR Database Script1 for Patient Data Access Checkbox');
commit;
spool off
exit;