#Region "Init"
Option Explicit On
Option Strict On
Imports System.IO
Imports Microsoft.Office.Interop
Imports System.Threading
Imports Oracle.DataAccess.Client
Imports System.Data.OleDb
Imports Microsoft.VisualBasic.FileIO
Imports System.Data.SqlClient
#End Region

Module ServiceRoutines
#Region "Declarations"
	Private msFontName, msCurrentCOVID19FileName As String
	Private miFontSize, miColor, miRow, miColorHold As Int32
	Private mbBold As Boolean
	Private moExcelApplication As Excel.Application
	Private moWorkbook As Excel.Workbook
	Private moWorkSheet As Excel.Worksheet
	Private moDBCEMR As OleDbConnection = New OleDbConnection
	Private moDBCWarehouse As OleDbConnection = New OleDbConnection
#End Region

#Region "Properties"
	Public ErrorMessage As String
#End Region

#Region "Routines"
	Private Sub CloseWorkbook()
		Try
			moWorkbook.Close(False)
		Catch oException As Exception
		Finally
		End Try
	End Sub

	Private Sub ReleaseObject(InObject As Object)
		Try
			System.Runtime.InteropServices.Marshal.ReleaseComObject(InObject)
			InObject = Nothing
		Catch oException As Exception
		Finally
		End Try
	End Sub

	Private Sub SetColumn(Column As String, Width As Double, Optional Allignment As String = "l", Optional CellFormat As String = "@")
		With moWorkSheet.Range(Column & ":" & Column)
			.Columns.ColumnWidth = Width
			Select Case LCase(Allignment)
				Case "c"
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
				Case "r"
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
				Case Else
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
			End Select
			.Font.Size = miFontSize
			.Font.Name = msFontName
			.Font.Bold = mbBold
			.Interior.Color = miColor
			.WrapText = True
			.NumberFormat = CellFormat
		End With
	End Sub

	Private Sub RowHeight(Height As Double)
		With moWorkSheet.Range("A" & miRow, "A" & miRow)
			.Rows.RowHeight = Height
		End With
	End Sub

	Private Sub SheetHeading(Value As String, BeginningCell As String, EndingCell As String, Allignment As String, Color As Int32, Optional Orientation As Int32 = 0)
		With moWorkSheet.Range(BeginningCell, EndingCell)
			.Merge()
			Select Case LCase(Allignment)
				Case "c"
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
				Case "r"
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
				Case Else
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
			End Select
			.Font.Size = miFontSize
			.Font.Name = msFontName
			.Font.Bold = mbBold
			.Interior.Color = Color
			.BorderAround(, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic)
			.WrapText = True
			.NumberFormat = "@"
			.Value = Value
			If Orientation <> 0 Then
				.Orientation = Orientation
				.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter
			End If
		End With
	End Sub

	Private Sub ValueCell(Value As String, Column As String, Allignment As String, Optional CellFormat As String = "@")
		With moWorkSheet.Range(Column & miRow) ', Column & miRow)
			Select Case LCase(Allignment)
				Case "c"
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
				Case "r"
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignRight
				Case Else
					.HorizontalAlignment = Excel.XlHAlign.xlHAlignLeft
			End Select
			.Font.Size = miFontSize
			.Font.Name = msFontName
			.Font.Bold = mbBold
			.Interior.Color = miColor
			.BorderAround(, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic)
			.WrapText = True
			.NumberFormat = CellFormat
			.Value = Value
		End With
	End Sub

	Private Function ExecuteSPNoParms(StoredProcedure As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC, 2) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = StoredProcedure
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			ExecuteSPNoParms = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			ExecuteSPNoParms = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Private Function Warehouse01(MRN As String, DischargeDateTime As Date, ByRef LACEVal As Int32, HospitalService As String, Found As Boolean) As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			oCmd.CommandText = "ostl00.sDischargeSum01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@SourceMRN", OleDbType.VarChar, 40).Value = MRN
			oCmd.Parameters.Add("@DischargeDateTime", OleDbType.Date).Value = DischargeDateTime
			oCmd.Connection = moDBCWarehouse
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oRdr.Read()
				LACEVal = SetInt32(oRdr!modfLACEVal)
				HospitalService = SetString(oRdr!hosp_svc)
				Found = True
			Else
				LACEVal = 0
				HospitalService = String.Empty
			End If
			Warehouse01 = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.Complete01: " & oException.Message
			LACEVal = 0
			HospitalService = String.Empty
			Warehouse01 = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
		End Try
	End Function

	Private Function UpdateLACE(DischargeSumHSXID As Int32, LACEVal As Int32, sHospitalService As String) As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "uDischargeDashHSX01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@DischargeSumHSXID", OleDbType.BigInt).Value = DischargeSumHSXID
			oCmd.Parameters.Add("@LACEval", OleDbType.BigInt).Value = LACEVal
			oCmd.Parameters.Add("@HospSvc", OleDbType.Char, 3).Value = sHospitalService
			oCmd.Connection = moDBCEMR
			oCmd.ExecuteNonQuery()
			UpdateLACE = True
		Catch oException As Exception
			ErrorMessage = "UpdateLACE: " & oException.Message
			UpdateLACE = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function

	Private Function WriteObs(PID As Double, episode_no As String, admitdate As Date, dischargedate As Date, dstc_cd As String, dstc_cd_desc As String, hosp_svc As String, CheifComplaint As String,
														 DischargeDisposition As String, modfLACEVal As Int32) As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			oCmd.CommandText = "uDischargeDashObs01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@PID", OleDbType.Double).Value = PID
			oCmd.Parameters.Add("@episode_no", OleDbType.VarChar, 20).Value = episode_no
			oCmd.Parameters.Add("@admitdate", OleDbType.Date).Value = admitdate
			oCmd.Parameters.Add("@dischargedate", OleDbType.Date).Value = dischargedate
			oCmd.Parameters.Add("@dstc_cd", OleDbType.Char, 3).Value = dstc_cd
			oCmd.Parameters.Add("@dstc_cd_desc", OleDbType.VarChar, 40).Value = dstc_cd_desc
			oCmd.Parameters.Add("@hosp_svc", OleDbType.Char, 4).Value = hosp_svc
			oCmd.Parameters.Add("@CheifComplaint", OleDbType.VarChar, 30).Value = CheifComplaint
			oCmd.Parameters.Add("@DischargeDisposition", OleDbType.VarChar, 254).Value = DischargeDisposition
			oCmd.Parameters.Add("@modfLACEVal", OleDbType.SmallInt).Value = modfLACEVal
			oCmd.Connection = moDBCEMR
			oCmd.ExecuteNonQuery()
			WriteObs = True
		Catch oException As Exception
			ErrorMessage = "UpdateObs: " & oException.Message
			WriteObs = False
		Finally
			CloseCMD(oCmd)
		End Try
	End Function

	Public Function MoveFile(FileName As String, SourcePath As String, DestinationPath As String) As Boolean
		Dim sMoveFileName As String
		Try
			sMoveFileName = Path.GetFileNameWithoutExtension(FileName) & "_" & Format(Now, "yyyyddMM_HHmmssfff") & Path.GetExtension(FileName)
			If Not File.Exists(SourcePath & FileName) Then Throw New Exception("Source file " & SourcePath & FileName & " doesn't exist")
			If IsOpen(SourcePath & FileName) Then Throw New Exception("Source file " & SourcePath & FileName & " is open by another process")
			If Not Directory.Exists(DestinationPath) Then
				Directory.CreateDirectory(DestinationPath)
				If Not Directory.Exists(DestinationPath) Then Throw New Exception("Directory " & DestinationPath & " doesn't exist")
			End If
			If File.Exists(DestinationPath & FileName) Then
				File.Delete(DestinationPath & FileName)
				If File.Exists(DestinationPath & FileName) Then Throw New Exception("File " & DestinationPath & FileName & " already exists")
			End If
			File.Move(SourcePath & FileName, DestinationPath & sMoveFileName)
			If File.Exists(SourcePath & FileName) Or Not File.Exists(DestinationPath & sMoveFileName) Then Throw New Exception("Move file " & SourcePath & FileName & " failed")
			MoveFile = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			MoveFile = False
		Finally
		End Try
	End Function

	Private Function IsOpen(ByVal pPathFileName As String) As Boolean
		Dim oFileInfo As New FileInfo(pPathFileName)
		Dim oFileStream As FileStream = Nothing

		Try
			oFileStream = oFileInfo.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)
			IsOpen = False
		Catch oException As Exception
			ErrorMessage = oException.Message
			IsOpen = True
		Finally
			CloseFileInfo(oFileInfo)
			CloseFileStream(oFileStream)
		End Try
	End Function

	Private Function SetFinalResult(bStatusPositiveFound As Boolean, bStatusNegativeFound As Boolean, bStatusPendingFound As Boolean, bStatusCanceledFound As Boolean) As Int32
		If bStatusCanceledFound Then Return 5
		If bStatusPositiveFound Then Return 2
		If bStatusNegativeFound Then Return 3
		If bStatusPendingFound Then Return 1
		Return 4
	End Function

	Private Function SetFinalResultDateTime(OrderProcessDateTime As String, ResultCreateDateTime As String) As String
		If IsDate(ResultCreateDateTime) Then Return Format(CDate(ResultCreateDateTime), "yyyy-MM-dd HH:mm:ss")
		If IsDate(OrderProcessDateTime) Then Return Format(CDate(OrderProcessDateTime), "yyyy-MM-dd HH:mm:ss")
		Return String.Empty
	End Function

	Private Function AddCSVField(ByRef OutRecord As String, Field As String, FieldType As Int32) As Boolean
		Try
			If OutRecord.Length > 0 Then OutRecord = OutRecord & ","
			Select Case FieldType
				Case 1
					If InStr(1, Field, ",") > 0 Then
						OutRecord = OutRecord & gsDoubleQuote & Field & gsDoubleQuote
					Else
						OutRecord = OutRecord & Field
					End If
					Return True
				Case 2
					OutRecord = OutRecord & Field
					Return True
				Case 3
					If IsDate(Field) Then OutRecord = OutRecord & Format(CDate(Field), "yyyy-MM-dd HH:mm:ss")
					Return True
				Case 4
					If IsDate(Field) Then OutRecord = OutRecord & Format(SetDate(Field), "yyyy-MM-dd")
					Return True
			End Select
			Return False
		Catch oException As Exception
			Return False
		End Try
	End Function
#End Region

#Region "StartService"
	Public Sub StartService(InTextWriterTraceListener As TextWriterTraceListener)
		Dim sRoutine As String = "StartService"
		Dim oFile As StreamWriter = Nothing
		Dim oServicesToRun() As System.ServiceProcess.ServiceBase
		Dim oStartup As New StartUp
		Dim iWork As Int32

		WriteEventLog("Service main method starting at " & Format$(Now, "MM/dd/yyyy HH:mm"), sRoutine, EventLogEntryType.Information)
		If oStartup.Run Then
			CreateDirectory(gsLogPath)
			gsLogFileName = gsApplicationName & " Log " & Format$(Now, "yyyyMMdd HHmmss") & ".txt"
			oFile = File.CreateText(gsLogPath & gsLogFileName)
			InTextWriterTraceListener = New TextWriterTraceListener(oFile)
			Trace.Listeners.Add(InTextWriterTraceListener)
			Trace.AutoFlush = True
			TraceWrite("Startup.Run was initialized...", sRoutine)
			TraceWrite("Application Name: " & gsApplicationName, sRoutine)
			TraceWrite("Windows UserID: " & gsWindowsUserID, sRoutine)
			TraceWrite("Application Version: " & gsVersion, sRoutine)
			TraceWrite("Application Startup Path: " & gsAppPath, sRoutine)
			If gbTrace Then
				TraceWrite("Trace: True", sRoutine)
			Else
				TraceWrite("Trace: False", sRoutine)
			End If
			TraceWrite("Service Pause Time: " & Format$(giServicePauseTime / 60000, "#,##0.00") & " minutes", sRoutine)
			If galEMailRecipient.Count > 0 Then
				For iWork = 0 To galEMailRecipient.Count - 1
					TraceWrite("EMail Recipient(" & iWork + 1 & "): " & galEMailRecipient(iWork).ToString, sRoutine)
				Next iWork
			End If
			TraceWrite("EMail Sender: " & gsEMailSender, sRoutine)
			If gbSendEMail Then
				TraceWrite("Send EMail: True", sRoutine)
			Else
				TraceWrite("Send EMail: False", sRoutine)
			End If
			TraceWrite("Unsigned Documents Time: " & gsUnsignedDocumentsTime, sRoutine)
			TraceWrite("Report Recipient Unsigned Documents: " & gsReportRecipientUnsignedDocuments, sRoutine)
			If galUnsignedDocumentsDay.Count > 0 Then
				For iWork = 0 To galUnsignedDocumentsDay.Count - 1
					TraceWrite("Unsigned Documents Day(" & iWork + 1 & "): " & galUnsignedDocumentsDay(iWork).ToString, sRoutine)
				Next iWork
			End If

			TraceWrite("Unsigned Documents Interval: " & giUnsignedDocumentsInterval, sRoutine)
			TraceWrite("Discharge Sum Time: " & gsDischargeSumTime, sRoutine)
			TraceWrite("EMail CheckBox Script Time: " & gsDischargeSumTime, sRoutine)
			TraceWrite("Report Recipient EMail CheckBox Script: " & gsReportRecipientEMailCheckBoxScript, sRoutine)
			If galCOVID19WarehouseUpdateTime.Count > 0 Then
				For iWork = 0 To galCOVID19WarehouseUpdateTime.Count - 1
					TraceWrite("COVID-19 Warehouse Update Time(" & iWork + 1 & "): " & galCOVID19WarehouseUpdateTime(iWork).ToString, sRoutine)
				Next iWork
			End If
			If galCOVID19CharterCareUpdateTime.Count > 0 Then
				For iWork = 0 To galCOVID19CharterCareUpdateTime.Count - 1
					TraceWrite("COVID-19 Charter Care Update Time(" & iWork + 1 & "): " & galCOVID19CharterCareUpdateTime(iWork).ToString, sRoutine)
				Next iWork
			End If
			TraceWrite("COVID-19 Waterbury Input Folder: " & gsCOVID19WaterburyInputfolder, sRoutine)
			TraceWrite("COVID-19 Waterbury Output Folder: " & gsCOVID19WaterburyOutputFolder, sRoutine)
			If galCOVID19WaterburyOutputTime.Count > 0 Then
				For iWork = 0 To galCOVID19WaterburyOutputTime.Count - 1
					TraceWrite("COVID-19 Waterbury Output Time(" & iWork + 1 & "): " & galCOVID19WaterburyOutputTime(iWork).ToString, sRoutine)
				Next iWork
			End If

			TraceWrite("COVID-19 ECHN Input Folder: " & gsCOVID19ECHNInputFolder, sRoutine)
			TraceWrite("COVID-19 EOGH Input Folder: " & gsCOVID19EOGHInputFolder, sRoutine)
			TraceWrite("COVID-19 Alta Input Folder: " & gsCOVID19AltaInputFolder, sRoutine)
			If galCOVID19HANUpdateTime.Count > 0 Then
				For iWork = 0 To galCOVID19HANUpdateTime.Count - 1
					TraceWrite("COVID-19 HAN Update Time(" & iWork + 1 & "): " & galCOVID19HANUpdateTime(iWork).ToString, sRoutine)
				Next iWork
			End If
			If galCOVID19ReportTime.Count > 0 Then
				For iWork = 0 To galCOVID19ReportTime.Count - 1
					TraceWrite("COVID-19 Report Time(" & iWork + 1 & "): " & galCOVID19ReportTime(iWork).ToString, sRoutine)
				Next iWork
			End If
			If gbByPassDateCheck Then
				TraceWrite("By Pass Date Check: True", sRoutine)
			Else
				TraceWrite("By Pass Date Check: False", sRoutine)
			End If
			If galCOVID19LabReportEMail.Count > 0 Then
				For iWork = 0 To galCOVID19LabReportEMail.Count - 1
					TraceWrite("COVID-19 Lab Report eMail(" & iWork + 1 & "): " & galCOVID19LabReportEMail(iWork).ToString, sRoutine)
				Next iWork
			End If
			If galCOVID19VisiquateUpdateTime.Count > 0 Then
				For iWork = 0 To galCOVID19VisiquateUpdateTime.Count - 1
					TraceWrite("COVID-19 Visiquate Update Time(" & iWork + 1 & "): " & galCOVID19VisiquateUpdateTime(iWork).ToString, sRoutine)
				Next iWork
			End If

			If Not gbTrace Then
					gbTrace = True
					TraceWrite("Trace is disabled", sRoutine)
					gbTrace = False
				End If
				WriteEventTrace("Startup Complete", sRoutine, EventLogEntryType.Information)
				TraceWrite(StringRepeat("-", 100), sRoutine)
				TraceWrite(Space$(1), sRoutine)
			Else
				WriteEventTrace("Startup.Run failed to initialize...", sRoutine, EventLogEntryType.Error)
			WriteEventTrace(oStartup.ErrorMessage, sRoutine, EventLogEntryType.Error)
			End
		End If
		oStartup = Nothing
		oServicesToRun = New System.ServiceProcess.ServiceBase() {New EMRStatService}
		ServiceProcess.ServiceBase.Run(oServicesToRun)
		WriteEventTrace("Service main method exiting...", sRoutine, EventLogEntryType.Information)
		Trace.Listeners.Remove(InTextWriterTraceListener)
		InTextWriterTraceListener.Close()
		InTextWriterTraceListener = Nothing
		oFile.Close()
	End Sub
#End Region

#Region "EMail"
	Public Sub StartUpEMail()
		Dim sRoutine As String = "StartUpEMail"
		Dim sBodyText, sInsertText As String
		Dim iWork As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			TraceWrite("Start Send Startup eMail", sRoutine)
			If gbSendEMail Then
				If galEMailRecipient.Count > 0 Then
					oStreamReader = New StreamReader(gsHTMLPath & "Start01.html")
					sBodyText = oStreamReader.ReadToEnd
					CloseStreamReader(oStreamReader)
					sInsertText = gsApplicationName & " Startup"
					sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Status: Started on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
					sBodyText = sBodyText.Replace("[text01]", sInsertText)
					sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
					sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
					For iWork = 0 To galEMailRecipient.Count - 1
						oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "EMRStat04.png"
						If Not oEMailFunctions.Send(galEMailRecipient.Item(iWork).ToString, gsApplicationName & " Startup", sBodyText, False, sImageTag) Then
							TraceWrite("Send startup eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
						End If
					Next iWork
					TraceWrite("End Send Startup eMail", sRoutine)
				End If
			End If
		Catch oException As Exception
			TraceWrite("Send startup eMail failed: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub

	Public Sub EMailCheckBoxScriptEMail(ErrorFlag As Boolean)
		Dim sRoutine As String = "EMailCheckBoxScriptEMail"
		Dim sBodyText, sInsertText As String
		Dim dtCurrentDateTime As Date = Now
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			TraceWrite("Start Send Startup eMail", sRoutine)
			If gbSendEMail Then
				If Not ErrorFlag Then
					oStreamReader = New StreamReader(gsHTMLPath & "EMailCheckBoxScript01.html")
				Else
					oStreamReader = New StreamReader(gsHTMLPath & "EMailCheckBoxScript02.html")
				End If
				sBodyText = oStreamReader.ReadToEnd
				CloseStreamReader(oStreamReader)
				sInsertText = "Centricity eMail CheckBox Script"
				sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5)
				If Not ErrorFlag Then
					sInsertText = sInsertText & "Status: Completed on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
				Else
					sInsertText = sInsertText & "Status: Failed on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
				End If
				sBodyText = sBodyText.Replace("[text01]", sInsertText)
				sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
				sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
				oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "EMRStat04.png"
				If Not oEMailFunctions.Send(gsReportRecipientEMailCheckBoxScript, gsApplicationName & " Startup", sBodyText, False, sImageTag) Then
					TraceWrite("Send email checkbox script eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
				End If
				TraceWrite("End email checkbox script eMail eMail", sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("Send email checkbox script eMail failed: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub

	Public Sub ShutdownEMail()
		Dim sRoutine As String = "ShutdownEMail"
		Dim sBodyText, sInsertText As String
		Dim iWork As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			TraceWrite("Start Send Shutdown eMail", sRoutine)
			If gbSendEMail Then
				If galEMailRecipient.Count > 0 Then
					oStreamReader = New StreamReader(gsHTMLPath & "Shutdown01.html")
					sBodyText = oStreamReader.ReadToEnd
					CloseStreamReader(oStreamReader)
					sInsertText = gsApplicationName & " Shutdown"
					sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Status: Shutdown on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
					sBodyText = sBodyText.Replace("[text01]", sInsertText)
					sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
					sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
					For iWork = 0 To galEMailRecipient.Count - 1
						oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "EMRStat04.png"
						If Not oEMailFunctions.Send(galEMailRecipient.Item(iWork).ToString, gsApplicationName & " Shutdown", sBodyText, False, sImageTag) Then
							TraceWrite("Send shutdown eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
						End If
					Next iWork
					TraceWrite("End Send Shutdown eMail", sRoutine)
				End If
			End If
		Catch oException As Exception
			TraceWrite("Send shutdown eMail failed: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub

	Public Sub Error01EMail()
		Dim sRoutine As String = "Error01EMail"
		Dim sBodyText, sInsertText As String
		Dim iWork As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim oEMailFunctions As New EMailFunctions
		Dim oStreamReader As StreamReader = Nothing
		Dim sImageTag As String = Format$(Now, "yyyyMMddHHmmss")

		Try
			TraceWrite("Start Send Error01 eMail", sRoutine)
			If gbSendErrorEMail Then
				If galEMailRecipient.Count > 0 Then
					oStreamReader = New StreamReader(gsHTMLPath & "Error01.html")
					sBodyText = oStreamReader.ReadToEnd
					CloseStreamReader(oStreamReader)
					sInsertText = gsApplicationName & " Error"
					sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Status: Error occurred on " & MonthName(Month(dtCurrentDateTime)) & " " & Day(dtCurrentDateTime) & ", " & Year(dtCurrentDateTime) & " at " & Format(dtCurrentDateTime, "HH:mm")
					sInsertText = sInsertText & ASPCRLF(2) & ASPSpace(5) & "Detail: " & gsErrorMessage
					sBodyText = sBodyText.Replace("[text01]", sInsertText)
					sBodyText = sBodyText.Replace("[donotreply01]", oEMailFunctions.DoNoTReply)
					sBodyText = sBodyText.Replace("[imagetag]", sImageTag)
					For iWork = 0 To galEMailRecipient.Count - 1
						oEMailFunctions.Logo01PathFileName = gsGraphicsPath & "EMRStat04.png"
						If Not oEMailFunctions.Send(galEMailRecipient.Item(iWork).ToString, gsApplicationName & " Error", sBodyText, False, sImageTag) Then
							TraceWrite("Send error01 eMail failed: " & oEMailFunctions.ErrorMessage, sRoutine)
						End If
					Next iWork
					TraceWrite("End Send Startup eMail", sRoutine)
				End If
			End If
		Catch oException As Exception
			TraceWrite("Send Error01 eMail failed: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub
#End Region

#Region "Unsigned Documents Routines"
	Public Sub UnsignedDocuments()
		Dim sRoutine As String = "UnsignedDocuments"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 1
		Dim iWork As Int32
		Dim bProcessed As Boolean = False

		Try
			TraceWrite("Start Unsigned Documents processing", sRoutine)
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If CDate(sCurrentDate & " " & gsUnsignedDocumentsTime) <= dtCurrentDateTime Then
				If galUnsignedDocumentsDay.Count <= 0 Then Throw New Exception("No processing days found.")
				For iWork = 0 To galUnsignedDocumentsDay.Count - 1
					If oProcessed.UnsignedDocumentsRequired(CInt(galUnsignedDocumentsDay.Item(iWork).ToString)) Then
						If Not oProcessed.Complete01(iProcessTypeID, sCurrentDate) Or gbByPassDateCheck Then
							bProcessed = True
							If Not CreateUnsignedDocuments() Then Throw New Exception("Error processing Unsigned Documents")
							RemovePastUnsignedDocuments()
							TraceWrite("End Unsigned Documents processing", sRoutine)
							If Not gbByPassDateCheck Then oProcessed.Insert01(iProcessTypeID, sCurrentDate)
						Else
							TraceWrite("Processing already completed for: " & sCurrentDate & " " & gsUnsignedDocumentsTime, sRoutine)
						End If
					End If
				Next iWork
				If Not bProcessed Then TraceWrite("Processing is not required.", sRoutine)
				TraceWrite("End Unsigned Documents processing", sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("ServiceRoutines.UnsignedDocuments Error: " & oException.Message, sRoutine)
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Private Function CreateUnsignedDocuments() As Boolean
		Dim sRoutine As String = "CreateUnsignedDocuments"
		Dim sFileName, sPathFileName, sEMailMessage, sSubject As String
		Dim iSpreadsheet As Int32 = 0
		Dim oFileInfo As IO.FileInfo
		Dim oDirectoryInfo As IO.DirectoryInfo
		Dim oSMTPEMail As New SMTPEMail.Mail
		Dim dtCurrentDateTime As Date = Now
		Dim sDebugStep As String = vbNullString

		TraceWrite("Processing", sRoutine)
		sFileName = "EMR Unsigned Documents " & Format(dtCurrentDateTime, "yyyy-MM-dd HH-mm-ss") & ".xlsx"
		sPathFileName = AddBackslash(gsBuildPath) & sFileName
		sSubject = "EMR Unsigned Documents"

		Try
			KillExcel()
			sDebugStep = "moExcelApplication = New Excel.Application"
			moExcelApplication = New Excel.Application
			sDebugStep = "moExcelApplication.SheetsInNewWorkbook = 2"
			moExcelApplication.SheetsInNewWorkbook = 2
			sDebugStep = "moWorkbook = moExcelApplication.Workbooks.Add"
			moWorkbook = moExcelApplication.Workbooks.Add
			sDebugStep = vbNullString

			iSpreadsheet += 1
			If Not UnsignedDocumentsSite(0, "CKHN", iSpreadsheet) Then Throw New Exception("Error: Processing site code 0")
			iSpreadsheet += 1
			If Not UnsignedDocumentsSite(1, "Comm EMR", iSpreadsheet) Then Throw New Exception("Error: Processing site code 1")
			TraceWrite("Start Spreadsheet Save - Path/File Name: " & sPathFileName, sRoutine)
			oDirectoryInfo = New IO.DirectoryInfo(gsBuildPath)
			If Not oDirectoryInfo.Exists Then oDirectoryInfo.Create()
			oDirectoryInfo = New IO.DirectoryInfo(gsBuildPath)
			If Not oDirectoryInfo.Exists Then Throw New Exception("Error: Directory " & gsBuildPath & " doesn't exist")
			oDirectoryInfo = Nothing
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then Throw New Exception("Error: File " & sPathFileName & " already exists")
			oFileInfo = Nothing
			TraceWrite("SaveCopyAs Statement - Path/File Name: " & sPathFileName, sRoutine)
			moWorkbook.SaveCopyAs(sPathFileName)
			TraceWrite("Complete Spreadsheet Save", sRoutine)

			If gbSendEMail Then
				If Not oSMTPEMail.Recipient(gsReportRecipientUnsignedDocuments) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.Subject(sSubject) Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
				sEMailMessage = "Attached is your EMR Unsigned Documents spreadsheet for " & MonthName(Month(dtCurrentDateTime)) & Space(1) & Format(dtCurrentDateTime, "dd, yyyy") & "<br />"
				sEMailMessage = sEMailMessage & "<br />" & "<br />" & Disclaimer() & "<br />"
				If Not oSMTPEMail.Message(sEMailMessage) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.AddAttachment(sPathFileName) Then Throw New Exception("AddAttachment EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
				Thread.Sleep(giEMailPauseTime)
			End If

			CreateUnsignedDocuments = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			If Len(sDebugStep) > 0 Then TraceWrite("Debug Step: " & sDebugStep, sRoutine)
			oFileInfo = New IO.FileInfo(sPathFileName)
			If oFileInfo.Exists Then DeleteFile(sPathFileName)
			CreateUnsignedDocuments = False
		Finally
			GC.Collect()
			GC.WaitForPendingFinalizers()
			ReleaseObject(moWorkSheet)
			ReleaseObject(moWorkbook)
			CloseWorkbook()
			moExcelApplication.Quit()
			ReleaseObject(moExcelApplication)
			moExcelApplication = Nothing
		End Try
	End Function

	Private Function UnsignedDocumentsSite(SiteCode As Int32, SiteName As String, Spreadsheet As Int32) As Boolean
		Dim sRoutine As String = "UnsignedDocumentsSite"
		Dim dtCurrentDateTime As Date = Now
		Dim oDBC As OracleConnection = New OracleConnection
		Dim oCmd As OracleCommand = New OracleCommand
		Dim oRdr As OracleDataReader = Nothing
		Dim iRecordCount As Int32

		TraceWrite("Start Processing SiteCode: " & SiteCode & ", SiteName: " & SiteName, sRoutine)
		UnsignedDocumentsSite = False
		Try
			miColor = RGB(255, 255, 255)
			moWorkSheet = DirectCast(moWorkbook.Sheets(Spreadsheet), Excel.Worksheet)
			miFontSize = 11
			msFontName = "Calibri"
			miRow = 0

			With moWorkSheet.PageSetup
				.Orientation = Excel.XlPageOrientation.xlLandscape
				.PrintGridlines = True
				.LeftMargin = 0
				.RightMargin = 0
				.CenterHorizontally = True
				.PrintTitleRows = "$1:$3"
				.PaperSize = Excel.XlPaperSize.xlPaperLegal
				.Zoom = False
				.FitToPagesTall = 1
				.FitToPagesWide = 1

			End With
			moWorkSheet.Name = SiteName
			SetColumn("A", 51.0)
			SetColumn("B", 12.0)
			SetColumn("C", 21.0)
			SetColumn("D", 19.0)
			SetColumn("E", 30.0)
			SetColumn("F", 12.0)
			SetColumn("G", 70.0)
			SetColumn("H", 6.0)
			SetColumn("I", 80.0)
			SetColumn("J", 8.0)
			mbBold = True

			'row 1
			miRow += 1
			miFontSize = 14
			mbBold = True
			SheetHeading(SiteName & " Unsigned Documents", "A1", "J1", "C", RGB(255, 255, 255))

			'row 2
			miRow += 1
			miFontSize = 10
			RowHeight(15.0)
			mbBold = True
			SheetHeading(MonthName(Month(dtCurrentDateTime)) & Space(1) & Format(Day(dtCurrentDateTime), "#0") & ", " & Format(dtCurrentDateTime, "yyyy"), "A2", "J2", "C", RGB(255, 255, 255))

			'row 3
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("Patient Name", "A", "L")
			ValueCell("Date of Birth", "B", "C")
			ValueCell("Provider Last Name", "C", "L")
			ValueCell("Provider First Name", "D", "L")
			ValueCell("Description", "E", "L")
			ValueCell("Date", "F", "C")
			ValueCell("Summary", "G", "L")
			ValueCell("Status", "H", "C")
			ValueCell("Practice", "I", "L")
			ValueCell("Visit ID", "J", "C")

			'detail
			miColor = RGB(255, 255, 255)
			miFontSize = 11
			RowHeight(15.0)
			mbBold = False
			If Not OpenDBOracle(oDBC, SiteCode) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "SELECT PERSON.SEARCHNAME, PERSON.DATEOFBIRTH, USR.LASTNAME, USR.FIRSTNAME, DOCTYPES.DESCRIPTION, DOCUMENT.DB_CREATE_DATE, DOCUMENT.SUMMARY, DOCUMENT.STATUS, LOCREG.SEARCHNAME As PRACTICE, DOCUMENT.VISITID " &
				"From DOCUMENT " &
				"INNER JOIN PERSON ON DOCUMENT.PID = PERSON.PID " &
				"INNER JOIN USR  ON DOCUMENT.USRID = USR.PVID " &
				"INNER JOIN DOCTYPES ON DOCUMENT.DOCTYPE = DOCTYPES.DTID " &
				"INNER JOIN LOCREG ON DOCUMENT.LOCOFCARE = LOCREG.LOCID " &
				"WHERE DOCUMENT.STATUS = 'H' "
			If SiteCode = 1 Then oCmd.CommandText = oCmd.CommandText & "AND LOCREG.ABBREVNAME LIKE '%BH01%' "
			oCmd.CommandText = oCmd.CommandText & "ORDER BY USR.LASTNAME, DOCUMENT.DB_CREATE_DATE ASC"
			oCmd.CommandType = CommandType.Text
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				Do While oRdr.Read
					iRecordCount += 1
					miRow += 1
					ValueCell(SetString(oRdr!SEARCHNAME), "A", "L")
					If IsDate(SetString(oRdr!DATEOFBIRTH)) Then
						ValueCell(Format(SetDate(oRdr!DATEOFBIRTH), "MM/dd/yyyy"), "B", "C")
					Else
						ValueCell(String.Empty, "B", "C")
					End If
					ValueCell(SetString(oRdr!LASTNAME), "C", "L")
					ValueCell(SetString(oRdr!FIRSTNAME), "D", "L")
					ValueCell(SetString(oRdr!DESCRIPTION), "E", "L")
					If IsDate(SetString(oRdr!DB_CREATE_DATE)) Then
						ValueCell(Format(SetDate(oRdr!DB_CREATE_DATE), "MM/dd/yyyy"), "F", "C")
					Else
						ValueCell(String.Empty, "F", "C")
					End If
					ValueCell(SetString(oRdr!SUMMARY), "G", "L")
					ValueCell(SetString(oRdr!STATUS), "H", "C")
					ValueCell(SetString(oRdr!PRACTICE), "I", "L")
					ValueCell(SetString(oRdr!VISITID), "J", "C")

					'test
					'If iRecordCount >= 10 Then Exit Do
					'test

				Loop
			End If
			UnsignedDocumentsSite = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			UnsignedDocumentsSite = False
		Finally
			CloseRDROracle(oRdr)
			CloseCMDOracle(oCmd)
			CloseDBOracle(oDBC)
			ReleaseObject(moWorkSheet)
			TraceWrite("Complete Processing SiteCode: " & SiteCode & ", SiteName: " & SiteName, sRoutine)
		End Try
	End Function

	Public Sub RemovePastUnsignedDocuments()
		Dim sRoutine As String = "RemovePastUnsignedDocuments"
		Dim dtCurrentDateTime As Date = Now
		Dim sFilePattern As String = "EMR Unsigned Documents*.xlsx"
		Dim oFiles() As String = Directory.GetFiles(gsBuildPath, sFilePattern)
		Dim oFileName As String
		Dim dtCreateDateTime As Date
		Dim iRemoveCount As Int32 = 0

		Try
			TraceWrite("Start Remove Past Unsigned Documents processing", sRoutine)
			TraceWrite("Checking: " & AddBackslash(gsBuildPath) & sFilePattern, sRoutine)
			If oFiles.Count > 0 Then
				For Each oFileName In oFiles
					dtCreateDateTime = File.GetCreationTime(oFileName)
					If DateDiff(DateInterval.Day, dtCreateDateTime, dtCurrentDateTime) > giHistoryDays Then
						If File.Exists(oFileName) Then
							DeleteFile(oFileName)
							If Not File.Exists(oFileName) Then iRemoveCount += 1
						End If
					End If
				Next
				TraceWrite("Removed " & Format(iRemoveCount, "#,##0") & " files.", sRoutine)
			Else
				TraceWrite("No files found.", sRoutine)
			End If
			TraceWrite("End Remove Past Unsigned Documents processing", sRoutine)
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub
#End Region

#Region "Discharge Sum"
	Public Sub DischargeSum()
		Dim sRoutine As String = "DischargeSum"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String = Format$(dtCurrentDateTime, "MM/dd/yyyy")
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 2

		Try
			If CDate(sCurrentDate & " " & gsDischargeSumTime) <= dtCurrentDateTime Then
				If Not oProcessed.Complete01(iProcessTypeID, sCurrentDate) Then
					TraceWrite("Start Discharge Sum processing", sRoutine)
					If Not CreateDischargeSum() Then Throw New Exception("Error processing Discharge Sum")
					TraceWrite("End Discharge Sum processing", sRoutine)
					oProcessed.Insert01(iProcessTypeID, sCurrentDate)
				Else
					TraceWrite("Processing already completed for: " & sCurrentDate & " " & gsDischargeSumTime, sRoutine)
				End If
				Else
				TraceWrite("Processing is not required.", sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("ServiceRoutines.DischargeSum Error: " & oException.Message, sRoutine)
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Private Function CreateDischargeSum() As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim bError As Boolean = False

		If Not ExecuteSPNoParms("uDischargeHSX01") Then Return False
		If Not ExecuteSPNoParms("uDischargeHSX02") Then
			ExecuteSPNoParms("uDischargeHSX01")
			Return False
		End If
		If Not ExecuteSPNoParms("uDischargeHSX03") Then
			ExecuteSPNoParms("uDischargeHSX01")
			Return False
		End If
		'If Not UpdateWH01() Then
		'	ExecuteSPNoParms("uDischargeHSX01")
		'	Return False
		'End If
		If Not ExecuteSPNoParms("uDischargeHSX04") Then
			ExecuteSPNoParms("uDischargeHSX01")
			Return False
		End If
		If Not UpdateObs01() Then
			ExecuteSPNoParms("uDischargeHSX01")
			Return False
		End If

		Return True
	End Function

	Private Function UpdateWH01() As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iDischargeSumHSXID As Int32
		Dim sSourceMRN As String
		Dim iLACEVal As Int32 = 0
		Dim sHospitalService As String = String.Empty
		Dim dtDischargeDateTime As Date
		Dim bFound As Boolean = False
		Dim iRecordCount As Int32 = 0

		UpdateWH01 = False
		Try
			If Not OpenDB(moDBCWarehouse, 3) Then Throw New Exception("Error opening Warehouse database.")
			If Not OpenDB(moDBCEMR, 2) Then Throw New Exception("Error opening EMR database.")
			oCmd.CommandText = "sDischargeDashHSX01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Connection = moDBCEMR
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				Do While oRdr.Read()
					iRecordCount += 1
					iDischargeSumHSXID = SetInt32(oRdr!DischargeDashHSXID)
					sSourceMRN = SetString(oRdr!SourceMRN)
					dtDischargeDateTime = CDate(SetString(oRdr!DischargeDate) & Space(1) & SetString(oRdr!DischargeTime))
					If Not Warehouse01(sSourceMRN, dtDischargeDateTime, iLACEVal, sHospitalService, bFound) Then Throw New Exception("Error getting LACE data from the warehouse.")
					If bFound Then If Not UpdateLACE(iDischargeSumHSXID, iLACEVal, sHospitalService) Then Throw New Exception("Error updating LACE value in EMR.")
					If iRecordCount Mod 1000 = 0 Then TraceWrite("Records Read: " & iRecordCount, "UpdateWH01")
				Loop
			End If
			UpdateWH01 = True
		Catch oException As Exception
			ErrorMessage = "UpdateWH01: " & oException.Message
			UpdateWH01 = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(moDBCEMR)
			CloseDB(moDBCWarehouse)
		End Try
	End Function

	Private Function UpdateObs01() As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordCount As Int32 = 0

		UpdateObs01 = False
		Try
			If Not OpenDB(moDBCEMR, 2) Then Throw New Exception("Error opening EMR database.")
			If Not OpenDB(moDBCWarehouse, 3) Then Throw New Exception("Error opening Warehouse database.")
			oCmd.CommandText = "ostl00.sDischargeSum02"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Connection = moDBCWarehouse
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				Do While oRdr.Read()
					iRecordCount += 1
					If Not WriteObs(SetDouble(oRdr!PID), SetString(oRdr!episode_no), CDate(SetString(oRdr!admitdate)), CDate(SetString(oRdr!dischargedate)), SetString(oRdr!dstc_cd), SetString(oRdr!dstc_cd_desc),
													 SetString(oRdr!hosp_svc), SetString(oRdr!CheifComplaint), SetString(oRdr!DischargeDisposition), SetInt32(oRdr!modfLACEVal)) Then Throw New Exception("Error updating Obs value in EMR.")
					If iRecordCount Mod 1000 = 0 Then TraceWrite("Records Read: " & iRecordCount, "UpdateWH01")
				Loop
			End If
			UpdateObs01 = True
		Catch oException As Exception
			ErrorMessage = "UpdateObs01: " & oException.Message
			UpdateObs01 = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(moDBCWarehouse)
			CloseDB(moDBCEMR)
		End Try
	End Function
#End Region

#Region "EMailCheckBoxScript"
	Public Sub EMailCheckBoxScript()
		Dim sRoutine As String = "EMailCheckBoxScript"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String = Format$(dtCurrentDateTime, "MM/dd/yyyy")
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 3

		Try
			If CDate(sCurrentDate & " " & gsEMailCheckBoxScriptTime) <= dtCurrentDateTime Then
				If Not oProcessed.Complete01(iProcessTypeID, sCurrentDate) Then
					TraceWrite("Start EMail CheckBox Script processing", sRoutine)
					'If Not EMailCheckBoxScriptRun(1) Then Throw New Exception("Error processing Mail CheckBox Script 1")
					'If Not EMailCheckBoxScriptRun(2) Then Throw New Exception("Error processing Mail CheckBox Script 2")
					If Not EMailCheckBoxScriptRun(3) Then Throw New Exception("Error processing Mail CheckBox Script 3")
					'EMailCheckBoxScriptEMail(False)
					TraceWrite("End EMail CheckBox Script processing", sRoutine)
					'oProcessed.Insert01(iProcessTypeID, sCurrentDate)
				Else
					TraceWrite("Processing already completed for: " & sCurrentDate & " " & gsDischargeSumTime, sRoutine)
				End If
			Else
				TraceWrite("Processing is not required.", sRoutine)
			End If
		Catch oException As Exception
			'EMailCheckBoxScriptEMail(True)
			TraceWrite("ServiceRoutines.EMailCheckBoxScript Error: " & oException.Message, sRoutine)
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Private Function EMailCheckBoxScriptRun(ScriptNumber As Int32) As Boolean
		Dim sRoutine As String = "EMailCheckBoxScriptRun"
		Dim oDBC As OracleConnection = New OracleConnection
		Dim oCmd As OracleCommand = New OracleCommand

		Try
			TraceWrite("Start EMail CheckBox Script Run Script " & ScriptNumber.ToString & " of 2", sRoutine)
			If Not OpenDBOracle(oDBC, 0) Then Throw New Exception("Error opening database.")
			'oCmd.CommandText = EMailCheckBoxScriptText(ScriptNumber)
			oCmd.CommandType = CommandType.Text
			oCmd.CommandText = EMailCheckBoxScriptText(ScriptNumber)
			'oCmd.CommandText = oCmd.CommandText.Replace("\r\n", "\n")
			'; ALTER TABLE MUActivityLog ENABLE ALL TRIGGERS"
			oCmd.Connection = oDBC
			'oCmd.ExecuteNonQueryAsync()
			oCmd.ExecuteNonQuery()
			EMailCheckBoxScriptRun = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			EMailCheckBoxScriptRun = False
		Finally
			CloseCMDOracle(oCmd)
			CloseDBOracle(oDBC)
			TraceWrite("Complete EMail CheckBox Script Run Script " & ScriptNumber.ToString & " of 2", sRoutine)
		End Try
	End Function

	Private Function EMailCheckBoxScriptText(ScriptNumber As Int32) As String
		Dim oStreamReader As StreamReader
		Select Case ScriptNumber
			Case 2
				Return File.OpenText(AddBackslash(gsScriptPath) & "EMR_Script2_PatientDataAccess.sql").ReadToEnd
			Case 3
				oStreamReader = New StreamReader(AddBackslash(gsScriptPath) & "EMR_Script3_PatientDataAccess.sql")
				Return oStreamReader.ReadToEnd
			Case Else
				Return File.OpenText(AddBackslash(gsScriptPath) & "EMR_Script1_PatientDataAccess.sql").ReadToEnd
		End Select
		'If ScriptNumber = 1 Then
		'	Dim oStreamReader As New StreamReader(AddBackslash(gsScriptPath) & "EMR_Script1_PatientDataAccess.sql")
		'	Return oStreamReader.ReadToEnd
		'Else
		'	Return File.OpenText(AddBackslash(gsScriptPath) & "EMR_Script2_PatientDataAccess.sql").ReadToEnd
		'End If
	End Function
#End Region

#Region "COVID19WarehouseUpdate"
	Public Sub COVID19WarehouseUpdate()
		Dim sRoutine As String = "COVID19WarehouseUpdate"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 4
		Dim iWork As Int32

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19WarehouseUpdateTime.Count <= 0 Then Exit Sub
			For iWork = 0 To galCOVID19WarehouseUpdateTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19WarehouseUpdateTime.Item(iWork).ToString) <= dtCurrentDateTime Then
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19WarehouseUpdateTime.Item(iWork).ToString) Or gbByPassDateCheck Then
						TraceWrite("Start COVID-19 warehouse update", sRoutine)
						If Not COVID19WarehouseProcessing() Then Throw New Exception(ErrorMessage)
						If Not COVID19WarehouseFinalResult() Then Throw New Exception(ErrorMessage)
						TraceWrite("End COVID-19 warehouse update", sRoutine)
						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19WarehouseUpdateTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: " & sCurrentDate & " " & galCOVID19WarehouseUpdateTime.Item(iWork).ToString, sRoutine)
					End If
				Else
					TraceWrite("Processing is not required for " & galCOVID19WarehouseUpdateTime.Item(iWork).ToString & ".", sRoutine)
				End If
			Next iWork
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19WarehouseUpdate Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19WarehouseProcessing() As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 1
		Dim sGroupKey As String = Guid.NewGuid.ToString()

		COVID19WarehouseProcessing = False
		Try
			If Not OpenDB(oDBC, 3) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "ostl00.sCOVID19PatientList"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				While oRdr.Read()
					oCOVID19Patient.NewPatient()
					oCOVID19Patient.PatientSourceID = iPatientSourceID
					oCOVID19Patient.LastName = SetString(oRdr!last_name)
					oCOVID19Patient.FirstName = SetString(oRdr!first_name)
					oCOVID19Patient.MiddleName = SetString(oRdr!middle_name)
					oCOVID19Patient.Gender = SetString(oRdr!gender_cd)
					oCOVID19Patient.DateOfBirth = SetDate(oRdr!birth_dtime)
					oCOVID19Patient.ZipCode = SetString(oRdr!postal_cd)
					oCOVID19Patient.DistrictCode = SetString(oRdr!dstc_cd)
					oCOVID19Patient.PatientID = SetString(oRdr!pt_id)
					oCOVID19Patient.PatientIDStartDateTime = SetDate(oRdr!pt_id_start_dtime)
					oCOVID19Patient.MedicalRecordNumber = SetString(oRdr!med_rec_no)
					oCOVID19Patient.NHSIDNumber = SetString(oRdr!nhs_id_no)
					oCOVID19Patient.AdmitDateTime = SetDate(oRdr!vst_start_dtime)
					oCOVID19Patient.DischargeDateTime = SetDate(oRdr!vst_end_dtime)
					oCOVID19Patient.OrderCode = SetString(oRdr!svc_cd)
					oCOVID19Patient.OrderDescription = SetString(oRdr!svc_desc)
					oCOVID19Patient.OrderLocation = SetString(oRdr!ord_loc)
					oCOVID19Patient.OrderingPhysicanNumber = SetString(oRdr!pty_cd)
					oCOVID19Patient.OrderingPhysicianName = SetString(oRdr!pty_name)
					oCOVID19Patient.ResultCode = SetString(oRdr!obsv_cd)
					oCOVID19Patient.ResultCodeName = SetString(oRdr!obsv_cd_name)
					oCOVID19Patient.ResultCreateDateTime = SetDate(oRdr!obsv_cre_dtime)
					oCOVID19Patient.ResultDisplay = SetString(oRdr!dsply_val)
					oCOVID19Patient.ResultText = SetString(oRdr!obsv_text_line1)
					oCOVID19Patient.FinalResultID = 0
					oCOVID19Patient.FinalResultDateTime = String.Empty
					oCOVID19Patient.OrderStatusCode = SetInt32(oRdr!sts_no).ToString
					oCOVID19Patient.OrderStatus = SetString(oRdr!ord_sts)
					oCOVID19Patient.DischargeDisposition = SetString(oRdr!dsch_disp_desc)
					oCOVID19Patient.VisitType = SetString(oRdr!vst_type_cd)
					oCOVID19Patient.HospitalService = SetString(oRdr!hosp_svc)
					oCOVID19Patient.PatientType = SetString(oRdr!pt_type)
					oCOVID19Patient.OrderProcessDateTime = SetDate(oRdr!ord_sts_prcs_dtime)
					oCOVID19Patient.OrderNumber = SetInt32(oRdr!ord_no)
					oCOVID19Patient.GroupKey = sGroupKey

					If oCOVID19Patient.ResultCode = "60394" AndAlso oCOVID19Patient.ResultDisplay.Length <= 0 Then oCOVID19Patient.ResultDisplay = oCOVID19Patient.ResultText
					If (oCOVID19Patient.OrderStatusCode = "65" Or oCOVID19Patient.OrderStatusCode = "63") AndAlso oCOVID19Patient.ResultDisplay.Length <= 0 AndAlso
						oCOVID19Patient.ResultText.Length > 0 AndAlso InStr(1, oCOVID19Patient.ResultText.ToLower, "negative") > 0 Then oCOVID19Patient.ResultDisplay = "Negative"
					If (oCOVID19Patient.OrderStatusCode = "65" Or oCOVID19Patient.OrderStatusCode = "63") AndAlso oCOVID19Patient.ResultDisplay.Length <= 0 AndAlso
							oCOVID19Patient.ResultText.Length > 0 AndAlso InStr(1, oCOVID19Patient.ResultText.ToLower, "positive") > 0 Then oCOVID19Patient.ResultDisplay = "positive"
					If (oCOVID19Patient.OrderStatusCode = "65" Or oCOVID19Patient.OrderStatusCode = "63") AndAlso oCOVID19Patient.ResultDisplay.Length <= 0 AndAlso
							oCOVID19Patient.ResultText.Length > 0 AndAlso InStr(1, oCOVID19Patient.ResultText.ToLower, "not detected") > 0 Then oCOVID19Patient.ResultDisplay = "Not Detected"
					If (oCOVID19Patient.OrderStatusCode = "65" Or oCOVID19Patient.OrderStatusCode = "63") AndAlso oCOVID19Patient.ResultDisplay.Length <= 0 AndAlso
							oCOVID19Patient.ResultText.Length > 0 AndAlso InStr(1, oCOVID19Patient.ResultText.ToLower, "detected") > 0 Then oCOVID19Patient.ResultDisplay = "Detected"

					If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				End While
				If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				oCOVID19Patient.Close()
				COVID19WarehouseProcessing = True
			End If
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19WarehouseProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function COVID19WarehouseFinalResult() As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim oCOVID19Patient As New COVID19Patient
		Dim iCOVID19PatientID, iCOVID19PatientIDHold, iOrderNumber, iOrderNumberHold, iOrderStatusCode As Int32
		Dim sPatientID, sPatientIDHold, sOrderStatusCode, sResultDisplay, sResultCreateDateTime, sOrderProcessDateTime, sWork As String
		Dim bStatusPendingFound, bStatusPositiveFound, bStatusNegativeFound, bStatusCanceledFound, bProcessLast As Boolean

		COVID19WarehouseFinalResult = False
		Try
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sCOVID19WarehouseFinalResult"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			bProcessLast = False
			If oRdr.HasRows Then
				If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				If Not oCOVID19Patient.FinalResultWarehouseInit() Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				iCOVID19PatientIDHold = -1
				sPatientIDHold = String.Empty
				iOrderNumberHold = -1
				bStatusPendingFound = False
				bStatusPositiveFound = False
				bStatusNegativeFound = False
				bStatusCanceledFound = False
				sResultCreateDateTime = String.Empty
				sOrderProcessDateTime = String.Empty
				bProcessLast = True
				While oRdr.Read()
					iCOVID19PatientID = SetInt32(oRdr!COVID19PatientID)
					sPatientID = SetString(oRdr!PatientID)

					'If sPatientID = "060016608378" Then
					'	Dim iwork As Int32 = 0
					'End If


					iOrderNumber = SetInt32(oRdr!OrderNumber)
					sOrderStatusCode = SetString(oRdr!OrderStatusCode)
					sResultDisplay = SetString(oRdr!ResultDisplay)
					If sPatientID <> sPatientIDHold OrElse iOrderNumber <> iOrderNumberHold Then
						If iCOVID19PatientIDHold <> -1 Then If Not oCOVID19Patient.FinalResultWarehouse(iCOVID19PatientIDHold, SetFinalResult(bStatusPositiveFound, bStatusNegativeFound, bStatusPendingFound, bStatusCanceledFound),
																																														SetFinalResultDateTime(sOrderProcessDateTime, sResultCreateDateTime)) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
						iCOVID19PatientIDHold = iCOVID19PatientID
						sPatientIDHold = sPatientID
						iOrderNumberHold = iOrderNumber
						bStatusPendingFound = False
						bStatusPositiveFound = False
						bStatusNegativeFound = False
						bStatusCanceledFound = False
						sResultCreateDateTime = String.Empty
						sOrderProcessDateTime = String.Empty
					End If
					iOrderStatusCode = SetInt32(sOrderStatusCode)
					Select Case iOrderStatusCode
						Case 20, 41
							bStatusPendingFound = True
						Case 63, 65
							Select Case sResultDisplay.ToLower
								Case "positive", "detected", "pos"
									bStatusPositiveFound = True
								Case "negative", "not detected", "neg"
									bStatusNegativeFound = True
							End Select
						Case 80
							bStatusCanceledFound = True
					End Select
					sWork = SetString(oRdr!ResultCreateDateTime)
					If IsDate(sWork) Then sResultCreateDateTime = sWork
					sWork = SetString(oRdr!OrderProcessDateTime)
					If IsDate(sWork) Then sOrderProcessDateTime = sWork
				End While
				If bProcessLast Then
					If iCOVID19PatientIDHold <> -1 Then If Not oCOVID19Patient.FinalResultWarehouse(iCOVID19PatientIDHold, SetFinalResult(bStatusPositiveFound, bStatusNegativeFound, bStatusPendingFound, bStatusCanceledFound),
																																													SetFinalResultDateTime(sOrderProcessDateTime, sResultCreateDateTime)) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				End If
				oCOVID19Patient.Close()
				If Not oCOVID19Patient.FinalResultByPatientSource(String.Empty, 1) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				COVID19WarehouseFinalResult = True
			End If
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19WarehouseFinalResult: " & oException.Message
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "COVID19CharterCareUpdate"
	Public Sub COVID19CharterCareUpdate()
		Dim sRoutine As String = "COVID19CharterCareUpdate"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 6
		Dim iWork As Int32

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19CharterCareUpdateTime.Count <= 0 Then Exit Sub
			For iWork = 0 To galCOVID19CharterCareUpdateTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19CharterCareUpdateTime.Item(iWork).ToString) <= dtCurrentDateTime Then
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19CharterCareUpdateTime.Item(iWork).ToString) Or gbByPassDateCheck Then
						TraceWrite("Start COVID-19 Charter Care update", sRoutine)
						If Not COVID19CharterCareProcessing() Then Throw New Exception("Error COVID-19 Charter Care update")
						TraceWrite("End COVID-19 Charter Care update", sRoutine)
						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19CharterCareUpdateTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: " & sCurrentDate & " " & galCOVID19CharterCareUpdateTime.Item(iWork).ToString, sRoutine)
					End If
				Else
					TraceWrite("Processing is not required for " & galCOVID19CharterCareUpdateTime.Item(iWork).ToString & ".", sRoutine)
				End If
			Next iWork
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19CharterCareUpdate Error: " & oException.Message & "/" & ErrorMessage, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19CharterCareProcessing() As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 3
		Dim sGroupKey As String = Guid.NewGuid.ToString()

		COVID19CharterCareProcessing = False
		Try
			If Not OpenDB(oDBC, 4) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "Clinical.sp_CovidResults"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Parameters.Add("@startDate", OleDbType.Date).Value = CDate("01/01/2020")
			oCmd.Parameters.Add("@endDate", OleDbType.Date).Value = CDate("12/31/2020")
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				While oRdr.Read()
					oCOVID19Patient.NewPatient()
					oCOVID19Patient.PatientSourceID = iPatientSourceID
					oCOVID19Patient.LastName = SetString(oRdr!LastName)
					oCOVID19Patient.FirstName = SetString(oRdr!FirstName)
					oCOVID19Patient.MiddleName = SetString(oRdr!MiddleName)
					oCOVID19Patient.Gender = SetString(oRdr!Gender)
					oCOVID19Patient.DateOfBirth = SetDate(oRdr!DateOfBirth)
					oCOVID19Patient.ZipCode = SetString(oRdr!ZipCode)
					oCOVID19Patient.DistrictCode = SetString(oRdr!DistrictCode)
					oCOVID19Patient.PatientID = SetString(oRdr!PatientID)
					oCOVID19Patient.PatientIDStartDateTime = SetDate(oRdr!PatientIDStartDateTime)
					oCOVID19Patient.MedicalRecordNumber = SetString(oRdr!MedicalRecordNumber)
					oCOVID19Patient.NHSIDNumber = SetString(oRdr!NHSIDNumber)
					oCOVID19Patient.AdmitDateTime = SetDate(oRdr!AdmitDateTime)
					oCOVID19Patient.DischargeDateTime = SetDate(oRdr!DischargeDateTime)
					oCOVID19Patient.OrderCode = SetString(oRdr!OrderCode)
					oCOVID19Patient.OrderDescription = SetString(oRdr!OrderDescription)
					oCOVID19Patient.OrderLocation = SetString(oRdr!OrderLocation)
					oCOVID19Patient.OrderingPhysicanNumber = SetString(oRdr!OrderingPhysicianNumber)
					oCOVID19Patient.OrderingPhysicianName = SetString(oRdr!OrderingPhysicianName)
					oCOVID19Patient.ResultCode = SetString(oRdr!ResultCode)
					oCOVID19Patient.ResultCodeName = SetString(oRdr!ResultCodeName)
					oCOVID19Patient.ResultCreateDateTime = SetDate(oRdr!ResultCreateDateTime)
					oCOVID19Patient.ResultDisplay = SetString(oRdr!ResultDisplay)
					oCOVID19Patient.ResultText = SetString(oRdr!ResultText)
					oCOVID19Patient.FinalResultID = 0
					oCOVID19Patient.FinalResultDateTime = String.Empty
					oCOVID19Patient.OrderStatusCode = SetString(oRdr!OrderStatusCode)
					oCOVID19Patient.OrderStatus = SetString(oRdr!OrderStatus)
					oCOVID19Patient.DischargeDisposition = SetString(oRdr!DischargeDisposition)
					oCOVID19Patient.VisitType = SetString(oRdr!VisitType)
					oCOVID19Patient.HospitalService = String.Empty
					oCOVID19Patient.PatientType = String.Empty
					oCOVID19Patient.OrderProcessDateTime = Nothing
					oCOVID19Patient.OrderNumber = 0
					oCOVID19Patient.GroupKey = sGroupKey
					If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				End While
				oCOVID19Patient.Close()
				If Not oCOVID19Patient.FinalResultByPatientSource(sGroupKey, 3) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				COVID19CharterCareProcessing = True
			End If
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19CharterCareProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "COVID19WaterburyUpdate"
	Public Sub COVID19WaterburyUpdate()
		Dim sRoutine As String = "COVID19WaterburyUpdate"
		Dim oProcessed As New Processed
		Dim dtCurrentDateTime As Date = Now
		Dim oDirectoryInfo As DirectoryInfo
		Dim oFileInfo As FileInfo()
		Dim iProcessTypeID As Int32 = 7
		Dim sEntityName As String = "Waterbury"
		Dim sSourcePath As String = gsCOVID19WaterburyInputfolder
		Dim sFilePattern As String = "wtby_covid19_orders_*.txt"
		Dim sBackupFolder As String = "\\Homefile02\DataExpress\COVID19\Backup\"

		Try
			TraceWrite("Start COVID-19 " & sEntityName & " update", sRoutine)
			oDirectoryInfo = New DirectoryInfo(sSourcePath)
			oFileInfo = oDirectoryInfo.GetFiles(sFilePattern)
			If oFileInfo.Count > 0 Then
				For Each oFile In oFileInfo
					If Not COVID19WaterburyProcessing(sSourcePath & oFile.Name) Then Throw New Exception(ErrorMessage)
					If Not MoveFile(oFile.Name, sSourcePath, sBackupFolder) Then Throw New Exception(ErrorMessage)
					If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, Format$(dtCurrentDateTime, "MM/dd/yyyy"), "00:00")
				Next
			Else
				TraceWrite("No files found for processing, using file pattern " & sFilePattern, sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19WaterburyUpdate Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19WaterburyProcessing(PathFileName As String) As Boolean
		Dim oFileStream As New FileStream(PathFileName, FileMode.Open, FileAccess.Read)
		Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord As String
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 4
		Dim sGroupKey As String = Guid.NewGuid.ToString()
		Dim iRecordCount As Int32 = 0
		Dim asInRecord() As String

		Try
			If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			While oStreamReader.Peek() > -1
				iRecordCount += 1
				sInRecord = Trim$(oStreamReader.ReadLine())
				If Len(sInRecord) > 0 Then
					asInRecord = sInRecord.Split("|"c)
					If asInRecord.Count <> 27 Then Throw New Exception("Invalid input file " & PathFileName)
					If asInRecord(0).ToLower <> "lastname" AndAlso asInRecord(1).ToLower <> "firstName" Then
						oCOVID19Patient.NewPatient()
						oCOVID19Patient.PatientSourceID = iPatientSourceID
						oCOVID19Patient.LastName = SetString(asInRecord(0))
						oCOVID19Patient.FirstName = SetString(asInRecord(1))
						oCOVID19Patient.MiddleName = SetString(asInRecord(2))
						oCOVID19Patient.Gender = SetString(asInRecord(3))
						oCOVID19Patient.DateOfBirth = SetDate(SetString(asInRecord(4)))
						oCOVID19Patient.ZipCode = SetString(asInRecord(5))
						oCOVID19Patient.DistrictCode = SetString(asInRecord(6))
						oCOVID19Patient.PatientID = SetString(asInRecord(7))
						oCOVID19Patient.PatientIDStartDateTime = Nothing
						oCOVID19Patient.MedicalRecordNumber = SetString(asInRecord(8))
						oCOVID19Patient.NHSIDNumber = SetString(asInRecord(9))
						oCOVID19Patient.AdmitDateTime = SetDate(asInRecord(10))
						oCOVID19Patient.DischargeDateTime = SetDate(asInRecord(11))
						oCOVID19Patient.OrderCode = SetString(asInRecord(12))
						oCOVID19Patient.OrderDescription = SetString(asInRecord(13))
						oCOVID19Patient.OrderLocation = SetString(asInRecord(14))
						oCOVID19Patient.OrderingPhysicanNumber = SetString(asInRecord(15))
						oCOVID19Patient.OrderingPhysicianName = SetString(asInRecord(16))
						oCOVID19Patient.ResultCode = SetString(asInRecord(17))
						oCOVID19Patient.ResultCodeName = SetString(asInRecord(18))
						oCOVID19Patient.ResultCreateDateTime = SetDate(asInRecord(19))
						oCOVID19Patient.ResultDisplay = SetString(asInRecord(20))
						oCOVID19Patient.ResultText = SetString(asInRecord(21))
						oCOVID19Patient.FinalResultID = 0
						oCOVID19Patient.FinalResultDateTime = String.Empty
						oCOVID19Patient.OrderStatusCode = SetString(asInRecord(23))
						oCOVID19Patient.OrderStatus = SetString(asInRecord(22))
						oCOVID19Patient.DischargeDisposition = SetString(asInRecord(24))
						oCOVID19Patient.VisitType = SetString(asInRecord(25))
						oCOVID19Patient.HospitalService = String.Empty
						oCOVID19Patient.PatientType = String.Empty
						oCOVID19Patient.OrderProcessDateTime = Nothing
						oCOVID19Patient.OrderNumber = SetInt32(asInRecord(26))
						oCOVID19Patient.GroupKey = sGroupKey
						If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
					End If
				End If
			End While
			oCOVID19Patient.Close()
			If Not oCOVID19Patient.FinalResultByPatientSource(sGroupKey, 4) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			COVID19WaterburyProcessing = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19WaterburyProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
			COVID19WaterburyProcessing = False
		Finally
			CloseStreamReader(oStreamReader)
			CloseFileStream(oFileStream)
		End Try
	End Function
#End Region

#Region "COVID19ECHNUpdate"
	Public Sub COVID19ECHNUpdate()
		Dim sRoutine As String = "COVID19ECHNUpdate"
		Dim oProcessed As New Processed
		Dim dtCurrentDateTime As Date = Now
		Dim oDirectoryInfo As DirectoryInfo
		Dim oFileInfo As FileInfo()
		Dim iProcessTypeID As Int32 = 8
		Dim sEntityName As String = "ECHN"
		Dim sSourcePath As String = gsCOVID19ECHNInputFolder
		Dim sFilePattern As String = "ECHN_COVID19_Lab_Test_Results*.txt"
		Dim sBackupFolder As String = "\\Homefile02\DataExpress\COVID19\Backup\"

		Try
			TraceWrite("Start COVID-19 " & sEntityName & " update", sRoutine)
			oDirectoryInfo = New DirectoryInfo(sSourcePath)
			oFileInfo = oDirectoryInfo.GetFiles(sFilePattern)
			If oFileInfo.Count > 0 Then
				For Each oFile In oFileInfo
					If Not COVID19ECHNProcessing(sSourcePath & oFile.Name) Then Throw New Exception(ErrorMessage)
					If Not MoveFile(oFile.Name, sSourcePath, sBackupFolder) Then Throw New Exception(ErrorMessage)
					If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, Format$(dtCurrentDateTime, "MM/dd/yyyy"), "00:00")
				Next
			Else
				TraceWrite("No files found for processing, using file pattern " & sFilePattern, sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19ECHNUpdate Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19ECHNProcessing(PathFileName As String) As Boolean
		Dim oFileStream As New FileStream(PathFileName, FileMode.Open, FileAccess.Read)
		Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord As String
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 5
		Dim sGroupKey As String = Guid.NewGuid.ToString()
		Dim iRecordCount As Int32 = 0
		Dim asInRecord() As String

		Try
			If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			While oStreamReader.Peek() > -1
				iRecordCount += 1
				sInRecord = Trim$(oStreamReader.ReadLine())
				If Len(sInRecord) > 0 Then
					If InStr(1, sInRecord.ToLower, "affected)") <= 0 Then
						asInRecord = sInRecord.Split("|"c)
						If asInRecord.Count <> 27 Then Throw New Exception("Invalid input file " & PathFileName)
						If asInRecord(0).ToLower <> "lastname" AndAlso asInRecord(1).ToLower <> "firstName" Then
							oCOVID19Patient.NewPatient()
							oCOVID19Patient.PatientSourceID = iPatientSourceID
							oCOVID19Patient.LastName = SetString(asInRecord(0))
							oCOVID19Patient.FirstName = SetString(asInRecord(1))
							oCOVID19Patient.MiddleName = SetString(asInRecord(2))
							oCOVID19Patient.Gender = SetString(asInRecord(3))
							oCOVID19Patient.DateOfBirth = SetDate(SetString(asInRecord(4)))
							oCOVID19Patient.ZipCode = SetString(asInRecord(5))
							oCOVID19Patient.DistrictCode = SetString(asInRecord(6))
							oCOVID19Patient.PatientID = SetString(asInRecord(7))
							oCOVID19Patient.PatientIDStartDateTime = SetDate(asInRecord(8))
							oCOVID19Patient.MedicalRecordNumber = SetString(asInRecord(9))
							oCOVID19Patient.NHSIDNumber = SetString(asInRecord(10))
							oCOVID19Patient.AdmitDateTime = SetDate(asInRecord(11))
							oCOVID19Patient.DischargeDateTime = SetDate(asInRecord(12))
							oCOVID19Patient.OrderCode = SetString(asInRecord(13))
							oCOVID19Patient.OrderDescription = SetString(asInRecord(14))
							oCOVID19Patient.OrderLocation = SetString(asInRecord(15))
							oCOVID19Patient.OrderingPhysicanNumber = SetString(asInRecord(16))
							oCOVID19Patient.OrderingPhysicianName = SetString(asInRecord(17))
							oCOVID19Patient.ResultCode = SetString(asInRecord(18))
							oCOVID19Patient.ResultCodeName = SetString(asInRecord(19))
							oCOVID19Patient.ResultCreateDateTime = SetDate(asInRecord(20))
							oCOVID19Patient.ResultDisplay = SetString(asInRecord(21))
							oCOVID19Patient.ResultText = SetString(asInRecord(22))

							If oCOVID19Patient.ResultDisplay.Length <= 0 AndAlso oCOVID19Patient.ResultText.Length > 0 Then
								If oCOVID19Patient.ResultText.ToLower.Contains("positive") Then
									oCOVID19Patient.ResultDisplay = "Positive"
								ElseIf oCOVID19Patient.ResultText.ToLower.Contains("not detected") Then
									oCOVID19Patient.ResultDisplay = "Not Detected"
								ElseIf oCOVID19Patient.ResultText.ToLower.Contains("detected") Then
									oCOVID19Patient.ResultDisplay = "Detected"
								ElseIf oCOVID19Patient.ResultText.ToLower.Contains("negative") Then
									oCOVID19Patient.ResultDisplay = "Negative"
								End If
							End If

							oCOVID19Patient.FinalResultID = 0
							oCOVID19Patient.FinalResultDateTime = String.Empty
							oCOVID19Patient.OrderStatusCode = SetString(asInRecord(23))
							oCOVID19Patient.OrderStatus = SetString(asInRecord(24))
							oCOVID19Patient.DischargeDisposition = SetString(asInRecord(25))
							oCOVID19Patient.VisitType = SetString(asInRecord(26))
							oCOVID19Patient.HospitalService = String.Empty
							oCOVID19Patient.PatientType = String.Empty
							oCOVID19Patient.OrderProcessDateTime = Nothing
							oCOVID19Patient.OrderNumber = 0
							oCOVID19Patient.GroupKey = sGroupKey
							If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
						End If
					End If
				End If
			End While
			oCOVID19Patient.Close()
			If Not oCOVID19Patient.FinalResultByPatientSource(sGroupKey, 5) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			COVID19ECHNProcessing = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19ECHNProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
			COVID19ECHNProcessing = False
		Finally
			CloseStreamReader(oStreamReader)
			CloseFileStream(oFileStream)
		End Try
	End Function
#End Region

#Region "COVID19EOGHUpdate"
	Public Sub COVID19EOGHUpdate()
		Dim sRoutine As String = "COVID19EOGHUpdate"
		Dim oProcessed As New Processed
		Dim dtCurrentDateTime As Date = Now
		Dim oDirectoryInfo As DirectoryInfo
		Dim oFileInfo As FileInfo()
		Dim iProcessTypeID As Int32 = 9
		Dim sEntityName As String = "EOGH"
		Dim sSourcePath As String = gsCOVID19EOGHInputFolder
		Dim sFilePattern As String = "eogh_covid19_orders_*.txt"
		Dim sBackupFolder As String = "\\Homefile02\DataExpress\COVID19\Backup\"

		Try
			TraceWrite("Start COVID-19 " & sEntityName & " update", sRoutine)
			oDirectoryInfo = New DirectoryInfo(sSourcePath)
			oFileInfo = oDirectoryInfo.GetFiles(sFilePattern)
			If oFileInfo.Count > 0 Then
				For Each oFile In oFileInfo
					If Not COVID19EOGHProcessing(sSourcePath & oFile.Name) Then Throw New Exception(ErrorMessage)
					If Not MoveFile(oFile.Name, sSourcePath, sBackupFolder) Then Throw New Exception(ErrorMessage)
					If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, Format$(dtCurrentDateTime, "MM/dd/yyyy"), "00:00")
				Next
			Else
				TraceWrite("No files found for processing, using file pattern " & sFilePattern, sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19EOGHUpdate Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19EOGHProcessing(PathFileName As String) As Boolean
		Dim oFileStream As New FileStream(PathFileName, FileMode.Open, FileAccess.Read)
		Dim oStreamReader As New StreamReader(oFileStream)
		Dim sInRecord As String
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 6
		Dim sGroupKey As String = Guid.NewGuid.ToString()
		Dim iRecordCount As Int32 = 0
		Dim asInRecord() As String

		Try
			If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			While oStreamReader.Peek() > -1
				iRecordCount += 1
				sInRecord = Trim$(oStreamReader.ReadLine())
				If Len(sInRecord) > 0 Then
					asInRecord = sInRecord.Split("|"c)
					If asInRecord.Count <> 27 Then Throw New Exception("Invalid input file " & PathFileName)
					If asInRecord(0).ToLower <> "lastname" AndAlso asInRecord(1).ToLower <> "firstName" Then
						oCOVID19Patient.NewPatient()
						oCOVID19Patient.PatientSourceID = iPatientSourceID
						oCOVID19Patient.LastName = SetString(asInRecord(0))
						oCOVID19Patient.FirstName = SetString(asInRecord(1))
						oCOVID19Patient.MiddleName = SetString(asInRecord(2))
						oCOVID19Patient.Gender = SetString(asInRecord(3))
						oCOVID19Patient.DateOfBirth = SetDate(SetString(asInRecord(4)))
						oCOVID19Patient.ZipCode = SetString(asInRecord(5))
						oCOVID19Patient.DistrictCode = SetString(asInRecord(6))
						oCOVID19Patient.PatientID = SetString(asInRecord(7))
						oCOVID19Patient.PatientIDStartDateTime = Nothing
						oCOVID19Patient.MedicalRecordNumber = SetString(asInRecord(8))
						oCOVID19Patient.NHSIDNumber = SetString(asInRecord(9))
						oCOVID19Patient.AdmitDateTime = SetDate(asInRecord(10))
						oCOVID19Patient.DischargeDateTime = SetDate(asInRecord(11))
						oCOVID19Patient.OrderCode = SetString(asInRecord(12))
						oCOVID19Patient.OrderDescription = SetString(asInRecord(13))
						oCOVID19Patient.OrderLocation = SetString(asInRecord(14))
						oCOVID19Patient.OrderingPhysicanNumber = SetString(asInRecord(15))
						oCOVID19Patient.OrderingPhysicianName = SetString(asInRecord(16))
						oCOVID19Patient.ResultCode = SetString(asInRecord(17))
						oCOVID19Patient.ResultCodeName = SetString(asInRecord(18))
						oCOVID19Patient.ResultCreateDateTime = SetDate(asInRecord(19))
						oCOVID19Patient.ResultDisplay = SetString(asInRecord(20))
						oCOVID19Patient.ResultText = SetString(asInRecord(21))
						oCOVID19Patient.FinalResultID = 0
						oCOVID19Patient.FinalResultDateTime = String.Empty
						oCOVID19Patient.OrderStatusCode = SetString(asInRecord(23))
						oCOVID19Patient.OrderStatus = SetString(asInRecord(22))
						oCOVID19Patient.DischargeDisposition = SetString(asInRecord(24))
						oCOVID19Patient.VisitType = SetString(asInRecord(25))
						oCOVID19Patient.HospitalService = String.Empty
						oCOVID19Patient.PatientType = String.Empty
						oCOVID19Patient.OrderProcessDateTime = Nothing
						oCOVID19Patient.OrderNumber = SetInt32(asInRecord(26))
						oCOVID19Patient.GroupKey = sGroupKey
						If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
					End If
				End If
			End While
			oCOVID19Patient.Close()
			If Not oCOVID19Patient.FinalResultByPatientSource(sGroupKey, 6) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			COVID19EOGHProcessing = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19EOGHProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
			COVID19EOGHProcessing = False
		Finally
			CloseStreamReader(oStreamReader)
			CloseFileStream(oFileStream)
		End Try
	End Function
#End Region

#Region "COVID19AltaUpdate"
	Public Sub COVID19AltaUpdate()
		Dim sRoutine As String = "COVID19AltaUpdate"
		Dim oProcessed As New Processed
		Dim dtCurrentDateTime As Date = Now
		Dim oDirectoryInfo As DirectoryInfo
		Dim oFileInfo As FileInfo()
		Dim iProcessTypeID As Int32 = 11
		Dim sEntityName As String = "Alta"
		Dim sSourcePath As String = gsCOVID19AltaInputFolder
		Dim sFilePattern As String = "AH_COVID19_*.csv"
		Dim sBackupFolder As String = "\\Homefile02\DataExpress\COVID19\Backup\"

		Try
			TraceWrite("Start COVID-19 " & sEntityName & " update", sRoutine)
			oDirectoryInfo = New DirectoryInfo(sSourcePath)
			oFileInfo = oDirectoryInfo.GetFiles(sFilePattern)
			If oFileInfo.Count > 0 Then
				For Each oFile In oFileInfo
					If Not COVID19AltaProcessing(sSourcePath & oFile.Name) Then Throw New Exception(ErrorMessage)
					If Not MoveFile(oFile.Name, sSourcePath, sBackupFolder) Then Throw New Exception(ErrorMessage)
					If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, Format$(dtCurrentDateTime, "MM/dd/yyyy"), "00:00")
				Next
			Else
				TraceWrite("No files found for processing, using file pattern " & sFilePattern, sRoutine)
			End If
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19AltaUpdate Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19AltaProcessing(PathFileName As String) As Boolean
		Dim oTextFieldParser As TextFieldParser = New FileIO.TextFieldParser(PathFileName)
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 7
		Dim sGroupKey As String = Guid.NewGuid.ToString()
		Dim iRecordCount As Int32 = 0
		Dim asInRecord() As String

		Try
			If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			oTextFieldParser.TextFieldType = FileIO.FieldType.Delimited
			oTextFieldParser.Delimiters = New String() {","}
			oTextFieldParser.HasFieldsEnclosedInQuotes = True
			Do While Not oTextFieldParser.EndOfData
				asInRecord = oTextFieldParser.ReadFields
				iRecordCount += 1
				If asInRecord.Count <> 26 Then Throw New Exception("Invalid input file " & PathFileName)
				If asInRecord(0).ToLower <> "lastname" AndAlso asInRecord(1).ToLower <> "firstName" Then
					oCOVID19Patient.NewPatient()
					oCOVID19Patient.PatientSourceID = iPatientSourceID
					oCOVID19Patient.LastName = SetString(asInRecord(0))
					oCOVID19Patient.FirstName = SetString(asInRecord(1))
					oCOVID19Patient.MiddleName = SetString(asInRecord(2))
					oCOVID19Patient.Gender = SetString(asInRecord(3))
					oCOVID19Patient.DateOfBirth = SetDate(SetString(asInRecord(4)))
					oCOVID19Patient.ZipCode = SetString(asInRecord(5))
					oCOVID19Patient.DistrictCode = SetString(asInRecord(6))
					oCOVID19Patient.PatientID = SetString(asInRecord(7))
					oCOVID19Patient.PatientIDStartDateTime = Nothing
					oCOVID19Patient.MedicalRecordNumber = SetString(asInRecord(9))
					oCOVID19Patient.NHSIDNumber = SetString(asInRecord(10))
					oCOVID19Patient.AdmitDateTime = SetDate(asInRecord(11))
					oCOVID19Patient.DischargeDateTime = SetDate(asInRecord(12))
					oCOVID19Patient.OrderCode = SetString(asInRecord(13))
					oCOVID19Patient.OrderDescription = SetString(asInRecord(14))
					oCOVID19Patient.OrderLocation = String.Empty
					oCOVID19Patient.OrderingPhysicanNumber = SetString(asInRecord(15))
					oCOVID19Patient.OrderingPhysicianName = SetString(asInRecord(16))
					oCOVID19Patient.ResultCode = SetString(asInRecord(17))
					oCOVID19Patient.ResultCodeName = SetString(asInRecord(18))
					oCOVID19Patient.ResultCreateDateTime = SetDate(asInRecord(19))
					oCOVID19Patient.ResultDisplay = SetString(asInRecord(20))
					oCOVID19Patient.ResultText = SetString(asInRecord(21))
					oCOVID19Patient.FinalResultID = 0
					oCOVID19Patient.FinalResultDateTime = String.Empty
					oCOVID19Patient.OrderStatusCode = SetString(asInRecord(22))
					oCOVID19Patient.OrderStatus = SetString(asInRecord(23))
					oCOVID19Patient.DischargeDisposition = SetString(asInRecord(24))
					oCOVID19Patient.VisitType = SetString(asInRecord(25))
					oCOVID19Patient.HospitalService = String.Empty
					oCOVID19Patient.PatientType = String.Empty
					oCOVID19Patient.OrderProcessDateTime = Nothing
					oCOVID19Patient.OrderNumber = 0
					oCOVID19Patient.GroupKey = sGroupKey
					If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				End If
			Loop
			oCOVID19Patient.Close()
			If Not oCOVID19Patient.FinalResultByPatientSource(sGroupKey, 7) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			COVID19AltaProcessing = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutine.COVID19AltaProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
			COVID19AltaProcessing = False
		Finally
			CloseTextFieldParser(oTextFieldParser)
		End Try
	End Function
#End Region

#Region "COVID19HANUpdate"
	Public Sub COVID19HANUpdate()
		Dim sRoutine As String = "COVID19HANUpdate"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 5
		Dim iWork As Int32

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19HANUpdateTime.Count <= 0 Then Exit Sub
			For iWork = 0 To galCOVID19HANUpdateTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19HANUpdateTime.Item(iWork).ToString) <= dtCurrentDateTime Then
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19HANUpdateTime.Item(iWork).ToString) Or gbByPassDateCheck Then

						TraceWrite("Start COVID-19 HAN update", sRoutine)
						If Not COVID19EMRProcessing(0) Then Throw New Exception(ErrorMessage)
						TraceWrite("End COVID-19 HAN update", sRoutine)

						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19HANUpdateTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: " & sCurrentDate & " " & galCOVID19HANUpdateTime.Item(iWork).ToString, sRoutine)
					End If
				Else
					TraceWrite("Processing is not required for " & galCOVID19HANUpdateTime.Item(iWork).ToString & ".", sRoutine)
				End If
			Next iWork
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19HANUpdate Error:   " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19EMRProcessing(SiteCode As Int32) As Boolean
		Dim oDBC As OracleConnection = New OracleConnection
		Dim oCmd As OracleCommand = New OracleCommand
		Dim oRdr As OracleDataReader = Nothing
		Dim oCOVID19Patient As New COVID19Patient
		Dim iPatientSourceID As Int32 = 2
		Dim sGroupKey As String = Guid.NewGuid.ToString()

		COVID19EMRProcessing = False
		Try
			If Not OpenDBOracle(oDBC, SiteCode) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "SELECT PERSON.LASTNAME, PERSON.FIRSTNAME, PERSON.MIDDLENAME, PERSON.SEX, PERSON.DATEOFBIRTH, PERSON.ZIP, " &
				"PERSON.PID, PERSON.MEDRECNO, PERSON.SOCSECNO, OBS.HDID, OBSHEAD.NAME, USR.PVID, USR.LASTNAME AS PhysicianLastName, " &
				"USR.FIRSTNAME AS PhysicianFirstName, OBS.OBSDATE, OBS.OBSVALUE, OBS.RANGE, OBS.STATE " &
				"FROM OBS " &
				"INNER JOIN PERSON ON OBS.PID = PERSON.PID " &
				"LEFT OUTER JOIN USR ON OBS.PUBUSER = USR.PVID " &
				"LEFT OUTER JOIN OBSHEAD ON OBS.HDID = OBSHEAD.HDID " &
				"WHERE OBS.HDID IN (665482, 665481, 665482, 666735, 665480, 664316, 665997) AND OBS.CHANGE = 2"
			oCmd.CommandType = CommandType.Text
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				While oRdr.Read()
					oCOVID19Patient.NewPatient()
					oCOVID19Patient.PatientSourceID = iPatientSourceID
					oCOVID19Patient.LastName = SetString(oRdr!LASTNAME)
					oCOVID19Patient.FirstName = SetString(oRdr!FIRSTNAME)
					oCOVID19Patient.MiddleName = SetString(oRdr!MIDDLENAME)
					oCOVID19Patient.Gender = SetString(oRdr!SEX)
					oCOVID19Patient.DateOfBirth = SetDate(oRdr!DATEOFBIRTH)
					oCOVID19Patient.ZipCode = SetString(oRdr!ZIP)
					oCOVID19Patient.DistrictCode = "HANEMR"
					oCOVID19Patient.PatientID = SetString(oRdr!PID)
					oCOVID19Patient.PatientIDStartDateTime = Nothing
					oCOVID19Patient.MedicalRecordNumber = SetString(oRdr!MEDRECNO)
					oCOVID19Patient.NHSIDNumber = SetString(oRdr!SOCSECNO)
					oCOVID19Patient.AdmitDateTime = Nothing
					oCOVID19Patient.DischargeDateTime = Nothing
					oCOVID19Patient.OrderCode = String.Empty
					oCOVID19Patient.OrderDescription = String.Empty
					oCOVID19Patient.OrderLocation = String.Empty
					oCOVID19Patient.OrderingPhysicanNumber = SetString(oRdr!PVID)
					oCOVID19Patient.OrderingPhysicianName = SetString(oRdr!PhysicianFirstName) & Space(1) & SetString(oRdr!PhysicianLastName)
					oCOVID19Patient.ResultCode = SetString(oRdr!HDID)
					oCOVID19Patient.ResultCodeName = SetString(oRdr!NAME)
					oCOVID19Patient.ResultCreateDateTime = SetDate(oRdr!OBSDATE)
					oCOVID19Patient.ResultDisplay = SetString(oRdr!OBSVALUE)
					oCOVID19Patient.ResultText = SetString(oRdr!RANGE)
					oCOVID19Patient.FinalResultID = 0
					oCOVID19Patient.FinalResultDateTime = String.Empty
					oCOVID19Patient.OrderStatusCode = String.Empty
					oCOVID19Patient.OrderStatus = SetString(oRdr!STATE)
					oCOVID19Patient.DischargeDisposition = String.Empty
					oCOVID19Patient.VisitType = String.Empty
					oCOVID19Patient.HospitalService = String.Empty
					oCOVID19Patient.PatientType = String.Empty
					oCOVID19Patient.OrderProcessDateTime = Nothing
					oCOVID19Patient.OrderNumber = 0
					oCOVID19Patient.GroupKey = sGroupKey
					If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				End While
				oCOVID19Patient.Close()
				If Not oCOVID19Patient.FinalResultHAN(sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
				COVID19EMRProcessing = True
			End If
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19EMRProcessing: " & oException.Message
			oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
		Finally
			CloseRDROracle(oRdr)
			CloseCMDOracle(oCmd)
			CloseDBOracle(oDBC)
		End Try
	End Function
#End Region

#Region "COVID19WaterburyData"
	Public Sub COVID19WaterburyData()
		Dim sRoutine As String = "COVID19WaterburyData"
		Dim oProcessed As New Processed
		Dim oCOVID19Patient As New COVID19Patient
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim iProcessTypeID As Int32 = 12
		Dim iWork As Int32

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19WaterburyOutputTime.Count <= 0 Then Exit Sub

			If Not oCOVID19Patient.UpdateCompleteCheck(2) Then
				TraceWrite(oCOVID19Patient.UpdateStatusMessage, sRoutine)
				Exit Sub
			End If

			For iWork = 0 To galCOVID19WaterburyOutputTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19WaterburyOutputTime.Item(iWork).ToString) <= dtCurrentDateTime Then
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19WaterburyOutputTime.Item(iWork).ToString) Or gbByPassDateCheck Then
						TraceWrite("Start COVID-19 Waterbury Data", sRoutine)
						If Not COVID19WaterburyDataProcessing() Then Throw New Exception("Error COVID-19 Waterbury Data")
						TraceWrite("End COVID-19 Waterbury Data", sRoutine)
						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19WaterburyOutputTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: " & sCurrentDate & " " & galCOVID19WaterburyOutputTime.Item(iWork).ToString, sRoutine)
					End If
				Else
					TraceWrite("Processing is not required for " & galCOVID19WaterburyOutputTime.Item(iWork).ToString & ".", sRoutine)
				End If
			Next iWork
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19WaterburyData Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19WaterburyDataProcessing() As Boolean
		Dim sPathFileName As String = AddBackslash(gsCOVID19WaterburyOutputFolder) & "COVID 19 Patient Data Waterbury " & Format(Now, "yyyy-MM-dd") & ".csv"
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim sOutRecord As String
		Dim bDeleteFile As Boolean = False

		Try
			If File.Exists(sPathFileName) Then
				File.Delete(sPathFileName)
				If File.Exists(sPathFileName) Then Throw New Exception("File " & sPathFileName & "already exists and can't be deleted.")
			End If

			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sCOVID19PatientWaterbury"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oFileStream = New FileStream(sPathFileName, FileMode.CreateNew, FileAccess.Write)
				oStreamWriter = New StreamWriter(oFileStream)
				sOutRecord = "PatientSourceID, LastName, FirstName, MiddleName, Gender, DateOfBirth, ZipCode, DistrictCode, PatientID, PatientIDStartDateTime, MedicalRecordNumber, NHSIDNumber, " &
					"AdmitDateTime, DischargeDateTime, OrderCode,OrderDescription, OrderLocation, OrderingPhysicanNumber, OrderingPhysicianName, ResultCode, ResultCodeName, ResultCreateDateTime, ResultDisplay, " &
					"ResultText, FinalResultID, FinalResultDateTime, OrderStatusCode, OrderStatus, DischargeDisposition, VisitType, OrderProcessDateTime, OrderNumber"
				oStreamWriter.WriteLine(sOutRecord)
				While oRdr.Read()

					sOutRecord = String.Empty
					'COVID19PatientID
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!PatientSourceID).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!LastName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!FirstName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!MiddleName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!Gender), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DateOfBirth), 4) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ZipCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DistrictCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!PatientID).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!PatientIDStartDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!MedicalRecordNumber), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!NHSIDNumber), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!AdmitDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DischargeDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderDescription), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderLocation), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderingPhysicanNumber), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderingPhysicianName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultCodeName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultCreateDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultDisplay), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultText), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!FinalResultID).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!FinalResultDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderStatusCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderStatus), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DischargeDisposition), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!VisitType), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderProcessDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!OrderNumber).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					'GroupKey
					'DeleteFlag
					'RecordCreate
					'RecordUpdate
					oStreamWriter.WriteLine(sOutRecord)
				End While
			Else
				Throw New Exception("No data found for Waterbury. All processing is canceled.")
			End If


			COVID19WaterburyDataProcessing = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19WaterburyProcessing: " & oException.Message
			bDeleteFile = True
			COVID19WaterburyDataProcessing = False
		Finally
			CloseStreamWriter(oStreamWriter)
			CloseFileStream(oFileStream)
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
			If bDeleteFile AndAlso File.Exists(sPathFileName) Then File.Delete(sPathFileName)
		End Try
	End Function
#End Region

#Region "COVID19PMHData"
	Public Sub COVID19PMHData()
		Dim sRoutine As String = "COVID19PMHData"
		Dim oProcessed As New Processed
		Dim oCOVID19Patient As New COVID19Patient
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim iProcessTypeID As Int32 = 14
		Dim iWork As Int32

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19VisiquateUpdateTime.Count <= 0 Then Exit Sub

			If Not oCOVID19Patient.UpdateCompleteCheck(3) Then
				TraceWrite(oCOVID19Patient.UpdateStatusMessage, sRoutine)
				Exit Sub
			End If

			For iWork = 0 To galCOVID19VisiquateUpdateTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19VisiquateUpdateTime.Item(iWork).ToString) <= dtCurrentDateTime Then
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19VisiquateUpdateTime.Item(iWork).ToString) Or gbByPassDateCheck Then
						TraceWrite("Start COVID-19 PMH Data", sRoutine)
						If Not COVID19PMHDataProcessing() Then Throw New Exception("Error COVID-19 PMH Data")
						TraceWrite("End COVID-19 PMH Data", sRoutine)
						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19VisiquateUpdateTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: " & sCurrentDate & " " & galCOVID19VisiquateUpdateTime.Item(iWork).ToString, sRoutine)
					End If
				Else
					TraceWrite("Processing is not required for " & galCOVID19VisiquateUpdateTime.Item(iWork).ToString & ".", sRoutine)
				End If
			Next iWork
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19PMHData Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19PMHDataProcessing() As Boolean
		Dim sPathFileName As String = AddBackslash(gsCOVID19PMHOutputFolder) & "COVID 19 Patient Data PMH " & Format(Now, "yyyy-MM-dd") & ".csv"
		Dim oFileStream As FileStream = Nothing
		Dim oStreamWriter As StreamWriter = Nothing
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim sOutRecord As String
		Dim bDeleteFile As Boolean = False

		Try
			If File.Exists(sPathFileName) Then
				File.Delete(sPathFileName)
				If File.Exists(sPathFileName) Then Throw New Exception("File " & sPathFileName & "already exists and can't be deleted.")
			End If

			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sCOVID19PatientPMH"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oFileStream = New FileStream(sPathFileName, FileMode.CreateNew, FileAccess.Write)
				oStreamWriter = New StreamWriter(oFileStream)
				sOutRecord = "COVID19PatientID, PatientSourceID, PatientSource, LastName, FirstName, MiddleName, Gender, DateOfBirth, ZipCode, DistrictCode, PatientID, PatientIDStartDateTime, " &
					"MedicalRecordNumber, NHSIDNumber, AdmitDateTime, DischargeDateTime, OrderCode, OrderDescription, OrderLocation, OrderingPhysicanNumber, OrderingPhysicianName, ResultCode, " &
					"ResultCodeName, ResultCreateDateTime, ResultDisplay, ResultText, FinalResultID, FinalResultDateTime, OrderStatusCode, OrderStatus, DischargeDisposition, VisitType, HospitalService, " &
					"PatientType, OrderProcessDateTime, OrderNumber, GroupKey"
				oStreamWriter.WriteLine(sOutRecord)
				While oRdr.Read()
					sOutRecord = String.Empty
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!COVID19PatientID).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!PatientSourceID).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!PatientSource), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!LastName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!FirstName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!MiddleName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!Gender), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DateOfBirth), 4) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ZipCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DistrictCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!PatientID), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!PatientIDStartDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!MedicalRecordNumber), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!NHSIDNumber), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!AdmitDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DischargeDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderDescription), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderLocation), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderingPhysicanNumber), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderingPhysicianName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultCodeName), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultCreateDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultDisplay), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!ResultText), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!FinalResultID).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!FinalResultDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderStatusCode), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderStatus), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!DischargeDisposition), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!VisitType), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!HospitalService), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!PatientType), 1) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!OrderProcessDateTime), 3) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetInt32(oRdr!OrderNumber).ToString, 2) Then Throw New Exception("Error adding CSV field.")
					If Not AddCSVField(sOutRecord, SetString(oRdr!GroupKey), 1) Then Throw New Exception("Error adding CSV field.")
					'DeleteFlag
					'RecordCreate
					'RecordUpdate
					oStreamWriter.WriteLine(sOutRecord)
				End While
			Else
				Throw New Exception("No data found for PMH. All processing is canceled.")
			End If


			COVID19PMHDataProcessing = True
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19PMHProcessing: " & oException.Message
			bDeleteFile = True
			COVID19PMHDataProcessing = False
		Finally
			CloseStreamWriter(oStreamWriter)
			CloseFileStream(oFileStream)
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
			If bDeleteFile AndAlso File.Exists(sPathFileName) Then File.Delete(sPathFileName)
		End Try
	End Function
#End Region

#Region "COVID19Report"
	Public Sub COVID19Report()
		Dim sRoutine As String = "COVID19Report"
		Dim oCOVID19Patient As New COVID19Patient
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32 = 10
		Dim iWork As Int32

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19ReportTime.Count <= 0 Then Exit Sub

			If Not oCOVID19Patient.UpdateCompleteCheck(1) Then
				TraceWrite(oCOVID19Patient.UpdateStatusMessage, sRoutine)
				Exit Sub
			End If

			For iWork = 0 To galCOVID19ReportTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19ReportTime.Item(iWork).ToString) <= dtCurrentDateTime Then
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19ReportTime.Item(iWork).ToString) Or gbByPassDateCheck Then
						TraceWrite("Start COVID-19 report", sRoutine)
						msCurrentCOVID19FileName = String.Empty
						If Not CreateCOVID19Report() Then Throw New Exception(ErrorMessage)

						'test
						'msCurrentCOVID19FileName = "COVID-19 Lab Report 2020-06-03 07-05-46.xlsx"
						'test

						If Not EMailCOVID19Report() Then Throw New Exception(ErrorMessage)
						'RemovePastCOVID19Documents()
						TraceWrite("End COVID-19 report", sRoutine)
						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19ReportTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: " & sCurrentDate & " " & galCOVID19ReportTime.Item(iWork).ToString, sRoutine)
					End If
				Else
					TraceWrite("Processing is not required for " & galCOVID19ReportTime.Item(iWork).ToString & ".", sRoutine)
				End If
			Next iWork
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Private Function CreateCOVID19Report() As Boolean
		Dim sRoutine As String = "CreateCOVID19Report"
		Dim sPathFileName As String
		Dim dtCurrentDateTime As Date = Now
		Dim sDebugStep As String = vbNullString

		TraceWrite("Processing", sRoutine)
		msCurrentCOVID19FileName = gsCOVID19ReportFileName & " " & Format(dtCurrentDateTime, "yyyy-MM-dd HH-mm-ss") & ".xlsx"
		sPathFileName = AddBackslash(gsBuildPath) & msCurrentCOVID19FileName

		Try
			KillExcel()
			sDebugStep = "moExcelApplication = New Excel.Application"
			moExcelApplication = New Excel.Application
			sDebugStep = "moExcelApplication.SheetsInNewWorkbook = 1"
			moExcelApplication.SheetsInNewWorkbook = 5
			sDebugStep = "moWorkbook = moExcelApplication.Workbooks.Add"
			moWorkbook = moExcelApplication.Workbooks.Add
			sDebugStep = vbNullString

			If Not COVID19TOC(1, "TOC - Definitions", 1) Then Throw New Exception("Error: Processing tab code 1")
			If Not COVID19Site(3, "eCare and HAN All", 2) Then Throw New Exception("Error: Processing tab code 2")
			If Not COVID19Site(6, "eCare HAN Primary Lab Status", 3) Then Throw New Exception("Error: Processing tab code 3")
			If Not COVID19CountByDistrictCode("eCare HAN COVID Test Summary", 4) Then Throw New Exception("Error: Processing tab code 4")
			If Not COVID19Site(7, "Antibody Testing", 5) Then Throw New Exception("Error: Processing tab code 5")

			TraceWrite("Start Spreadsheet Save - Path/File Name: " & sPathFileName, sRoutine)

			If Not Directory.Exists(gsBuildPath) Then
				Directory.CreateDirectory(gsBuildPath)
				If Not Directory.Exists(gsBuildPath) Then Throw New Exception("Error: Directory " & gsBuildPath & " doesn't exist")
			End If

			If File.Exists(sPathFileName) Then
				File.Delete(sPathFileName)
				If File.Exists(sPathFileName) Then Throw New Exception("Error: File " & sPathFileName & " already exists")
			End If

			TraceWrite("SaveCopyAs Statement - Path/File Name: " & sPathFileName, sRoutine)
			moWorkbook.SaveCopyAs(sPathFileName)
			TraceWrite("Complete Spreadsheet Save", sRoutine)

			CreateCOVID19Report = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			TraceWrite("Error: " & ErrorMessage, sRoutine)
			If Len(sDebugStep) > 0 Then TraceWrite("Debug Step: " & sDebugStep, sRoutine)
			If File.Exists(sPathFileName) Then File.Delete(sPathFileName)
			CreateCOVID19Report = False
		Finally
			GC.Collect()
			GC.WaitForPendingFinalizers()
			ReleaseObject(moWorkSheet)
			ReleaseObject(moWorkbook)
			CloseWorkbook()
			moExcelApplication.Quit()
			ReleaseObject(moExcelApplication)
			moExcelApplication = Nothing
		End Try
	End Function

	Private Function COVID19TOC(TabCode As Int32, TabName As String, Spreadsheet As Int32) As Boolean
		Dim sRoutine As String = "COVID19TOC"
		Dim dtCurrentDateTime As Date = Now

		TraceWrite("Start Processing TabCode: " & TabCode & ", TabName: " & TabName, sRoutine)
		COVID19TOC = False
		Try
			miColor = RGB(255, 255, 255)
			moWorkSheet = DirectCast(moWorkbook.Sheets(Spreadsheet), Excel.Worksheet)
			miFontSize = 11
			msFontName = "Calibri"
			miRow = 0

			'With moWorkSheet.PageSetup
			'	.Orientation = Excel.XlPageOrientation.xlLandscape
			'	.PrintGridlines = True
			'	.LeftMargin = 0
			'	.RightMargin = 0
			'	.CenterHorizontally = True
			'	.PrintTitleRows = "$1:$3"
			'	.PaperSize = Excel.XlPaperSize.xlPaperLegal
			'	.Zoom = False
			'	.FitToPagesTall = 1
			'	.FitToPagesWide = 1
			'End With

			moWorkSheet.Name = TabName
			SetColumn("A", 12.0)
			SetColumn("B", 30.0)
			SetColumn("C", 30.0)
			SetColumn("D", 30.0)
			mbBold = True

			'row 1
			miRow += 1
			miFontSize = 14
			mbBold = True
			SheetHeading(TabName & " COVID-19 Lab Report", "A1", "D1", "C", RGB(255, 255, 255))

			'row 2
			miRow += 1
			miFontSize = 10
			RowHeight(15.0)
			mbBold = True
			SheetHeading(MonthName(Month(dtCurrentDateTime)) & Space(1) & Format(Day(dtCurrentDateTime), "#0") & ", " & Format(dtCurrentDateTime, "yyyy"), "A2", "D2", "C", RGB(255, 255, 255))

			'row 3
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("Tab Number", "A", "C")
			ValueCell("Tab Name", "B", "L")
			ValueCell("Description", "C", "L")

			'row 4
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("1", "A", "C")
			ValueCell("TOC � Definitions", "B", "L")
			ValueCell("Table of Contents & Definitions", "C", "L")

			'row 5
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("2", "A", "C")
			ValueCell("eCare HAN ALL", "B", "L")
			ValueCell("", "C", "L")

			'row 6
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("3", "A", "C")
			ValueCell("eCare HAN Primary Lab Status", "B", "L")
			ValueCell("", "C", "L")

			'row 7
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("4", "A", "C")
			ValueCell("eCare HAN COVID Test Summary", "B", "L")
			ValueCell("", "C", "L")

			COVID19TOC = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			COVID19TOC = False
		Finally
			ReleaseObject(moWorkSheet)
			TraceWrite("Complete Processing TabCode: " & TabCode & ", SiteName: " & TabName, sRoutine)
		End Try
	End Function

	Private Function COVID19Site(TabCode As Int32, TabName As String, Spreadsheet As Int32) As Boolean
		Dim sRoutine As String = "COVID19Site"
		Dim dtCurrentDateTime As Date = Now
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim iRecordCount, iPatientSourceID, iPatientSourceIDHold, iOrderNumber, iOrderNumberHold As Int32
		Dim dtWork As Date
		Dim sLastName, sLastNameHold, sFirstName, sFirstNameHold, sDateOfBirth, sDateOfBirthHold, sPatientID, sPatientIDHold As String
		Dim bWriteRecord As Boolean = False

		TraceWrite("Start Processing TabCode: " & TabCode & ", TabName: " & TabName, sRoutine)
		COVID19Site = False
		Try
			miColor = RGB(255, 255, 255)
			moWorkSheet = DirectCast(moWorkbook.Sheets(Spreadsheet), Excel.Worksheet)
			miFontSize = 11
			msFontName = "Calibri"
			miRow = 0

			'With moWorkSheet.PageSetup
			'	.Orientation = Excel.XlPageOrientation.xlLandscape
			'	.PrintGridlines = True
			'	.LeftMargin = 0
			'	.RightMargin = 0
			'	.CenterHorizontally = True
			'	.PrintTitleRows = "$1:$3"
			'	.PaperSize = Excel.XlPaperSize.xlPaperLegal
			'	.Zoom = False
			'	.FitToPagesTall = 1
			'	.FitToPagesWide = 1
			'End With

			moWorkSheet.Name = TabName
			SetColumn("A", 9.0)
			SetColumn("B", 20.0)
			SetColumn("C", 17.0)
			SetColumn("D", 13.0)
			SetColumn("E", 7.0)
			SetColumn("F", 12.0)
			SetColumn("G", 10.0)
			SetColumn("H", 12.0)
			SetColumn("I", 17.0)
			SetColumn("J", 23.0)
			SetColumn("K", 15.0)
			SetColumn("L", 12.0)
			SetColumn("M", 15.0)
			SetColumn("N", 12.0)
			SetColumn("O", 16.0)
			SetColumn("P", 19.0)
			SetColumn("Q", 20.0)
			SetColumn("R", 17.0)
			SetColumn("S", 31.0)
			SetColumn("T", 14.0)
			SetColumn("U", 25.0)
			SetColumn("V", 23.0)
			SetColumn("W", 12.0)
			SetColumn("X", 25.0)
			SetColumn("Y", 22.0)
			SetColumn("Z", 25.0)
			SetColumn("AA", 25.0)
			SetColumn("AB", 17.0)
			SetColumn("AC", 17.0)
			SetColumn("AD", 22.0)
			SetColumn("AE", 17.0)
			mbBold = True

			'row 1
			miRow += 1
			miFontSize = 14
			mbBold = True
			SheetHeading(TabName & " COVID-19 Lab Report", "A1", "AE1", "C", RGB(255, 255, 255))

			'row 2
			miRow += 1
			miFontSize = 10
			RowHeight(15.0)
			mbBold = True
			SheetHeading(MonthName(Month(dtCurrentDateTime)) & Space(1) & Format(Day(dtCurrentDateTime), "#0") & ", " & Format(dtCurrentDateTime, "yyyy"), "A2", "AE2", "C", RGB(255, 255, 255))

			'row 3
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("Source", "A", "L")
			ValueCell("Last Name", "B", "L")
			ValueCell("First Name", "C", "L")
			ValueCell("Middle Name", "D", "L")
			ValueCell("Gender", "E", "C")
			ValueCell("Date of Birth", "F", "C")
			ValueCell("Zip Code", "G", "L")
			ValueCell("District Code", "H", "L")
			ValueCell("Patient ID", "I", "L")
			ValueCell("Medical Record Number", "J", "L")
			ValueCell("NHS ID Number", "K", "C")
			ValueCell("Visit Type", "L", "C")
			ValueCell("Hospital Service", "M", "C")
			ValueCell("Patient Type", "N", "C")
			ValueCell("Admit Date/Time", "O", "C")
			ValueCell("Discharge Date/Time", "P", "C")
			ValueCell("Discharge Disposition", "Q", "C")
			ValueCell("Order Code", "R", "L")
			ValueCell("Order Description", "S", "L")
			ValueCell("Order Location", "T", "L")
			ValueCell("Ordering Physican Number", "U", "L")
			ValueCell("Ordering Physician Name", "V", "L")
			ValueCell("Result Code", "W", "L")
			ValueCell("Result Code Name", "X", "L")
			ValueCell("Result Create Date/Time", "Y", "C")
			ValueCell("Result Display", "Z", "L")
			ValueCell("Result Text", "AA", "L")
			ValueCell("Order Status", "AB", "L")
			ValueCell("Order Number", "AC", "C")
			ValueCell("Order Status", "AD", "C")
			ValueCell("Final Result", "AE", "L")

			'detail
			miColor = RGB(255, 255, 255)
			miFontSize = 11
			RowHeight(15.0)
			mbBold = False
			If Not OpenDB(oDBC, 1) Then Throw New Exception("Error opening database.")
			Select Case TabCode
				Case 1
					oCmd.CommandText = "sCOVID19PatientListECare01"
				Case 2
					oCmd.CommandText = "sCOVID19PatientListECare02"
				Case 3
					oCmd.CommandText = "sCOVID19PatientListECareHAN01"
				Case 4
					oCmd.CommandText = "sCOVID19PatientListHAN01"
				Case 5
					oCmd.CommandText = "sCOVID19PatientListECareHAN02"
				Case 6
					oCmd.CommandText = "sCOVID19PatientListECareHAN04"
				Case 7
					oCmd.CommandText = "sCOVID19PatientListECare03"
				Case Else
					Throw New Exception("Invalid Tab Code - " & TabCode)
			End Select

			iPatientSourceIDHold = -1
			sLastNameHold = String.Empty
			sFirstNameHold = String.Empty
			sDateOfBirthHold = String.Empty
			sPatientIDHold = String.Empty
			iOrderNumberHold = -1

			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				Do While oRdr.Read
					If TabCode <> 6 Then
						bWriteRecord = True
					Else
						iPatientSourceID = SetInt32(oRdr!PatientSourceID)
						sLastName = SetString(oRdr!LastName)
						sFirstName = SetString(oRdr!FirstName)
						sDateOfBirth = SetString(oRdr!DateOfBirth)
						sPatientID = SetString(oRdr!PatientID)
						iOrderNumber = SetInt32(oRdr!OrderNumber)
						If iPatientSourceID <> iPatientSourceIDHold OrElse sLastName <> sLastNameHold OrElse sFirstName <> sFirstNameHold OrElse sDateOfBirth <> sDateOfBirthHold OrElse
								sPatientID <> sPatientIDHold OrElse iOrderNumber <> iOrderNumberHold Then
							bWriteRecord = True
							iPatientSourceIDHold = iPatientSourceID
							sLastNameHold = sLastName
							sFirstNameHold = sFirstName
							sDateOfBirthHold = sDateOfBirth
							sPatientIDHold = sPatientID
							iOrderNumberHold = iOrderNumber
						Else
							bWriteRecord = False
						End If
					End If

					If bWriteRecord Then
						iRecordCount += 1
						If (iRecordCount Mod 1000) = 0 Then TraceWrite("COVID19Site: " & "Processed = " & iRecordCount.ToString, sRoutine)
						miRow += 1
							ValueCell(SetString(oRdr!PatientSource), "A", "L")
							ValueCell(SetString(oRdr!LastName), "B", "L")
							ValueCell(SetString(oRdr!FirstName), "C", "L")
							ValueCell(SetString(oRdr!MiddleName), "D", "L")
							ValueCell(SetString(oRdr!Gender), "E", "L")
							If IsDate(SetString(oRdr!DateOfBirth)) Then
								ValueCell(Format(SetDate(oRdr!DateOfBirth), "MM/dd/yyyy"), "F", "C")
							Else
								ValueCell(String.Empty, "F", "C")
							End If
							ValueCell(SetString(oRdr!ZipCode), "G", "L")
							ValueCell(SetString(oRdr!DistrictCode), "H", "L")
							ValueCell(SetString(oRdr!PatientID), "I", "L")
							ValueCell(SetString(oRdr!MedicalRecordNumber), "J", "L")
							ValueCell(SetString(oRdr!NHSIDNumber), "K", "L")
							ValueCell(SetString(oRdr!VisitType), "L", "C")
							ValueCell(SetString(oRdr!HospitalService), "M", "C")
							ValueCell(SetString(oRdr!PatientType), "N", "C")
							If IsDate(SetString(oRdr!AdmitDateTime)) Then
								dtWork = CDate(oRdr!AdmitDateTime)
								If Year(dtWork) = 1899 AndAlso Month(dtWork) = 12 AndAlso Day(dtWork) = 30 Then
									ValueCell(String.Empty, "O", "C")
								Else
									ValueCell(Format(CDate(oRdr!AdmitDateTime), "MM/dd/yyyy hh:MM"), "O", "C")
								End If
							Else
								ValueCell(String.Empty, "O", "C")
							End If
							If IsDate(SetString(oRdr!DischargeDateTime)) Then
								dtWork = CDate(oRdr!DischargeDateTime)
								If Year(dtWork) = 1899 AndAlso Month(dtWork) = 12 AndAlso Day(dtWork) = 30 Then
									ValueCell(String.Empty, "P", "C")
								Else
									ValueCell(Format(CDate(oRdr!DischargeDateTime), "MM/dd/yyyy hh:MM"), "P", "C")
								End If
							Else
								ValueCell(String.Empty, "P", "C")
							End If
							ValueCell(SetString(oRdr!DischargeDisposition), "Q", "C")
							ValueCell(SetString(oRdr!OrderCode), "R", "L")
							ValueCell(SetString(oRdr!OrderDescription), "S", "L")
							ValueCell(SetString(oRdr!OrderLocation), "T", "L")
							ValueCell(SetString(oRdr!OrderingPhysicanNumber), "U", "L")
							ValueCell(SetString(oRdr!OrderingPhysicianName), "V", "L")
							ValueCell(SetString(oRdr!ResultCode), "W", "L")
							ValueCell(SetString(oRdr!ResultCodeName), "X", "L")
							If IsDate(SetString(oRdr!ResultCreateDateTime)) Then
								dtWork = CDate(oRdr!ResultCreateDateTime)
								If Year(dtWork) = 1899 AndAlso Month(dtWork) = 12 AndAlso Day(dtWork) = 30 Then
									ValueCell(String.Empty, "Y", "C")
								Else
									ValueCell(Format(CDate(oRdr!ResultCreateDateTime), "MM/dd/yyyy hh:MM"), "Y", "C")
								End If
							Else
								ValueCell(String.Empty, "Y", "C")
							End If
							ValueCell(SetString(oRdr!ResultDisplay), "Z", "L")
							ValueCell(SetString(oRdr!ResultText), "AA", "L")
							ValueCell(SetString(oRdr!OrderStatus), "AB", "L")
							ValueCell(SetString(oRdr!OrderNumber), "AC", "C")
							If IsDate(SetString(oRdr!OrderProcessDateTime)) Then
								dtWork = CDate(oRdr!OrderProcessDateTime)
								If Year(dtWork) = 1899 AndAlso Month(dtWork) = 12 AndAlso Day(dtWork) = 30 Then
									ValueCell(String.Empty, "AD", "C")
								Else
									ValueCell(Format(CDate(oRdr!OrderProcessDateTime), "MM/dd/yyyy hh:MM"), "AD", "C")
								End If
							Else
								ValueCell(String.Empty, "AD", "C")
							End If
							ValueCell(SetString(oRdr!FinalResult), "AE", "L")
							'test
							'If iRecordCount >= 10 Then Exit Do
							'test

						End If
        Loop
			End If
			COVID19Site = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			COVID19Site = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
			ReleaseObject(moWorkSheet)
			TraceWrite("Complete Processing TabCode: " & TabCode & ", SiteName: " & TabName, sRoutine)
		End Try
	End Function

	Private Function COVID19CountByDistrictCode(TabName As String, Spreadsheet As Int32) As Boolean
		Dim sRoutine As String = "COVID19CountByDistrictCode"
		Dim oCOVID19Count As New COVID19Count
		Dim dtCurrentDateTime As Date = Now
		Dim dtYesterdayDateTime = DateAdd(DateInterval.Day, -1, dtCurrentDateTime)
		Dim dtBeginDate As Date = CDate("03/01/2020")
		Dim dtStartDate, dtEndDate, dtStartDateTotal, dtEndDateTotal As Date
		'Dim iRecordCount As Int32
		Dim iDays As Int32 = CInt(DateDiff(DateInterval.Day, dtBeginDate, DateAdd(DateInterval.Day, -1, dtCurrentDateTime))) + 1
		Dim iWork, iCurrentYear, iCurrentMonth As Int32
		Dim bExit As Boolean
		Dim sWork As String

		TraceWrite("Start Processing TabCode: " & ", TabName: " & TabName, sRoutine)
		COVID19CountByDistrictCode = False
		Try
			miColor = RGB(255, 255, 255)
			moWorkSheet = DirectCast(moWorkbook.Sheets(Spreadsheet), Excel.Worksheet)
			miFontSize = 11
			msFontName = "Calibri"
			miRow = 0

			'With moWorkSheet.PageSetup
			'	.Orientation = Excel.XlPageOrientation.xlLandscape
			'	.PrintGridlines = True
			'	.LeftMargin = 0
			'	.RightMargin = 0
			'	.CenterHorizontally = True
			'	.PrintTitleRows = "$1:$3"
			'	.PaperSize = Excel.XlPaperSize.xlPaperLegal
			'	.Zoom = False
			'	.FitToPagesTall = 1
			'	.FitToPagesWide = 1
			'End With

			moWorkSheet.Name = TabName
			SetColumn("A", 10.0)
			SetColumn("B", 6.0)
			SetColumn("C", 9.0)
			SetColumn("D", 9.0)
			SetColumn("E", 9.0)
			SetColumn("F", 3.0)
			SetColumn("G", 9.0)
			SetColumn("H", 9.0)
			SetColumn("I", 9.0)
			SetColumn("J", 3.0)
			SetColumn("K", 9.0)
			SetColumn("L", 9.0)
			SetColumn("M", 9.0)
			SetColumn("N", 3.0)
			SetColumn("O", 9.0)
			SetColumn("P", 9.0)
			SetColumn("Q", 9.0)
			SetColumn("R", 3.0)
			SetColumn("S", 9.0)
			SetColumn("T", 9.0)
			SetColumn("U", 9.0)
			SetColumn("V", 3.0)
			SetColumn("W", 9.0)
			SetColumn("X", 9.0)
			SetColumn("Y", 9.0)
			SetColumn("Z", 3.0)
			SetColumn("AA", 9.0)
			SetColumn("AB", 9.0)
			SetColumn("AC", 9.0)
			mbBold = True

			'main header
			miRow += 1
			miFontSize = 14
			mbBold = True
			SheetHeading(TabName & " COVID-19 Lab Report", "A1", "AC1", "C", RGB(255, 255, 255))

			'date header
			miRow += 1
			miFontSize = 10
			RowHeight(15.0)
			mbBold = True
			SheetHeading(MonthName(Month(dtCurrentDateTime)) & Space(1) & Format(Day(dtCurrentDateTime), "#0") & ", " & Format(dtCurrentDateTime, "yyyy"), "A2", "AC2", "C", RGB(255, 255, 255))

			'month totals
			'blank row
			miRow += 1

			'header row 1
			miRow += 1
			miFontSize = 10
			RowHeight(15.0)
			mbBold = True
			SheetHeading(String.Empty, "A" & miRow.ToString, "B" & miRow.ToString, "C", RGB(255, 255, 255))
			SheetHeading("CCMC", "C" & miRow.ToString, "E" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "F", "C")
			SheetHeading("Taylor", "G" & miRow.ToString, "I" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "J", "C")
			SheetHeading("Springfield", "K" & miRow.ToString, "M" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "N", "C")
			SheetHeading("DCMH", "O" & miRow.ToString, "Q" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "R", "C")
			SheetHeading("CKHS Hospitals Total", "S" & miRow.ToString, "U" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "V", "C")
			SheetHeading("HAN", "W" & miRow.ToString, "Y" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "Z", "C")
			SheetHeading("CKHS+CKHN Total", "AA" & miRow.ToString, "AC" & miRow.ToString, "C", RGB(255, 255, 255))

			'header row 2
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			SheetHeading("Month", "A" & miRow.ToString, "B" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell("Positive", "C", "C")
			ValueCell("Negative", "D", "C")
			ValueCell("Pending", "E", "C")
			ValueCell(String.Empty, "F", "C")
			ValueCell("Positive", "G", "C")
			ValueCell("Negative", "H", "C")
			ValueCell("Pending", "I", "C")
			ValueCell(String.Empty, "J", "C")
			ValueCell("Positive", "K", "C")
			ValueCell("Negative", "L", "C")
			ValueCell("Pending", "M", "C")
			ValueCell(String.Empty, "N", "C")
			ValueCell("Positive", "O", "C")
			ValueCell("Negative", "P", "C")
			ValueCell("Pending", "Q", "C")
			ValueCell(String.Empty, "R", "C")
			ValueCell("Positive", "S", "C")
			ValueCell("Negative", "T", "C")
			ValueCell("Pending", "U", "C")
			ValueCell(String.Empty, "V", "C")
			ValueCell("Positive", "W", "C")
			ValueCell("Negative", "X", "C")
			ValueCell("Pending", "YX", "C")
			ValueCell(String.Empty, "Z", "C")
			ValueCell("Positive", "AA", "C")
			ValueCell("Negative", "AB", "C")
			ValueCell("Pending", "AC", "C")

			'month detail
			iCurrentYear = 2020
			iCurrentMonth = 3
			bExit = False
			mbBold = False
			If Not oCOVID19Count.Open Then Throw New Exception(oCOVID19Count.ErrorMessage)

			Do Until bExit
				dtStartDate = CDate(iCurrentMonth.ToString & "/01/" & iCurrentYear)
				dtEndDate = DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, dtStartDate))
				If Not oCOVID19Count.CountByDistrictCode(dtStartDate, dtEndDate) Then Throw New Exception(oCOVID19Count.ErrorMessage)

				miRow += 1
				sWork = iCurrentYear.ToString & "-" & MonthName(iCurrentMonth, True).ToString
				If iCurrentYear = Year(dtYesterdayDateTime) And iCurrentMonth = Month(dtYesterdayDateTime) And
					Day(dtYesterdayDateTime) <> Day(DateAdd(DateInterval.Day, -1, DateAdd(DateInterval.Month, 1, CDate(iCurrentMonth.ToString & "/01/" & iCurrentYear)))) Then sWork = sWork & " (MTD)"
				SheetHeading(sWork, "A" & miRow.ToString, "b" & miRow.ToString, "C", RGB(255, 255, 255))
				ValueCell(oCOVID19Count.CCMCPositive.ToString, "C", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CCMCNegative.ToString, "D", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CCMCPending.ToString, "E", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "F", "C")
				ValueCell(oCOVID19Count.TaylorPositive.ToString, "G", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.TaylorNegative.ToString, "H", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.TaylorPending.ToString, "I", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "J", "C")
				ValueCell(oCOVID19Count.SpringfieldPositive.ToString, "K", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.SpringfieldNegative.ToString, "L", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.SpringfieldPending.ToString, "M", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "N", "C")
				ValueCell(oCOVID19Count.DCMHPositive.ToString, "O", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.DCMHNegative.ToString, "P", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.DCMHPending.ToString, "Q", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "R", "C")
				ValueCell(oCOVID19Count.CKHSPositive.ToString, "S", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSNegative.ToString, "T", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSPending.ToString, "U", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "V", "C")
				ValueCell(oCOVID19Count.HANPositive.ToString, "W", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.HANNegative.ToString, "X", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.HANPending.ToString, "Y", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "Z", "C")
				ValueCell(oCOVID19Count.CKHSHANPositive.ToString, "AA", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSHANNegative.ToString, "AB", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSHANPending.ToString, "AC", "C", "#,##0;-#,##0")

				iCurrentMonth += 1
				If iCurrentMonth > 12 Then
					iCurrentMonth = 1
					iCurrentYear += 1
				End If
				If (iCurrentYear * 100) + iCurrentMonth > (Year(dtYesterdayDateTime) * 100) + Month(dtYesterdayDateTime) Then bExit = True
			Loop

			If Not oCOVID19Count.CountByDistrictCode(dtBeginDate, dtYesterdayDateTime) Then Throw New Exception(oCOVID19Count.ErrorMessage)
			'iRecordCount += 1
			miRow += 1
			mbBold = True
			SheetHeading(Space(3) & "Total", "A" & miRow.ToString, "B" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(oCOVID19Count.CCMCPositive.ToString, "C", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CCMCNegative.ToString, "D", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CCMCPending.ToString, "E", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "F", "C")
			ValueCell(oCOVID19Count.TaylorPositive.ToString, "G", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.TaylorNegative.ToString, "H", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.TaylorPending.ToString, "I", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "J", "C")
			ValueCell(oCOVID19Count.SpringfieldPositive.ToString, "K", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.SpringfieldNegative.ToString, "L", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.SpringfieldPending.ToString, "M", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "N", "C")
			ValueCell(oCOVID19Count.DCMHPositive.ToString, "O", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.DCMHNegative.ToString, "P", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.DCMHPending.ToString, "Q", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "R", "C")
			ValueCell(oCOVID19Count.CKHSPositive.ToString, "S", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSNegative.ToString, "T", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSPending.ToString, "U", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "V", "C")
			ValueCell(oCOVID19Count.HANPositive.ToString, "W", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.HANNegative.ToString, "X", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.HANPending.ToString, "Y", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "Z", "C")
			ValueCell(oCOVID19Count.CKHSHANPositive.ToString, "AA", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSHANNegative.ToString, "AB", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSHANPending.ToString, "AC", "C", "#,##0;-#,##0")

			'day totals
			'blank row
			miRow += 2

			'header row 1
			miRow += 1
			miFontSize = 10
			RowHeight(15.0)
			mbBold = True
			SheetHeading(String.Empty, "A" & miRow.ToString, "B" & miRow.ToString, "C", RGB(255, 255, 255))
			SheetHeading("CCMC", "C" & miRow.ToString, "E" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "F", "C")
			SheetHeading("Taylor", "G" & miRow.ToString, "I" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "J", "C")
			SheetHeading("Springfield", "K" & miRow.ToString, "M" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "N", "C")
			SheetHeading("DCMH", "O" & miRow.ToString, "Q" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "R", "C")
			SheetHeading("CKHS Hospitals Total", "S" & miRow.ToString, "U" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "V", "C")
			SheetHeading("HAN", "W" & miRow.ToString, "Y" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(String.Empty, "Z", "C")
			SheetHeading("CKHS+CKHN Total", "AA" & miRow.ToString, "AC" & miRow.ToString, "C", RGB(255, 255, 255))

			'header row 2
			miRow += 1
			miFontSize = 11
			RowHeight(15.0)
			miColor = RGB(255, 255, 255)
			mbBold = True
			ValueCell("Date", "A", "C")
			ValueCell("Day", "B", "C")
			ValueCell("Positive", "C", "C")
			ValueCell("Negative", "D", "C")
			ValueCell("Pending", "E", "C")
			ValueCell(String.Empty, "F", "C")
			ValueCell("Positive", "G", "C")
			ValueCell("Negative", "H", "C")
			ValueCell("Pending", "I", "C")
			ValueCell(String.Empty, "J", "C")
			ValueCell("Positive", "K", "C")
			ValueCell("Negative", "L", "C")
			ValueCell("Pending", "M", "C")
			ValueCell(String.Empty, "N", "C")
			ValueCell("Positive", "O", "C")
			ValueCell("Negative", "P", "C")
			ValueCell("Pending", "Q", "C")
			ValueCell(String.Empty, "R", "C")
			ValueCell("Positive", "S", "C")
			ValueCell("Negative", "T", "C")
			ValueCell("Pending", "U", "C")
			ValueCell(String.Empty, "V", "C")
			ValueCell("Positive", "W", "C")
			ValueCell("Negative", "X", "C")
			ValueCell("Pending", "YX", "C")
			ValueCell(String.Empty, "Z", "C")
			ValueCell("Positive", "AA", "C")
			ValueCell("Negative", "AB", "C")
			ValueCell("Pending", "AC", "C")

			'detail
			miColor = RGB(255, 255, 255)
			miFontSize = 11
			RowHeight(15.0)
			mbBold = False
			'If Not oCOVID19Count.Open Then Throw New Exception(oCOVID19Count.ErrorMessage)
			For iWork = 1 To iDays
				dtStartDate = DateAdd(DateInterval.Day, iWork - 1, dtBeginDate)
				dtEndDate = dtStartDate
				If iWork = 1 Then dtStartDateTotal = dtStartDate
				If iWork = iDays Then dtEndDateTotal = dtEndDate
				If Not oCOVID19Count.CountByDistrictCode(dtStartDate, dtEndDate) Then Throw New Exception(oCOVID19Count.ErrorMessage)
				'iRecordCount += 1
				miRow += 1
				ValueCell(Format(dtStartDate, "MM/dd/yyyy"), "A", "C")
				ValueCell(Format(dtStartDate, "ddd"), "B", "C")
				ValueCell(oCOVID19Count.CCMCPositive.ToString, "C", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CCMCNegative.ToString, "D", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CCMCPending.ToString, "E", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "F", "C")
				ValueCell(oCOVID19Count.TaylorPositive.ToString, "G", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.TaylorNegative.ToString, "H", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.TaylorPending.ToString, "I", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "J", "C")
				ValueCell(oCOVID19Count.SpringfieldPositive.ToString, "K", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.SpringfieldNegative.ToString, "L", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.SpringfieldPending.ToString, "M", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "N", "C")
				ValueCell(oCOVID19Count.DCMHPositive.ToString, "O", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.DCMHNegative.ToString, "P", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.DCMHPending.ToString, "Q", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "R", "C")
				ValueCell(oCOVID19Count.CKHSPositive.ToString, "S", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSNegative.ToString, "T", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSPending.ToString, "U", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "V", "C")
				ValueCell(oCOVID19Count.HANPositive.ToString, "W", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.HANNegative.ToString, "X", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.HANPending.ToString, "Y", "C", "#,##0;-#,##0")

				ValueCell(String.Empty, "Z", "C")
				ValueCell(oCOVID19Count.CKHSHANPositive.ToString, "AA", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSHANNegative.ToString, "AB", "C", "#,##0;-#,##0")
				ValueCell(oCOVID19Count.CKHSHANPending.ToString, "AC", "C", "#,##0;-#,##0")
			Next iWork
			If Not oCOVID19Count.CountByDistrictCode(dtStartDateTotal, dtEndDateTotal) Then Throw New Exception(oCOVID19Count.ErrorMessage)
			'iRecordCount += 1
			miRow += 1
			mbBold = True
			SheetHeading(Space(3) & "Total", "A" & miRow.ToString, "B" & miRow.ToString, "C", RGB(255, 255, 255))
			ValueCell(oCOVID19Count.CCMCPositive.ToString, "C", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CCMCNegative.ToString, "D", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CCMCPending.ToString, "E", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "F", "C")
			ValueCell(oCOVID19Count.TaylorPositive.ToString, "G", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.TaylorNegative.ToString, "H", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.TaylorPending.ToString, "I", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "J", "C")
			ValueCell(oCOVID19Count.SpringfieldPositive.ToString, "K", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.SpringfieldNegative.ToString, "L", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.SpringfieldPending.ToString, "M", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "N", "C")
			ValueCell(oCOVID19Count.DCMHPositive.ToString, "O", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.DCMHNegative.ToString, "P", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.DCMHPending.ToString, "Q", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "R", "C")
			ValueCell(oCOVID19Count.CKHSPositive.ToString, "S", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSNegative.ToString, "T", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSPending.ToString, "U", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "V", "C")
			ValueCell(oCOVID19Count.HANPositive.ToString, "W", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.HANNegative.ToString, "X", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.HANPending.ToString, "Y", "C", "#,##0;-#,##0")

			ValueCell(String.Empty, "Z", "C")
			ValueCell(oCOVID19Count.CKHSHANPositive.ToString, "AA", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSHANNegative.ToString, "AB", "C", "#,##0;-#,##0")
			ValueCell(oCOVID19Count.CKHSHANPending.ToString, "AC", "C", "#,##0;-#,##0")

			COVID19CountByDistrictCode = True
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
			COVID19CountByDistrictCode = False
		Finally
			oCOVID19Count.Close()
			ReleaseObject(moWorkSheet)
			TraceWrite("Complete Processing TabCode: " & ", SiteName: " & TabName, sRoutine)
		End Try
	End Function

	Public Sub RemovePastCOVID19Documents()
		Dim sRoutine As String = "RemovePastCOVID19Documents"
		Dim sFilePattern As String = gsCOVID19ReportFileName & "*.xlsx"
		Dim oFiles() As String = Directory.GetFiles(gsBuildPath, sFilePattern)
		Dim oFileName As String
		Dim iRemoveCount As Int32 = 0

		Try
			TraceWrite("Start Remove Past COVID-19 documents processing", sRoutine)
			If msCurrentCOVID19FileName.Length <= 0 Then
				TraceWrite("Current file name is blank, remove processing canceled.", sRoutine)
				Exit Sub
			End If
			TraceWrite("Checking: " & AddBackslash(gsBuildPath) & sFilePattern, sRoutine)
			If oFiles.Count > 0 Then
				For Each oFileName In oFiles
					If Path.GetFileName(oFileName).ToLower <> msCurrentCOVID19FileName.ToLower Then
						If File.Exists(oFileName) Then
							DeleteFile(oFileName)
							If Not File.Exists(oFileName) Then iRemoveCount += 1
						End If
					End If
				Next
				TraceWrite("Removed " & Format(iRemoveCount, "#,##0") & " files.", sRoutine)
			Else
				TraceWrite("No files found.", sRoutine)
			End If
			TraceWrite("End Remove Past COVID-19 documents processing", sRoutine)
		Catch oException As Exception
			TraceWrite("Error: " & oException.Message, sRoutine)
		Finally
		End Try
	End Sub

	Public Function EMailCOVID19Report() As Boolean
		Dim sRoutine As String = "EMailCOVID19Report"
		Dim oSMTPEMail As New SMTPEMail.Mail
		Dim iWork As Int32
		Dim dtCurrentDateTime As Date = Now
		Dim sEMailMessage As String

		Try
			TraceWrite("Start email COVID-19 lab report", sRoutine)
			If Not gbSendEMail OrElse galCOVID19LabReportEMail.Count <= 0 Then Return True
			If msCurrentCOVID19FileName.Length <= 0 Then Throw New Exception("Current file name is blank, email processing canceled.")
			If Not File.Exists(AddBackslash(gsBuildPath) & msCurrentCOVID19FileName) Then Throw New Exception("Could not locate file " & gsBuildPath & msCurrentCOVID19FileName & ", email processing canceled.")

			For iWork = 0 To galCOVID19LabReportEMail.Count - 1
				oSMTPEMail = New SMTPEMail.Mail
				If Not oSMTPEMail.Recipient(galCOVID19LabReportEMail.Item(iWork).ToString) Then Throw New Exception("Recipient EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.Sender(gsEMailSender) Then Throw New Exception("Sender EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.Subject("COVID-19 Lab Report") Then Throw New Exception("Subject EMail Error: " & oSMTPEMail.ErrorMessage)
				sEMailMessage = "Attached is your COVID-19 Lab Report for " & MonthName(Month(dtCurrentDateTime)) & Space(1) & Format(dtCurrentDateTime, "dd, yyyy") & "<br />"
				sEMailMessage = sEMailMessage & "<br />" & "<br />" & Disclaimer() & "<br />"
				If Not oSMTPEMail.Message(sEMailMessage) Then Throw New Exception("Message EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.AddAttachment(AddBackslash(gsBuildPath) & msCurrentCOVID19FileName) Then Throw New Exception("AddAttachment EMail Error: " & oSMTPEMail.ErrorMessage)
				If Not oSMTPEMail.Send(gsSMTPAddress) Then Throw New Exception("Send EMail Error: " & oSMTPEMail.ErrorMessage)
				Thread.Sleep(giEMailPauseTime)
			Next iWork

			TraceWrite("End email COVID-19 lab report", sRoutine)
			EMailCOVID19Report = True
		Catch oException As Exception
			ErrorMessage = oException.Message
			TraceWrite("Error: " & ErrorMessage, sRoutine)
			EMailCOVID19Report = False
		Finally
		End Try
	End Function
#End Region

#Region "COVID19VisiquateUpdate"
	Public Sub COVID19VisiquateUpdate()
		Dim sRoutine As String = "COVID19VisiquateUpdate"
		Dim dtCurrentDateTime As Date = Now
		Dim sCurrentDate As String
		Dim oProcessed As New Processed
		Dim iProcessTypeID As Int32
		Dim iWork As Int32
		Dim bProcessed As Boolean = False

		Try
			sCurrentDate = Format$(dtCurrentDateTime, "MM/dd/yyyy")
			If galCOVID19VisiquateUpdateTime.Count <= 0 Then Exit Sub
			TraceWrite("Start COVID-19 Visiquate update", sRoutine)

			For iWork = 0 To galCOVID19VisiquateUpdateTime.Count - 1
				If CDate(sCurrentDate & " " & galCOVID19VisiquateUpdateTime.Item(iWork).ToString) <= dtCurrentDateTime Then

					'eCare
					iProcessTypeID = 13
					If Not oProcessed.Complete02(iProcessTypeID, sCurrentDate, galCOVID19CharterCareUpdateTime.Item(iWork).ToString) Or gbByPassDateCheck Then
						If Not COVID19VisiquateProcessing() Then Throw New Exception(ErrorMessage)
						bProcessed = True
						If Not gbByPassDateCheck Then oProcessed.Insert02(iProcessTypeID, sCurrentDate, galCOVID19CharterCareUpdateTime.Item(iWork).ToString)
					Else
						TraceWrite("Processing already completed for: Process Type ID - " & iProcessTypeID.ToString & ", " & sCurrentDate & " " & galCOVID19CharterCareUpdateTime.Item(iWork).ToString, sRoutine)
					End If
				End If
			Next iWork


			If Not bProcessed Then TraceWrite("Processing is not required.", sRoutine)
			TraceWrite("End COVID-19 Visiquate update", sRoutine)
		Catch oException As Exception
			TraceWrite("ServiceRoutines.COVID19VisiquateUpdate Error: " & oException.Message, sRoutine)
			gsErrorMessage = oException.Message
			Error01EMail()
		Finally
			oProcessed = Nothing
		End Try
	End Sub

	Public Function COVID19VisiquateProcessing() As Boolean
		Dim oSqlConnectionStringBuilder As SqlConnectionStringBuilder = New SqlConnectionStringBuilder
		Dim oSqlConnection As SqlConnection

		'Dim oDBC As OleDbConnection = New OleDbConnection
		'Dim oCmd As OleDbCommand = New OleDbCommand
		'Dim oRdr As OleDbDataReader = Nothing
		'Dim oCOVID19Patient As New COVID19Patient
		'Dim iPatientSourceID As Int32 = 3
		'Dim sGroupKey As String = Guid.NewGuid.ToString()

		COVID19VisiquateProcessing = False
		Try
			oSqlConnectionStringBuilder.DataSource = "pmhpro.database.windows.net"
			oSqlConnectionStringBuilder.UserID = "ostl00@ckhsad.crozer.org"
			oSqlConnectionStringBuilder.Password = "warrior3"
			oSqlConnectionStringBuilder.InitialCatalog = "pmh_datamart"
			oSqlConnectionStringBuilder.Authentication = SqlAuthenticationMethod.ActiveDirectoryPassword
			'oSqlConnectionStringBuilder.Encrypt
			oSqlConnection = New SqlConnection(oSqlConnectionStringBuilder.ConnectionString)
			oSqlConnection.Open()


			oSqlConnection.Close()
			oSqlConnection.Dispose()


			'If Not OpenDB(oDBC, 5) Then Throw New Exception("Error opening database.")
			'oCmd.CommandText = "Clinical.sp_CovidResults"
			'oCmd.CommandType = CommandType.StoredProcedure
			'oCmd.CommandTimeout = giLongSQLCommandTimeout
			'oCmd.Parameters.Add("@startDate", OleDbType.Date).Value = CDate("01/01/2020")
			'oCmd.Parameters.Add("@endDate", OleDbType.Date).Value = CDate("12/31/2020")
			'oCmd.Connection = oDBC
			'oRdr = oCmd.ExecuteReader
			'If oRdr.HasRows Then
			'	If Not oCOVID19Patient.Open Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			'	While oRdr.Read()
			'		oCOVID19Patient.NewPatient()
			'		oCOVID19Patient.PatientSourceID = iPatientSourceID
			'		oCOVID19Patient.LastName = SetString(oRdr!LastName)
			'		oCOVID19Patient.FirstName = SetString(oRdr!FirstName)
			'		oCOVID19Patient.MiddleName = SetString(oRdr!MiddleName)
			'		oCOVID19Patient.Gender = SetString(oRdr!Gender)
			'		oCOVID19Patient.DateOfBirth = SetDate(oRdr!DateOfBirth)
			'		oCOVID19Patient.ZipCode = SetString(oRdr!ZipCode)
			'		oCOVID19Patient.DistrictCode = SetString(oRdr!DistrictCode)
			'		oCOVID19Patient.PatientID = SetString(oRdr!PatientID)
			'		oCOVID19Patient.PatientIDStartDateTime = SetDate(oRdr!PatientIDStartDateTime)
			'		oCOVID19Patient.MedicalRecordNumber = SetString(oRdr!MedicalRecordNumber)
			'		oCOVID19Patient.NHSIDNumber = SetString(oRdr!NHSIDNumber)
			'		oCOVID19Patient.AdmitDateTime = SetDate(oRdr!AdmitDateTime)
			'		oCOVID19Patient.DischargeDateTime = SetDate(oRdr!DischargeDateTime)
			'		oCOVID19Patient.OrderCode = SetString(oRdr!OrderCode)
			'		oCOVID19Patient.OrderDescription = SetString(oRdr!OrderDescription)
			'		oCOVID19Patient.OrderLocation = SetString(oRdr!OrderLocation)
			'		oCOVID19Patient.OrderingPhysicanNumber = SetString(oRdr!OrderingPhysicianNumber)
			'		oCOVID19Patient.OrderingPhysicianName = SetString(oRdr!OrderingPhysicianName)
			'		oCOVID19Patient.ResultCode = SetString(oRdr!ResultCode)
			'		oCOVID19Patient.ResultCodeName = SetString(oRdr!ResultCodeName)
			'		oCOVID19Patient.ResultCreateDateTime = SetDate(oRdr!ResultCreateDateTime)
			'		oCOVID19Patient.ResultDisplay = SetString(oRdr!ResultDisplay)
			'		oCOVID19Patient.ResultText = SetString(oRdr!ResultText)
			'		oCOVID19Patient.FinalResultID = 0
			'		oCOVID19Patient.FinalResultDateTime = String.Empty
			'		oCOVID19Patient.OrderStatusCode = SetString(oRdr!OrderStatusCode)
			'		oCOVID19Patient.OrderStatus = SetString(oRdr!OrderStatus)
			'		oCOVID19Patient.DischargeDisposition = SetString(oRdr!DischargeDisposition)
			'		oCOVID19Patient.VisitType = SetString(oRdr!VisitType)
			'		oCOVID19Patient.HospitalService = String.Empty
			'		oCOVID19Patient.PatientType = String.Empty
			'		oCOVID19Patient.OrderProcessDateTime = Nothing
			'		oCOVID19Patient.OrderNumber = 0
			'		oCOVID19Patient.GroupKey = sGroupKey
			'		If Not oCOVID19Patient.Insert01 Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			'	End While
			'	oCOVID19Patient.Close()
			'	If Not oCOVID19Patient.FinalResultByPatientSource(sGroupKey, 3) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			'	If Not oCOVID19Patient.DeleteByPastKey(iPatientSourceID, sGroupKey) Then Throw New Exception(oCOVID19Patient.ErrorMessage)
			COVID19VisiquateProcessing = True
			'End If
		Catch oException As Exception
			ErrorMessage = "ServiceRoutines.COVID19VisiquateProcessing: " & oException.Message
			'oCOVID19Patient.DeleteByCurrentKey(iPatientSourceID, sGroupKey)
		Finally
			'CloseRDR(oRdr)
			'CloseCMD(oCmd)
			'CloseDB(oDBC)
		End Try
	End Function
#End Region
End Module
