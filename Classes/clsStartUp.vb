#Region "Init"
Option Strict On
Option Explicit On
Imports System.Reflection
Imports System.Security.Principal
#End Region

Public Class StartUp
#Region "Declarations"
  Private msErrorMessage As String = vbNullString
#End Region

#Region "Class Properties"
  Public ReadOnly Property ErrorMessage() As String
    Get
      Return msErrorMessage
    End Get
  End Property
#End Region

#Region "StartUp Routine"
  Public ReadOnly Property Run() As Boolean
    Get
      Dim oInitializeApplication As New InitializeApplication
      Dim oWindowsPrincipal As WindowsPrincipal
      Dim oWindowsIdentity As WindowsIdentity

      Try
        gsAppPath = ApplicationStartupPath()
				gsGraphicsPath = AddBackslash(gsAppPath) & "Graphics\"
				gsScriptPath = AddBackslash(gsAppPath) & "Scripts\"
				gsHTMLPath = AddBackslash(gsAppPath) & "HTML\"
				gsVersion = Assembly.GetExecutingAssembly.GetName.Version.Major.ToString & "." & Assembly.GetExecutingAssembly.GetName.Version.Minor.ToString & "." & Format$(Assembly.GetExecutingAssembly.GetName.Version.Build, "0###") & "." & Format$(Assembly.GetExecutingAssembly.GetName.Version.Revision, "0###")
				AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal)
        oWindowsPrincipal = CType(System.Threading.Thread.CurrentPrincipal, WindowsPrincipal)
        oWindowsIdentity = (CType(oWindowsPrincipal.Identity, WindowsIdentity))
        gsWindowsUserID = CType(GetUser(oWindowsIdentity.Name), String)

				With oInitializeApplication
					If Not .GetSettings(AddBackslash(gsAppPath) & gsINIFileName) Then
						WriteEventTrace("Invalid or missing start-up file.", "StartUp", EventLogEntryType.Error)
						End
					End If
					'gsEMRStatConnectionString = EMRStatDataBaseConnectionString()
					'gsCKEMR01ConnectionString = CKEMR01ConnectionString()
					'gsCommEMR1ConnectionString = CommEMR1ConnectionString()
					gsLogPath = AddBackslash(.LogPath)
					gbTrace = .Trace
					giServicePauseTime = .ServicePauseTime
					galEMailRecipient = .EMailRecipient
					gsEMailSender = .EMailSender
					gbSendEMail = .SendEMail
					gbSendErrorEMail = .SendErrorEMail
					gsUnsignedDocumentsTime = .UnsignedDocumentsTime
					gsReportRecipientUnsignedDocuments = .ReportRecipientUnsignedDocuments
					'gbByPassDateCheck = False
					galUnsignedDocumentsDay = .UnsignedDocumentsDay
					giUnsignedDocumentsInterval = .UnsignedDocumentsInterval
					gsDischargeSumTime = .DischargeSumTime
					gsEMailCheckBoxScriptTime = .EMailCheckBoxScriptTime
					gsReportRecipientEMailCheckBoxScript = .ReportRecipientEMailCheckBoxScript
					galCOVID19WarehouseUpdateTime = .COVID19WarehouseUpdateTime
					galCOVID19CharterCareUpdateTime = .COVID19CharterCareUpdateTime
					gsCOVID19WaterburyInputfolder = .COVID19WaterburyInputFolder
					gsCOVID19WaterburyOutputFolder = .COVID19WaterburyOutputFolder
					galCOVID19WaterburyOutputTime = .COVID19WaterburyOutputTime
					gsCOVID19ECHNInputFolder = .COVID19ECHNInputFolder
					gsCOVID19EOGHInputFolder = .COVID19EOGHInputFolder
					gsCOVID19AltaInputFolder = .COVID19AltaInputFolder
					galCOVID19HANUpdateTime = .COVID19HANUpdateTime
					galCOVID19ReportTime = .COVID19ReportTime
					gbByPassDateCheck = .ByPassDateCheck
					galCOVID19LabReportEMail = .COVID19LabReportEMail
					galCOVID19VisiquateUpdateTime = .COVID19VisiquateUpdateTime
					gsCOVID19PMHOutputFolder = .COVID19PMHOutputFolder
				End With

				If Not oInitializeApplication.VersionCheck(gsVersion) Then
          WriteEventTrace("Error: The version of this service (" & gsVersion & ") doesn't match the database version.", "StartUp", EventLogEntryType.Error)
          End
        End If
      oInitializeApplication = Nothing

      WriteEventTrace("Initialization Complete: Processing!", "StartUp", EventLogEntryType.Information)
      WriteEventLog("For further service initialization information see the service log." & vbCrLf & "Log File: " & gsLogPath & gsLogFileName, "StartUp", EventLogEntryType.Information)
            Catch oException As Exception
        WriteEventTrace("Global Initialization Error: " & oException.Message, "StartUp", EventLogEntryType.Error)
        End
      End Try
      Return True
    End Get
  End Property
#End Region
End Class
