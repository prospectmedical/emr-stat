#Region "Init"
Option Strict On
Option Explicit On
Imports System.Data.OleDb
Imports Oracle.DataAccess.Client
#End Region

Module DBRoutines
#Region "Declartions"
	'Private Const EMRStatConnectionString As String = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=EMRStat;Password=App9968523;Initial Catalog=EMRStat;Data Source=SQLDB04;Use Procedure for Prepare=1;" &
	'	"Auto Translate=True;Packet Size=4096;Workstation ID=AIApplication;Use Encryption for Data=False;Tag with column collation when possible=False"

	Private Const EMRStatConnectionString As String = "Provider=SQLOLEDB.1;User ID=EMRStat;Password=App9968523;Initial Catalog=EMRStat;Data Source=SQLDB04;"

	Private Const EMR01ConnectionString As String = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=EMRDirector;Password=edapp8751;Initial Catalog=EMR01;Data Source=SQLDB04;Use Procedure for Prepare=1;" &
		"Auto Translate=True;Packet Size=4096;Workstation ID=AIApplication;Use Encryption for Data=False;Tag with column collation when possible=False"

	Private Const CKEMR01ConnectionString As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=EMR08)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=CKEMR01)));User Id=ml;Password=ml;"

	Private Const CommEMR1ConnectionString As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=CKCHEC03)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=CommEMR1)));User Id=ml;Password=ml;"

	Private Const WarehouseConnectionString As String = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=EDStat;Password=EDSApp96654;Initial Catalog=SMSPHdss10z0;Data Source=CERNERBI02;Use Procedure for Prepare=1;" &
		"Auto Translate=True;Packet Size=4096;Workstation ID=AIApplication;Use Encryption for Data=False;Tag with column collation when possible=False"

	Private Const CharteCareConnectionString As String = "Provider=SQLOLEDB.1;User ID=CROZER;Password=4B6EfmUSpZ3c9ErYtYFR;Initial Catalog=user-live;Data Source=10.2.3.118;" &
		"Integrated Security=SSPI; Trusted-Connection=True"

	'Private Const VisiquateConnectionString As String = "Provider=SQLOLEDB.1;User ID=;Password=;Initial Catalog=pmh_datamart;Data Source=pmhpro.database.windows.net;"
#End Region

#Region "OpenDB"
	''' <summary>
	''' Open database
	''' </summary>
	''' <param name="DatabaseConnection">Database connection string</param>
	''' <param name="ConnectionType">1=EMRStat, 2=EMR01, 3=Warehouse</param>
	''' <remarks></remarks>
	Public Function OpenDB(DatabaseConnection As OleDbConnection, Optional ConnectionType As Int32 = 1) As Boolean
		Try
			Select Case ConnectionType
				Case 1
					DatabaseConnection.ConnectionString = EMRStatConnectionString
				Case 2
					DatabaseConnection.ConnectionString = EMR01ConnectionString
				Case 3
					DatabaseConnection.ConnectionString = WarehouseConnectionString
				Case 4
					DatabaseConnection.ConnectionString = CharteCareConnectionString
					'Case 5
					'	DatabaseConnection.ConnectionString = VisiquateConnectionString
				Case Else
					DatabaseConnection.ConnectionString = EMRStatConnectionString
			End Select
			'DatabaseConnection.ConnectionTimeout = 60
			DatabaseConnection.Open()
			OpenDB = True
		Catch oException As Exception
			OpenDB = False
		Finally
		End Try
	End Function

	''' <summary>
	''' Open an Oracle database
	''' </summary>
	''' <param name="DatabaseConnection">Oracle database connection string</param>
	''' <param name="DBToOpen">0=EMR08.CKEMR01, 1=CKCHEC03.CommEMR1</param>
	''' <remarks></remarks>
	Public Function OpenDBOracle(DatabaseConnection As OracleConnection, DBToOpen As Int32) As Boolean
		Try
			Select Case DBToOpen
				Case 0
					DatabaseConnection.ConnectionString = CKEMR01ConnectionString
				Case 1
					DatabaseConnection.ConnectionString = CommEMR1ConnectionString
				Case Else
					Throw New Exception("Invalid database specified")
			End Select
			DatabaseConnection.Open()
			OpenDBOracle = True
		Catch oException As Exception
			OpenDBOracle = False
		Finally
		End Try
	End Function
#End Region

#Region "CloseDB"
	Public Sub CloseDB(oOleDbConnection As OleDbConnection)
		Try
			oOleDbConnection.Close()
			oOleDbConnection.Dispose()
			oOleDbConnection = Nothing
		Catch oException As Exception
		End Try
	End Sub

	Public Sub CloseDBOracle(oOracleConnection As OracleConnection)
		Try
			oOracleConnection.Close()
			oOracleConnection.Dispose()
			oOracleConnection = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region

#Region "CloseRDR"
	Public Sub CloseRDR(oOleDbDataReader As OleDbDataReader)
		Try
			oOleDbDataReader.Close()
			oOleDbDataReader = Nothing
		Catch oException As Exception
		End Try
	End Sub

	Public Sub CloseRDROracle(oOracleDataReader As OracleDataReader)
		Try
			oOracleDataReader.Close()
			oOracleDataReader = Nothing
		Catch oException As Exception
		End Try
	End Sub
#End Region

#Region "CloseCMD"
	Public Sub CloseCMD(oOleDbCommand As OleDbCommand)
		Try
			oOleDbCommand.Dispose()
		Catch oException As Exception
		End Try
	End Sub

	Public Sub CloseCMDOracle(oOracleCommand As OracleCommand)
		Try
			oOracleCommand.Dispose()
		Catch oException As Exception
		End Try
	End Sub
#End Region
End Module
