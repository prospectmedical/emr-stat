#Region "Init"
Option Explicit On
Option Strict On
Imports System.Threading
#End Region

#Region "Enums"
Public Enum CustomCommands
  StopProcess = 128
  RestartProcess = 129
  CheckProcess = 130
End Enum
#End Region

Public Class EMRStatService
#Region "Declaraions"
  Private Shared moTextWriterTraceListener As TextWriterTraceListener = Nothing
  Private moThread As Thread = Nothing
#End Region

#Region "Service Processing"
	Public Sub New()
		InitializeComponent()
		CanPauseAndContinue = False
		ServiceName = "EMRStat"

		COVID19CharterCareUpdate()
		Trace.Close()
		End

	End Sub

	Private Sub ServiceProcessMethod()
		Dim sRoutine As String = "ServiceProcessMethod"

		TraceWrite("Starting the service process thread.", sRoutine)
		Try
			If gbSendEMail Then StartUpEMail()
			WriteEventTrace("Process Messages", sRoutine, EventLogEntryType.Information)
			Do
				UnsignedDocuments()
				DischargeSum()
				'EMailCheckBoxScript
				COVID19WarehouseUpdate()
				COVID19HANUpdate()
				COVID19CharterCareUpdate()
				COVID19WaterburyUpdate()
				COVID19ECHNUpdate()
				COVID19EOGHUpdate()
				COVID19AltaUpdate()
				COVID19WaterburyData()
				COVID19PMHData()
				COVID19Report()
				'COVID19VisiquateUpdate()

				TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
				TraceWrite(StringRepeat("-", 100), sRoutine)
				TraceWrite(StringRepeat(Chr(127), 100), sRoutine)
				TraceWrite(Space$(1), sRoutine)
				Thread.Sleep(giServicePauseTime)
			Loop While True
		Catch oThreadAbortException As ThreadAbortException
			WriteEventTrace("Thread abort signaled.", sRoutine, EventLogEntryType.Information)
		Finally
			WriteEventTrace("Exiting the service process thread.", sRoutine, EventLogEntryType.Information)
			Trace.Close()
			If gbSendEMail Then ShutdownEMail()
			moThread.Join(500)
			moThread = Nothing
			Me.Stop()
		End Try
	End Sub
#End Region

#Region "Service Control"
  Protected Overrides Sub OnStart(args() As String)
    If (moThread Is Nothing) OrElse ((moThread.ThreadState And (System.Threading.ThreadState.Unstarted Or System.Threading.ThreadState.Stopped)) <> 0) Then
      WriteEventTrace("Starting the service process thread.", "OnStart", EventLogEntryType.Information)
      moThread = New Thread(New ThreadStart(AddressOf ServiceProcessMethod))
      moThread.Start()
    End If
    If Not moThread Is Nothing Then
      WriteEventTrace("Process thread state = " & moThread.ThreadState.ToString(), "OnStart", EventLogEntryType.Information)
    End If
  End Sub

  Protected Overrides Sub OnStop()
    If (Not moThread Is Nothing) AndAlso moThread.IsAlive Then
      WriteEventTrace("Stopping the service process thread.", "OnStop", EventLogEntryType.Information)
      moThread.Abort()
      moThread.Join(500)
    End If
    If Not moThread Is Nothing Then
      WriteEventTrace("Process thread state = " & moThread.ThreadState.ToString(), "OnStop", EventLogEntryType.Information)
    End If
  End Sub

  Protected Overrides Sub OnCustomCommand(Command As Integer)
    WriteEventTrace("Custom command received: " & Command.ToString(), "OnCustomCommand", EventLogEntryType.Information)
    Select Case Command
      Case CustomCommands.StopProcess
        OnStop()
      Case CustomCommands.RestartProcess
        OnStart(Nothing)
      Case CustomCommands.CheckProcess
        WriteEventTrace("Process thread state = " & moThread.ThreadState.ToString(), "OnCustomCommand", EventLogEntryType.Information)
        WriteEventTrace("Inrecognized custom command ignored!", "OnCustomCommand", EventLogEntryType.Information)
    End Select
  End Sub
#End Region
End Class
