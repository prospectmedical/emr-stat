#Region "Init"
Option Explicit On
Option Strict On
#End Region

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class EMRStatService
#Region "Declarations"
  Inherits System.ServiceProcess.ServiceBase
  Private oIContainer As System.ComponentModel.IContainer
#End Region

#Region "Routines"
	<MTAThread()> <System.Diagnostics.DebuggerNonUserCode()> Public Shared Sub Main()
		gsEventLogName = "EMR Stat"
		gsApplicationName = "EMR Stat"
		gsINIFileName = "EMRStat.ini"
		StartService(moTextWriterTraceListener)
	End Sub
#End Region

#Region "Service Control"
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overrides Sub Dispose(disposing As Boolean)
    If disposing AndAlso oIContainer IsNot Nothing Then
      oIContainer.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.CanShutdown = True
		Me.ServiceName = "EMRStat"
	End Sub
#End Region
End Class
