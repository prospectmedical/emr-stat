﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class Processed
#Region "Class Properties"
	Public Property ErrorMessage As String
#End Region

#Region "Routines"
	Public Sub New()
		Initialize()
	End Sub

	Public Sub Initialize()
		ErrorMessage = vbNullString
	End Sub
#End Region

#Region "UnsignedDocumentsRequired"
	Public Function UnsignedDocumentsRequired(UnsignedDocumentsDay As Int32) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing
		Dim dtCurrentDate As Date = CDate(Format(Now, "MM/dd/yyyy"))
		Dim sProcessingDate As String
		Dim dtProcessingDate As Date

		UnsignedDocumentsRequired = False
		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sProcessed02"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@DayOfWeek", OleDbType.BigInt).Value = UnsignedDocumentsDay
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oRdr.Read()
				sProcessingDate = SetString(oRdr!ProcessingDate)
				If IsDate(sProcessingDate) Then
					dtProcessingDate = CDate(sProcessingDate)
					If dtCurrentDate >= DateAdd(DateInterval.Day, giUnsignedDocumentsInterval, dtProcessingDate) Then UnsignedDocumentsRequired = True
				Else
					UnsignedDocumentsRequired = (UnsignedDocumentsDay = Now.DayOfWeek)
				End If
			Else
				UnsignedDocumentsRequired = (UnsignedDocumentsDay = Now.DayOfWeek)
			End If
		Catch oException As Exception
			ErrorMessage = "Processed.UnsignedDocumentsRequired: " & oException.Message
			UnsignedDocumentsRequired = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "Insert"
	Public Function Insert01(pProcessedTypeID As Int32, pProcessingDate As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "iProcessed01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessedTypeID", OleDbType.BigInt).Value = pProcessedTypeID
			oCmd.Parameters.Add("@ProcessingDate", OleDbType.VarChar, 10).Value = pProcessingDate
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			Insert01 = True
		Catch oException As Exception
			ErrorMessage = "Processed.Insert01: " & oException.Message
			Insert01 = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function Insert02(pProcessedTypeID As Int32, pProcessingDate As String, pProcessingTime As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening the database.")
			oCmd.CommandText = "iProcessed02"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessedTypeID", OleDbType.BigInt).Value = pProcessedTypeID
			oCmd.Parameters.Add("@ProcessingDate", OleDbType.VarChar, 10).Value = pProcessingDate
			oCmd.Parameters.Add("@ProcessingTime", OleDbType.VarChar, 10).Value = pProcessingTime
			oCmd.Connection = oDBC
			oCmd.ExecuteNonQuery()
			Insert02 = True
		Catch oException As Exception
			ErrorMessage = "Processed.Insert02: " & oException.Message
			Insert02 = False
		Finally
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region

#Region "Complete"
	Public Function Complete01(pProcessedTypeID As Int32, pProcessingDate As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sProcessed01"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessedTypeID", OleDbType.BigInt).Value = pProcessedTypeID
			oCmd.Parameters.Add("@ProcessingDate", OleDbType.VarChar, 10).Value = pProcessingDate
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			Complete01 = oRdr.HasRows
		Catch oException As Exception
			ErrorMessage = "Processed.Complete01: " & oException.Message
			Complete01 = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function

	Public Function Complete02(pProcessedTypeID As Int32, pProcessingDate As String, pProcessingTime As String) As Boolean
		Dim oDBC As OleDbConnection = New OleDbConnection
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			If Not OpenDB(oDBC) Then Throw New Exception("Error opening database.")
			oCmd.CommandText = "sProcessed03"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.Parameters.Add("@ProcessedTypeID", OleDbType.BigInt).Value = pProcessedTypeID
			oCmd.Parameters.Add("@ProcessingDate", OleDbType.VarChar, 10).Value = pProcessingDate
			oCmd.Parameters.Add("@ProcessingTime", OleDbType.VarChar, 5).Value = pProcessingTime
			oCmd.Connection = oDBC
			oRdr = oCmd.ExecuteReader
			Complete02 = oRdr.HasRows
		Catch oException As Exception
			ErrorMessage = "Processed.Complete02: " & oException.Message
			Complete02 = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
			CloseDB(oDBC)
		End Try
	End Function
#End Region
End Class


