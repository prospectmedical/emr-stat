﻿#Region "Init"
Option Explicit On
Option Strict On
Imports System.Data.OleDb
#End Region

Public Class COVID19Count
#Region "Declartions"
	Private moDBC As OleDbConnection = New OleDbConnection
#End Region

#Region "Class Properties"
	Public Property CCMCPositive As Int32
	Public Property CCMCNegative As Int32
	Public Property CCMCPending As Int32
	Public Property TaylorPositive As Int32
	Public Property TaylorNegative As Int32
	Public Property TaylorPending As Int32
	Public Property SpringfieldPositive As Int32
	Public Property SpringfieldNegative As Int32
	Public Property SpringfieldPending As Int32
	Public Property DCMHPositive As Int32
	Public Property DCMHNegative As Int32
	Public Property DCMHPending As Int32
	Public Property CKHSPositive As Int32
	Public Property CKHSNegative As Int32
	Public Property CKHSPending As Int32
	Public Property HANPositive As Int32
	Public Property HANNegative As Int32
	Public Property HANPending As Int32
	Public Property CKHSHANPositive As Int32
	Public Property CKHSHANNegative As Int32
	Public Property CKHSHANPending As Int32
	Public Property ErrorMessage As String
#End Region

#Region "Routines"
	Public Sub New()
		Initialize()
	End Sub

	Public Sub Initialize()
		moDBC = New OleDbConnection
		NewCount()
		ErrorMessage = vbNullString
	End Sub
#End Region

#Region "Open"
	Public Function Open() As Boolean
		Try
			If Not OpenDB(moDBC, 1) Then Throw New Exception("Error opening database.")
			Open = True
		Catch oException As Exception
			ErrorMessage = "COVID19Count.Open: " & oException.Message
			Open = False
		Finally
		End Try
	End Function
#End Region

#Region "NewCount"
	Public Sub NewCount()
		CCMCPositive = 0
		CCMCNegative = 2
		CCMCPending = 0
		TaylorPositive = 0
		TaylorNegative = 0
		TaylorPending = 0
		SpringfieldPositive = 0
		SpringfieldNegative = 0
		SpringfieldPending = 0
		DCMHPositive = 0
		DCMHNegative = 0
		DCMHPending = 0
		CKHSPositive = 0
		CKHSNegative = 0
		CKHSPending = 0
		HANPositive = 0
		HANNegative = 0
		HANPending = 0
		CKHSHANPositive = 0
		CKHSHANNegative = 0
		CKHSHANPending = 0
	End Sub
#End Region

#Region "Count"
	Public Function CountByDistrictCode(pStartDate As Date, pEndDate As Date) As Boolean
		Dim oCmd As OleDbCommand = New OleDbCommand
		Dim oRdr As OleDbDataReader = Nothing

		Try
			oCmd.CommandText = "sCOVID19CountByDistrictCode"
			oCmd.CommandType = CommandType.StoredProcedure
			oCmd.CommandTimeout = giLongSQLCommandTimeout
			oCmd.Parameters.Add("@StartDate", OleDbType.Date).Value = pStartDate
			oCmd.Parameters.Add("@EndDate", OleDbType.Date).Value = pEndDate
			oCmd.Connection = moDBC
			oRdr = oCmd.ExecuteReader
			If oRdr.HasRows Then
				oRdr.Read()
				CCMCPositive = SetInt32(oRdr!CCMCPositive)
				CCMCNegative = SetInt32(oRdr!CCMCNegative)
				CCMCPending = SetInt32(oRdr!CCMCPending)
				TaylorPositive = SetInt32(oRdr!TaylorPositive)
				TaylorNegative = SetInt32(oRdr!TaylorNegative)
				TaylorPending = SetInt32(oRdr!TaylorPending)
				SpringfieldPositive = SetInt32(oRdr!SpringfieldPositive)
				SpringfieldNegative = SetInt32(oRdr!SpringfieldNegative)
				SpringfieldPending = SetInt32(oRdr!SpringfieldPending)
				DCMHPositive = SetInt32(oRdr!DCMHPositive)
				DCMHNegative = SetInt32(oRdr!DCMHNegative)
				DCMHPending = SetInt32(oRdr!DCMHPending)
				CKHSPositive = SetInt32(oRdr!CKHSPositive)
				CKHSNegative = SetInt32(oRdr!CKHSNegative)
				CKHSPending = SetInt32(oRdr!CKHSPending)
				HANPositive = SetInt32(oRdr!HANPositive)
				HANNegative = SetInt32(oRdr!HANNegative)
				HANPending = SetInt32(oRdr!HANPending)
				CKHSHANPositive = SetInt32(oRdr!CKHSHANPositive)
				CKHSHANNegative = SetInt32(oRdr!CKHSHANNegative)
				CKHSHANPending = SetInt32(oRdr!CKHSHANPending)
			Else
				NewCount()
			End If
			CountByDistrictCode = True
		Catch oException As Exception
			ErrorMessage = "COVID19Count.sCOVID19CountByDistrictCode: " & oException.Message
			CountByDistrictCode = False
		Finally
			CloseRDR(oRdr)
			CloseCMD(oCmd)
		End Try
	End Function
#End Region

#Region "Close"
	Public Sub Close()
		Try
			CloseDB(moDBC)
		Catch oException As Exception
			ErrorMessage = "COVID19Count.Close: " & oException.Message
		Finally
		End Try
	End Sub
#End Region
End Class


