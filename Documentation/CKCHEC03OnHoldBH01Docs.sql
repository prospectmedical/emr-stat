select pers.searchname,pers.dateofbirth, usr.lastname,usr.firstname, doctype.description,doc.db_create_date,doc.summary,doc.status,loc.searchname as Practice,doc.visitid
from ((((document doc inner join person pers on doc.pid = pers.pid)
inner join usr usr on doc.usrid = usr.pvid)
inner join doctypes doctype on doc.doctype = doctype.dtid)
inner join locreg loc on doc.locofcare = loc.locid)
--inner join usr usr on doc.route_ids = usr.pvid)
where doc.status = 'H'
and loc.abbrevname like '%BH01%'
order by usr.lastname, doc.db_create_date asc;
