spool EMR_Script2_PatientDataAccess.log

-- Step 2
--*******************************************************************************************************
-- Author          :  BVPS Naidu / Nancy
-- USER STORY      :  SPR 72352, 72354, 72453 
-- Purpose         :  Apply API grant access MU EventID 1 min after previous denied access.
-- Create Date     :  20-Dec-2018
-- Notes           :  Applicable for New Install/Demo/Upgrade.
-- ********************************************************************************************************

ALTER TABLE MUActivityLog DISABLE ALL TRIGGERS;

DECLARE 
  v_currentdate DATE := SYSDATE; 
  v_APIB2CDate DATE := to_date('01/01/2018','MM/DD/YYYY');
  v_PreviousRun Numeric(19,0):= 0;
BEGIN
	Select NVL(OtherLong,0) into v_PreviousRun FROM MEDLISTS where TABLENAME = 'OneTimeActivity' and FUNCTIONNAME = 'PatientAPIAccessScript1';
		if NVL(v_PreviousRun,0) = 1 then
			-- For each valid email and haspatientAccess=�Y� deny 525 event > 1/1/2018 insert grant 526 at event time + 1 min, run as often as needed
			INSERT INTO MUActivityLog (MUActivityLogId, MUActivityLogTypeId, PID,  PVID, exported, DeliveryMethod, Event_Timestamp, DB_Create_Timestamp)
			with CTE as 
			(select PID, Max(Event_Timestamp) MaxEvent_Timestamp 
			from MUActivityLog where MUActivityLogTypeId = 525 and Event_Timestamp > v_APIB2CDate 
			group by PID)
			SELECT GEN_EMR_ID, (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 526 else 525 end), PERSON.PID, -2, 'N', 
			'Patient Access'|| (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then ' Granted' else ' Denied' end) ,(Cdata.MaxEvent_Timestamp + 1/24/60) ,v_currentdate
			from PERSON inner join CTE Cdata on PERSON.PID = Cdata.PID 
			where NVL(ISPATIENT,'Y') = 'Y' and PSTATUS = 'A' 
			and HasPatientAccess = 1 AND (CASE WHEN EMail IS not null and EMail like '%_@_%.__%' then 1 else 0 end) = 1 
			AND not exists(select 1 from MUActivityLog where PID = PERSON.PID and MUActivityLogTypeId = 526 and Event_Timestamp > Cdata.MaxEvent_Timestamp);
			commit;

		end if; 
END;
/

ALTER TABLE MUActivityLog ENABLE ALL TRIGGERS;

-- ***********************************************************************
-- Author		: Abhijeet Badale
-- User Story	: Back porting SPR 3872: CISDB00072359: CCDA generation failing for some patients via FHIR 2.1 version
-- Purpose		: Add index for performance improvement
-- Date			: 
-- Apply		: Upgrade, Demo, and Install
-- ***********************************************************************
Exec ML.Patch_Update('EMR Database Script2 for Patient Data Access Checkbox', 'EMR Database Script2 for Patient Data Access Checkbox');
commit;

spool off

exit;