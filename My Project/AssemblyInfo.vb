﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("EMR Stat")>
<Assembly: AssemblyDescription("Create EMR Report")>
<Assembly: AssemblyCompany("Crozer Prospect")>
<Assembly: AssemblyProduct("EMR Stat")>
<Assembly: AssemblyCopyright("Copyright © Crozer Prospect 2018")>
<Assembly: AssemblyTrademark("Crozer Prospect 2018")>

<Assembly: ComVisible(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c5158af1-0309-4c15-a6b2-a8aa03940631")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("3.7.2020.0707")>
<Assembly: AssemblyFileVersion("3.7.2020.0707")>
